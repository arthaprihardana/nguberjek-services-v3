'use strict';

module.exports = {
    "plugins": [],
    "recurseDepth": 10,
    "source": {
        "include": [ "./src", "./README.md" ],
        "exclude": [ "./node_modules", "./src/models/veritransModel.js", "./src/controllers/veritransController.js", "./src/routes" ],
        "includePattern": ".+\\.js(doc|x)?$",
        "excludePattern": "(^|\\/|\\\\)_"
    },
    "sourceType": "module",
    "tags": {
        "allowUnknownTags": true,
        "dictionaries": ["jsdoc"]
    },
    "templates": {
        // "cleverLinks": false,
        // "monospaceLinks": false
    },
    "opts": {
        "template": "node_modules/tui-jsdoc-template",
        "encoding": "utf8",
        "destination": "./dokumentasi/",
        "recurse": true,
        // "tutorials": "./tutorials",
    }
}