/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 11:36:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-29 06:03:40
 */
import gulp from 'gulp';
import path from 'path';
import del from 'del';
import pump from 'pump';
import gulpLoadPlugins from 'gulp-load-plugins';
import babelCompiler from 'babel-core/register';
import * as isparta from 'isparta';
import rollupIncludePaths from 'rollup-plugin-includepaths';

const plugins = gulpLoadPlugins();

const paths = {
  scripts: ['./**/*.js', '!build/**', '!node_modules/**', '!report/**'],
  rollupfile: ['src/routes', 'src/controllers', 'src/helpers', 'src/models', 'src/middlewares'],
  rollupentry: 'src/app.js',
  bdd: './src/tests/bdd/*.js',
  tdd: './src/tests/tdd/*.js',
};

const options = {
  codeCoverage: {
    reporters: ['lcov', 'text-summary'],
    thresholds: {
      global: { statements: 80, branches: 80, functions: 80, lines: 80 }
    }
  }
};

// Lint Code
gulp.task('lint', () =>
  gulp.src(paths.scripts)
  .pipe(plugins.eslint())
  .pipe(plugins.eslint.format())
  .pipe(plugins.eslint.failAfterError())
);

// Clean Up Transpile
gulp.task('clean-transpile', () =>
  del(['build//**', '!build/transpile'])
);

// Clean Up Bundle
gulp.task('clean-bundle', () =>
  del(['build/bundle/app.js', '!build/bundle'])
);

// Clean Up Bundle Minify
gulp.task('clean-bundle-min', () =>
  del(['build/bundle/app.min.js', '!build/bundle'])
);

gulp.task('clean-bdd', () =>
  del(['report/unit-test/bdd//**', '!report/unit-test'])
);

gulp.task('clean-coverage', () =>
  del(['report/unit-test/coverage//**', '!report/unit-test'])
);

// covers files for code coverage
gulp.task('pre-test-coverage', () =>
  gulp.src([...paths.scripts, '!gulpfile.babel.js'])
    // Covering files
    .pipe(plugins.istanbul({
      instrumenter: isparta.Instrumenter,
      includeUntested: true
    }))
    // Force `require` to return covered files
    .pipe(plugins.istanbul.hookRequire())
);

// triggers mocha test BDD (Behaviour Development Test)
gulp.task('bdd', ['pre-test-coverage', 'clean-bdd', 'clean-coverage'], () => {
  let reporters;
  let exitCode = 0;

  if (plugins.util.env['code-coverage-reporter']) {
    reporters = [...options.codeCoverage.reporters, plugins.util.env['code-coverage-reporter']];
  } else {
    reporters = options.codeCoverage.reporters;
  }

  return gulp.src([paths.bdd], { read: false })
    .pipe(plugins.plumber())
    .pipe(plugins.mocha({
      reporter: 'mochawesome',
      reporterOptions: {
        reportDir: 'report/unit-test/bdd',
        reportName: 'BDD API TEST',
        reportTitle: 'API Test Restful',
        inlineAssets: true
      },
      ui: 'bdd',
      timeout: 6000,
      compilers: {
        js: babelCompiler
      }
    }))
    .once('error', (err) => {
      plugins.util.log(err);
      exitCode = 1;
    })
    // Creating the reports after execution of test cases
    .pipe(plugins.istanbul.writeReports({
      dir: './report/unit-test/coverage',
      reporters
    }))
    // Enforce test coverage
    .pipe(plugins.istanbul.enforceThresholds({
      thresholds: options.codeCoverage.thresholds
    }))
    .once('end', () => {
      plugins.util.log('BDD Unit Test Completed !!!');
      process.exit(exitCode);
    });
});

// Compile ES6 to ES5 and copy to build/transpile
gulp.task('transpile', ['clean-transpile'], () =>
  gulp.src([...paths.scripts, '!gulpfile.babel.js'], { base: '.' })
  .pipe(plugins.newer('dist'))
  .pipe(plugins.sourcemaps.init())
  .pipe(plugins.babel())
  .pipe(plugins.sourcemaps.write('.', {
    includeContent: false,
    sourceRoot(file) {
      return path.relative(file.path, __dirname);
    }
  }))
  .pipe(gulp.dest('build/transpile'))
);

// Bundle Project - Build to production
gulp.task('bundle', ['clean-bundle'], () => {
  gulp.src([...paths.scripts, '!gulpfile.babel.js'], { base: '.' })
    .pipe(plugins.rollup({
      entry: paths.rollupentry,
      sourceMap: true,
      plugins: [
        rollupIncludePaths({
          paths: paths.rollupfile
        }),
        plugins.babel({
          exclude: 'node_modules/**',
          presets: ['es2015-rollup']
        })
      ]
    }))
    .pipe(plugins.babel())
    .on('error', plugins.util.log)
    .pipe(plugins.rename('app.js'))
    .pipe(plugins.sourcemaps.write('.', {
      includeContent: false,
      sourceRoot(file) {
        return path.relative(file.path, __dirname);
      }
    }))
    .pipe(gulp.dest('build/bundle'));
});

// Bundle Project - Build to production (Minify)
gulp.task('bundle-min', ['clean-bundle-min'], (cb) => {
  pump([
    gulp.src([...paths.scripts, '!gulpfile.babel.js'], { base: '.' })
    .pipe(plugins.rollup({
      entry: paths.rollupentry,
      sourceMap: true,
      plugins: [
        rollupIncludePaths({
          paths: paths.rollupfile
        }),
        plugins.babel({
          exclude: 'node_modules/**',
          presets: ['es2015-rollup']
        })
      ]
    }))
    .pipe(plugins.babel())
    .on('error', plugins.util.log)
    .pipe(plugins.rename('app.min.js'))
    .pipe(plugins.sourcemaps.write('.', {
      includeContent: false,
      sourceRoot(file) {
        return path.relative(file.path, __dirname);
      }
    })),
    plugins.uglify(),
    gulp.dest('build/bundle')
  ], cb);
});