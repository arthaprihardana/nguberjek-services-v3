# NGUBERJEK API SERVICE 2017

Dokumentasi API layanan ojek online NGUBERJEK.

> Download app NGUBERJEK dibawah ini :
> 
> [![unduh app di playstore](http://magicourway.com/wp-content/themes/playcast/images/btn-widget/google-play1.png "Unduh app di playstore")](https://play.google.com/store/apps/details?id=com.boncenger.nguberjek&hl=in)


## Model

1. Admin Model
1. Boncenger Model
1. CDN Model
1. Chat Model
1. Driver Model
1. Order Model
1. Price Model
1. Selfi Order Model

## Controllers
1. Admin Controller
1. Boncenger Controller
1. CDN Controller
1. Chat Controller
1. Driver Controller
1. Order Controller
1. Price Controller
1. Selfi Order Controller

## Helpers
1. Auth
1. Firebase
1. Generate
1. Socket IO
1. Math
1. Object
1. Tanggal
1. Validate

## Team
Copyright 2017 &copy; Karawang Akademy

###### Project Manager
1.  Artha Prihardana
1.  Ade

###### BackEnd Developer
1. Artha Prihardana

###### Database Design
1. Artha Prihardana

###### Mobile Application Developer
1. Adi Kurniawan
1. Iif Alan

###### Design UI/UX
1.  Hilmy Sugoi

###### Owner
1.  Sabah Saparian