/**
 * @author: Artha Prihardana
 * @Date: 2017-10-25 10:45:26
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-26 23:32:09
 */
import express from "express";
import {
  requestFoodOrder,
  getOrderFood,
  acceptRequest,
  updateOrder,
  pingOrder
} from "../controllers/foodOrderController";

const router = express.Router();

router.route("/ws/requestFoodOrder").post(requestFoodOrder);

router.route("/ws/acceptFoodOrder").post(acceptRequest);

router.route("/ns/riwayatFoodOrder").get(getOrderFood);

router.route("/ns/updateFoodOrder").post(updateOrder);

router.route("/ping/foodorder/:id").get(pingOrder);

export default router;
