/**
 * @author: Artha Prihardana 
 * @Date: 2017-11-04 14:41:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 14:42:16
 */
import express from 'express'
import {
    createKategori,
    getKategori,
    putKategori,
    delKategori
} from '../controllers/kategoriRestoController'

const router = express.Router()

router.route('/ns/kategoriResto')
    .post(createKategori)
    .get(getKategori)
    .put(putKategori)
    .delete(delKategori);

router.route('/ns/kategoriResto/:id')
    .get(getKategori)
    .put(putKategori)
    .delete(delKategori)

export default router;