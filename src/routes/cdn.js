/**
 * @author Artha Prihardana 
 * @Date 2017-02-03 22:09:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-02-03 23:04:33
 */
import express from 'express';
import {
    upload,
    // getImage
} from '../controllers/cdnController';

const router = express.Router();

router.route('/ns/upload')
    .post(upload);
// router.route('/ns/getImage')
//     .get(getImage);

export default router;