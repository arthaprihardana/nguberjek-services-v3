/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 09:44:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-30 17:44:31
 */
import express from 'express';
import {
    getHistory,
    addSelfieOrder,
    updateSelfieOrder
} from '../controllers/selfieOrderController';

const router = express.Router();

router.route('/ns/selfie')
    .get(getHistory)
    .post(addSelfieOrder);

router.route('/ns/status_perjalanan_selfie')
    .put(updateSelfieOrder);

export default router;