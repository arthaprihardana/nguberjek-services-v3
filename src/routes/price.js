/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 00:52:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-31 18:15:37
 */
import express from 'express';
import {
    getPrice,
    addPrice,
    updatePrice,
    delPrice,
    hitPrice
} from '../controllers/priceController';

const router = express.Router();

router.route('/ns/price')
    .get(getPrice)
    .post(addPrice)
    .put(updatePrice)
    .delete(delPrice);

router.route('/ns/fare')
    .get(hitPrice)

export default router;