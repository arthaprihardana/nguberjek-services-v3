/**
 * @author Artha Prihardana 
 * @Date 2017-02-15 22:00:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-03-17 01:51:18
 */
import express from 'express';
import {
    sendMessage,
    listMessage,
    sendNotificationTest
} from '../controllers/chatController';

const router = express.Router();

router.route('/ws/sendChat')
    .post(sendMessage);

router.route('/ws/listChat')
    .get(listMessage);

router.route('/ns/sendNotificationTest')
    .post(sendNotificationTest);

export default router;