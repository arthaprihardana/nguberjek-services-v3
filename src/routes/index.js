/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 11:39:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-12 11:26:49
 */
import express from 'express';
import admin from './admin';
import boncenger from './boncenger';
import driver from './driver';
import price from './price';
import selfieorder from './selfieOrder';
import order from './order';
import cdn from './cdn';
import chat from './chat';
import payment from './veritrans';
import resto from './resto';
import kategoriMenu from './kategoriMenu'
import kategoriResto from './kategoriResto'
import menu from './menu'
import foodOrder from './foodOrder'

const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({
    s:1,
    statusCode: res.statusCode,
    message: "",
    data: {
      apiName: "REST API NGUBERJEK VERSION 1.0.0",
      apiVersion: "1.0.0",
      apiDescription: "REST API APLIKASI NGUBERJEK"
    }
  })
});

router.get('/info', (req, res, next) => {
  res.send({
    s:1,
    statusCode: res.statusCode,
    message: '',
    data: {
      boncegerVersion: "16",
      riderVersion: "14"
    }
  })
})

router.use('/', admin);
router.use('/', boncenger);
router.use('/', driver);
router.use('/', price);
router.use('/', selfieorder);
router.use('/', order);
router.use('/', cdn);
router.use('/', chat);
router.use('/', payment);
router.use('/', resto);
router.use('/', kategoriMenu)
router.use('/', kategoriResto)
router.use('/', menu)
router.use('/', foodOrder)


export default router;
