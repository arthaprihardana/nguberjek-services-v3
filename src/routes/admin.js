/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 21:10:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-30 05:47:12
 */
import express from 'express';
import { 
    registrasiAdmin, 
    loginAdmin, 
    getAdmin,
    updateAdmin,
    deleteAdmin,
    logoutAdmin,
    lupaPassword,
    getFranchise
} from '../controllers/adminController';

const router = express.Router();

router.route('/ns/admin')
    .get(getAdmin)
    .put(updateAdmin)
    .delete(deleteAdmin);

router.route('/ns/lgo/admin')
    .get(logoutAdmin);

router.route('/reg/admin')
    .post(registrasiAdmin);

router.route('/lgn/admin')
    .post(loginAdmin)

router.route('/lupaPassword/admin')
    .post(lupaPassword);

router.route('/ns/franchise')
    .get(getFranchise)

export default router;