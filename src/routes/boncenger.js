/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 07:10:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-02-15 21:35:01
 */
import express from 'express';
import {
    registrasiBoncenger,
    loginBoncenger,
    getBoncenger,
    updateBoncenger,
    deleteBoncenger,
    logoutBoncenger,
    lupaPassword,
    simpanFavoritPerjalanan,
    favoritPerjalanan,
    hapusFavoritPerjalanan,
    simpanFavoritDriver,
    favoritDriver,
    hapusFavoritDriver,
    graphBoncenger, 
    orderCountByBoncenger
} from '../controllers/boncengerController';

const router = express.Router();

router.route('/ns/boncenger')
    .get(getBoncenger)
    .put(updateBoncenger)
    .delete(deleteBoncenger);

router.route('/reg/boncenger')
    .post(registrasiBoncenger);

router.route('/lgn/boncenger')
    .post(loginBoncenger);

router.route('/ns/lgo/boncenger')
    .get(logoutBoncenger);

router.route('/lupaPassword/boncenger')
    .post(lupaPassword);

router.route('/ns/favorit/perjalanan')
    .get(favoritPerjalanan)
    .post(simpanFavoritPerjalanan)
    .delete(hapusFavoritPerjalanan);

router.route('/ns/favorit/driver')
    .get(favoritDriver)
    .post(simpanFavoritDriver)
    .delete(hapusFavoritDriver);

router.route('/ns/graph/boncenger')
    .get(graphBoncenger)

router.route('/ns/boncenger/top5')
    .get(orderCountByBoncenger)

export default router;