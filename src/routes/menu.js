/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-21 10:27:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 07:09:30
 */
import express from 'express'
import {
    createMenu,
    putMenu,
    delMenu,
    getMenu,
    groupMenuByCategory
} from '../controllers/menuController'

const router = express.Router();

router.route('/ns/menu')
    .post(createMenu)
    .get(getMenu)
    .put(putMenu)
    .delete(delMenu)

router.route('/ns/menu/:id')
    .get(getMenu)
    .put(putMenu)
    .delete(delMenu)

router.route('/ns/groupmenu')
    .get(groupMenuByCategory)

export default router;