/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 07:52:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-05-13 14:27:59
 */
import express from 'express';
import {
    registrasiDriver,
    loginDriver,
    getDriver,
    updateDriver,
    deleteDriver,
    realtimeRadiusDriver,
    realtimeDriverOn,
    realtimePosition,
    logoutDriver,
    pendapatanOrder,
    pendapatanSelfieOrder,
    ratingDriver,
    lupaPassword,
    saldoDriver,
    lastOrder,
    lastSelfieOrder,
    totalPendapatan,
    driverByLocation
} from '../controllers/driverController';

const router = express.Router();

router.route('/ns/driver')
    .get(getDriver)
    .put(updateDriver)
    .delete(deleteDriver);

router.route('/reg/driver')
    .post(registrasiDriver);

router.route('/lgn/driver')
    .post(loginDriver);

router.route('/ws/radius/driver')
    .get(realtimeRadiusDriver);

router.route('/ws/active/driver')
    .get(realtimeDriverOn);

router.route('/ws/position/driver')
    .put(realtimePosition);

router.route('/ns/lgo/driver')
    .get(logoutDriver);

router.route('/ns/pendapatanOrder')
    .get(pendapatanOrder);

router.route('/ns/pendapatanSelfieOrder')
    .get(pendapatanSelfieOrder);

router.route('/ns/totalPendapatan')
    .get(totalPendapatan);

router.route('/ns/ratingDriver')
    .get(ratingDriver);

router.route('/lupaPassword/driver')
    .post(lupaPassword);

router.route('/ns/saldo/rider')
    .get(saldoDriver);

router.route('/ns/lastorder')
    .get(lastOrder);

router.route('/ns/lastselfieorder')
    .get(lastSelfieOrder);

router.route('/ns/driverbylocation')
    .get(driverByLocation);

export default router;