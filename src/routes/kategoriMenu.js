/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-08 15:39:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-08 15:43:58
 */
import express from 'express'
import {
    createKategori,
    getKategori,
    putKategori,
    delKategori
} from '../controllers/kategoriMenuController'

const router = express.Router()

router.route('/ns/kategoriMenu')
    .post(createKategori)
    .get(getKategori)
    .put(putKategori)
    .delete(delKategori);

router.route('/ns/kategoriMenu/:id')
    .get(getKategori)
    .put(putKategori)
    .delete(delKategori)

export default router;