/**
 * @author Artha Prihardana 
 * @Date 2017-02-25 16:40:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-12 11:17:26
 */
import express from 'express';
import {
    charge,
    status,
    getPayment,
    midtransNotifikasi,
    bisaTopUp
} from '../controllers/veritransController';

const router = express.Router();

router.route('/ns/payment/charge')
    .post(charge);

router.route('/ns/payment/stats/:order_id')
    .get(status);

router.route('/ns/payment/getdb')
    .get(getPayment);

router.route('/payment/notification')
    .post(midtransNotifikasi);

router.route('/payment/bisatopup')
    .post(bisaTopUp)

export default router;