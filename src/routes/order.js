/**
 * @author Artha Prihardana 
 * @Date 2017-01-31 09:35:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-05-05 01:58:17
 */
import express from 'express';
import {
    getHistory,
    requestOrder,
    acceptRequest,
    updateOrder,
    orderDetail,
    pingOrder,
    simulasiCancelOrder,
    orderInProgress
} from '../controllers/OrderController';

const router = express.Router();

router.route('/ws/requestOrder')
    .post(requestOrder);

router.route('/ws/acceptOrder')
    .post(acceptRequest);

router.route('/ns/updateStatusOrder')
    .post(updateOrder);

router.route('/ns/history')
    .get(getHistory);

router.route('/ns/orderDetail')
    .get(orderDetail);

router.route('/ping/order/:id')
    .get(pingOrder);

router.route('/simulasi')
    .get(simulasiCancelOrder);

router.route('/ns/orderInProgress')
    .get(orderInProgress)

export default router;