/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-07 11:34:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-08 15:40:33
 */
import express from 'express'
import {
    createResto,
    putResto,
    delResto,
    getResto, 
    getRadiusResto
} from '../controllers/restoController'

const router = express.Router();

router.route('/ns/resto')
    .post(createResto)
    .get(getResto)
    .put(putResto)
    .delete(delResto)

router.route('/ns/resto/:id')
    .get(getResto)
    .put(putResto)
    .delete(delResto)

router.route('/ws/radius/resto')
    .get(getRadiusResto);

export default router;