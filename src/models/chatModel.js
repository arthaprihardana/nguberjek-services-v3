/**
 * @author Artha Prihardana 
 * @Date 2017-02-15 22:00:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-25 00:19:21
 */
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Chat
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} orderId       - Order ID
 * @property {string} riderId       - Rider ID
 * @property {string} boncengerId   - Boncenger ID
 * @property {array} message        - Message
 */
const chatSchema = new mongoose.Schema({
    orderId: {type: String},
    riderId: {type: mongoose.Schema.Types.ObjectId, ref: 'rider'},
    boncengerId: {type: mongoose.Schema.Types.ObjectId, ref: 'boncenger'},
    message: [
        { 
            rider: {type: Boolean},
            boncenger: {type: Boolean},
            msg: {type: String},
            date: {type: Date}
        }
    ]
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"} });

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const ChatModel = mongoose.model('chat', chatSchema);
/**
 * Export module Chat Model
 * @module ChatModel
 * @see chatSchema
 */
export default ChatModel;