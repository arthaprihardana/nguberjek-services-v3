/**
 * @author: Artha Prihardana 
 * @Date: 2017-08-26 12:18:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 14:47:58
 */
import config from '../app.conf'
import validate from '../helpers/validateHelper'
import mongoose from 'mongoose'

/**
 * Skema dokumen (tabel) Resto
 * @const
 * @type {Object.<string>}
 * @property {string} namaResto         - Nama Resto
 * @property {string} alamat            - Alamat
 * @property {number} koordinat         - Koordinat
 * @property {string} jamBuka           - Jam Buka
 * @property {string} foto              - Foto
 */
const restoSchema = new mongoose.Schema({
    namaResto: {
        type: String,
        required: true
    },
    alamat: {
        type: String,
        required: true
    },
    noTelp: {
        type: String
    },
    koordinat: {
        type: [Number],
        index:'2dsphere',
        required: true
    },
    jamBuka: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    kategori: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'kategoriResto',
        required: true
    },
    show: {
        type: Boolean,
        default: true
    }
}, {
    timestamps:{
        createdAt:'created_at',
        updatedAt:'updated_at'
    }
});

/**
 * Validasi namaResto jika sudah pernah terdaftar sebelumnya
 * @func validateNamaResto
 * @param {string} val - Nilai (namaResto) yang akan dicek
 * @param {cbNamaResto} cb - Callback yang akan menangani respon
 */
restoSchema.path('namaResto').validate((val, cb) => {
    RestoModel.find({namaResto: val}, (err, docs) => {
        if(docs.length > 0) {
            cb(false,'Nama Resto sudah terdaftar');
        } else {
            cb(true);
        }
    })
})
/**
 * Callback akan mengembalikan nilai namaResto
 * @callback cbNamaResto
 * @param {boolean} status - status true / false
 * @param {string} message - pesan yang akan ditampilkan
 */

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const RestoModel = mongoose.model('resto', restoSchema);
/**
 * Export module Resto Model
 * @module RestoModel
 * @see restoSchema
 * @see validateNamaResto
 */
/** Call model "RestoModel" */
export default RestoModel;