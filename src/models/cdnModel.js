/**
 * @author Artha Prihardana 
 * @Date 2017-02-03 21:51:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-25 00:16:19
 */
import config from '../app.conf';
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Content Image
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} fieldname         - Fieldname
 * @property {string} originalname      - Original Name
 * @property {string} encoding          - Encoding
 * @property {string} mimetype          - Mimetype
 * @property {string} destination       - Destination
 * @property {string} filename          - Filename
 * @property {string} path              - Path
 * @property {number} size              - Size
 * @property {string} base64            - Base 64 File
 * @property {string} link              - Link
 */
const cdnSchema = new mongoose.Schema({
    "fieldname": {type: String},
    "originalname": {type: String},
    "encoding": {type: String},
    "mimetype": {type: String},
    "destination": {type: String},
    "filename": {type: String},
    "path": {type: String},
    "size": {type: Number},
    "base64": {type: String},
    "link": {type: String},
}, {timestamps:{createdAt:'created_at',updatedAt:'updated_at'}});

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
let cdnModel = mongoose.model('image', cdnSchema);
/**
 * Export module CDN Model
 * @module cdnModel
 * @see cdnSchema
 */
export default cdnModel;