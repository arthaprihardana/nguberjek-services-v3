/**
 * @author: Artha Prihardana 
 * @Date: 2017-08-26 12:27:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-21 10:35:52
 */
import config from '../app.conf'
import validate from '../helpers/validateHelper'
import mongoose from 'mongoose'

/**
 * Skema dokumen (tabel) Menu
 * @const
 * @type {Object.<string>}
 * @property {string} namaMenu      - Nama Menu
 * @property {string} deskripsi     - Deskripsi
 * @property {number} harga         - Harga
 * @property {string} foto          - Foto
 * @property {string} kategori      - Kategori
 * @property {ObjectId} resto       - Resto
 */
const MenuSchema = new mongoose.Schema({
    namaMenu: {
        type: String,
        required: true
    },
    deskripsi: {
        type: String,
    },
    harga: {
        type: Number,
        required: true
    },
    foto: {
        type: String
    },
    kategori: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'kategoriMenu',
        required: true
    },
    resto: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'resto',
        required: true
    },
    show: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: {createdAt: "created_at", updatedAt: "updated_at"}
})

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const MenuModel = mongoose.model('menu', MenuSchema);
/**
 * Export module Menu Model
 * @module MenuModel
 * @see MenuSchema
 */
/** Call model "MenuModel" */
export default MenuModel