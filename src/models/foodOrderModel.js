/**
 * @author: Artha Prihardana 
 * @Date: 2017-08-26 12:37:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-26 23:08:06
 */
import config from '../app.conf';
import validate from '../helpers/validateHelper'
import mongoose from 'mongoose'

/**
 * Skema dokumen (tabel) food order
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} namaResto                 - Nama Resto
 * @property {array.<Object>} namaMenu          - Nama Menu
 * @property {number} koordinatPengantaran      - Koordinat pengantaran
 * @property {string} alamatPengantaran         - Alamat pengantaran
 * @property {number} perkiraanHarga            - Perkiraan Harga
 * @property {number} biayaAntar                - Biaya Antar
 * @property {number} totalHarga                - Total Harga
 * @property {objectId} penumpang               - Penumpang
 */
const FoorOrderSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: true
    },
    tanggal: {
        type: Date, 
        required: true
    },
    namaResto: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'resto',
        required: true
    },
    namaMenu: [{
        menuId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'menu'
        },
        harga: {
            type: Number,
            required: true
        },
        jmlPorsi: {
            type: Number,
            required: true
        },
        subTotal: {
            type: Number,
            required: true
        },
        catatan:{
            type: String
        }
    }],
    koordinatPengantaran: {
        type: [Number], 
        index: '2dsphere'
    },
    alamatPengantaran: {
        type: String,
        required: true
    },
    jarak: {
        type: Number
    },
    waktu: {
        type: String
    },
    perkiraanHarga: {
        type: Number,
        required: true
    },
    biayaAntar: {
        type: Number,
        required: true
    },
    totalHarga: {
        type: Number,
        required: true
    },
    boncengerId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'boncenger'
    },
    riderId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'rider'
    },
    rating: {
        type: Number
    },
    komentar: {
        type: String
    },
    status: {
        type: Number    // (0) untuk status order request (1) untuk status order diterima (2) untuk status rider menjemput boncenger (3) untuk status perjalanan sedang berlangsung (4) untuk status telah selesai (5) untuk status jika terjadi masalah pada saat perjalanan berlangsung (potong harga 50%)
    },
    show: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: {
        createdAt: "created_at", 
        updatedAt: "updated_at"
    } 
})

/**
 * Register dan inisalisasi nama model
 * @const
 * @type {Object.<function>}
 */
const foodOrder = mongoose.model('foodOrder', FoorOrderSchema);
/**
 * Export module Food Order
 * @module foodOrder
 * @see FoorOrderSchema
 */
/** Call model "foodOrder" */
export default foodOrder;