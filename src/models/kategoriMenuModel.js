/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-08 14:56:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-08 15:52:18
 */
import config from '../app.conf'
import validate from '../helpers/validateHelper'
import mongoose from 'mongoose'

const KategoriMenuSchema = new mongoose.Schema({
    namaKategori: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    show: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    }
})

KategoriMenuSchema.path('namaKategori').validate((val, cb) => {
    kategoriMenuModel.find({namaKategori: val}, ( err, docs) => {
        if(docs.length > 0) {
            cb(false, 'Nama Kategori sudah terdaftar')
        } else {
            cb(true)
        }
    })
})

const kategoriMenuModel = mongoose.model('kategoriMenu', KategoriMenuSchema)

export default kategoriMenuModel