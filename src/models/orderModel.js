/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 18:33:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-27 16:48:15
 */
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Order
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} orderId           - Order ID
 * @property {date} tanggal             - Tanggal Order
 * @property {ObjectId} boncengerId     - Boncenger ID
 * @property {ObjectId} riderId         - Driver ID
 * @property {object} lokasiAwal        - Lokasi Awal
 * @property {object} lokasiAkhir       - Lokasi Akhir
 * @property {number} harga             - Harga
 * @property {number} jarak             - Jarak
 * @property {string} waktu             - Waktu
 * @property {number} status            - Status Order
 * @property {number} rating            - Rating
 * @property {string} komentar          - Komentar
 * @property {boolean} setasfavorit     - Jadikan favorit rute
 * @property {string} kategori          - Kategori
 * @property {boolean} show             - Show order
 */
const OrderSchema = new mongoose.Schema({
    orderId:{type:String, required: true},
    tanggal: {type: Date, required: true},
    boncengerId: {type: mongoose.Schema.Types.ObjectId, ref: 'boncenger'},
    riderId: {type: mongoose.Schema.Types.ObjectId, ref: 'rider'},
    lokasiAwal: {
        lokasi: {type: String},
        koordinatLokasi: { type: [Number], index: '2dsphere'},   // [<longitude>, <latitude>]
        keteranganLokasiAwal: { type: String }
    },
    lokasiAkhir: {
        tujuan: {type:String},
        koordinatTujuan: { type: [Number], index: '2dsphere'},   // [<longitude>, <latitude>]
        keteranganLokasiAkhir: {type:String}
    },
    harga: {type: Number},
    jarak: {type: Number},
    waktu: {type: String},
    status: {type: Number},     // (0) untuk status order request (1) untuk status order diterima (2) untuk status rider menjemput boncenger (3) untuk status perjalanan sedang berlangsung (4) untuk status telah selesai (5) untuk status jika terjadi masalah pada saat perjalanan berlangsung (potong harga 50%)
    rating: {type: Number},     // bintang 1 - 5
    komentar: {type: String},
    setasfavorit: {type: Boolean, default: false},
    kategori: {type: String, required: true},
    show: {type: Boolean, default: true}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"} });

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const OrderModel = mongoose.model("order", OrderSchema);
/**
 * Export module Order Model
 * @module OrderModel
 * @see OrderSchema
 */
export default OrderModel;