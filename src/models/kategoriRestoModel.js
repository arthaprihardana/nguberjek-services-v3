/**
 * @author: Artha Prihardana 
 * @Date: 2017-11-04 14:39:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 14:40:00
 */
import config from '../app.conf'
import validate from '../helpers/validateHelper'
import mongoose from 'mongoose'

const KategoriRestoSchema = new mongoose.Schema({
    namaKategori: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    show: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    }
})

KategoriRestoSchema.path('namaKategori').validate((val, cb) => {
    kategoriRestoModel.find({namaKategori: val}, ( err, docs) => {
        if(docs.length > 0) {
            cb(false, 'Nama Kategori sudah terdaftar')
        } else {
            cb(true)
        }
    })
})

const kategoriRestoModel = mongoose.model('kategoriResto', KategoriRestoSchema)

export default kategoriRestoModel