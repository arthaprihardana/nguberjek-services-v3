/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 00:26:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-27 16:54:42
 */
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Harga
 * @const
 * @type {Object.<string>}
 * @property {string} role              - Tipe Layanan
 * @property {boolean} use              - Status penggunaan
 * @property {number} tarif_buka_pintu  - Tarif dasar
 * @property {number} flat_jarak        - Satuan flat jarak
 * @property {number} flat_harga        - Satuan flat harga
 * @property {array} price              - Detail harga
 * @property {string} placePrice        - Lokasi berlaku harga / tarif
 * @property {number} placeKoordinat    - Koordinat radius berlaku harga / tarif
 * @property {boolean} show             - Show harga
 */
const priceSchema = new mongoose.Schema({
    role: {type: String, required: true},          // ada 2 role : harga untuk nguberjek dan harga untuk ngubertaksi
    use: {type: Boolean, default: true},
    tarif_buka_pintu: {type: Number},              // jika role adalah ngubertaksi
    flat_jarak: {type: Number},
    flat_harga: {type: Number, min: 1000, max: 10000},
    price: [],
    placePrice: {type:String},
    placeKoordinat: {type:[Number],index:'2dsphere'},
    show: {type: Boolean, default: true}
}, {timestamps: {createdAt:"created_at", updatedAt:"updated_at"}});

/**
 * Detail Price
 * @const
 * @type {Object.<function>}
 * @property {number} kalkulasi_jarak       - Kalkulasi Jarak
 * @property {number} kalkulasi_harga       - Kalkulasi harga
 */
const priceDetailSchema = new mongoose.Schema({
    kalkulasi_jarak: {type: Number},
    kalkulasi_harga: {type: Number, min: 1000, max: 10000}
});

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const PriceModel = mongoose.model('price',priceSchema);
/**
 * Export module Price Model
 * @module PriceModel
 * @see priceSchema
 * @see priceDetailSchema
 */
export default PriceModel;