/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 09:14:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-27 21:08:45
 */
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Selfi Order
 * @const
 * @type {Object.<string>}
 * @property {string} orderId           - Order ID
 * @property {date} tanggal             - Tanggal
 * @property {string} noTelpKonsumen    - No Telepeon
 * @property {string} namaKonsumen      - Nama Konsumen
 * @property {ObjectId} riderId         - Rider ID
 * @property {object} lokasiAwal        - Lokasi Awal
 * @property {object} lokasiAkhir       - Lokasi Akhir
 * @property {number} harga             - Harga
 * @property {string} jarak             - Jarak
 * @property {string} waktu             - Waktu
 * @property {number} status            - Status
 * @property {boolean} show             - Tampilkan data
 */
const selfieOrderSchema = new mongoose.Schema({
    orderId: {type: String, require: true},
    tanggal: {type: Date, required: true},
    noTelpKonsumen: {type: String, required: true},
    namaKonsumen: {type: String, required: true},
    riderId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'rider'},
    lokasiAwal: {
        lokasi: {type: String},
        koordinatLokasi: {type: [Number], index: '2dsphere'}
    },
    lokasiAkhir: {
        tujuan: {type: String},
        koordinatTujuan: {type: [Number], index: '2dsphere'}
    },
    harga: {type: Number},
    jarak: {type: String},
    waktu: {type: String},
    status: {type: Number},
    show: {type: Boolean, default: true}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"} });

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
const selfieOrderModel = mongoose.model("selfieorder", selfieOrderSchema);
/**
 * Export module Selfie Order Model
 * @module selfieOrderModel
 * @see selfieOrderSchema
 */
/** Call model "selfieOrderModel" */
export default selfieOrderModel;