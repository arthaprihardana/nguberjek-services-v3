/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 20:06:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-27 08:47:55
 */
import config from '../app.conf';
import validate from '../helpers/validateHelper';
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Admin
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} nama          - Nama
 * @property {string} tempatLahir   - Tempat Lahir
 * @property {date} tglLahir        - Tanggal Lahir
 * @property {string} alamat        - Alamat 
 * @property {string} email         - Email
 * @property {string} noTelp        - No Telepon
 * @property {string} role          - Role
 * @property {string} photo         - Photo
 * @property {string} lokasi        - Lokasi
 * @property {string} username      - Username
 * @property {string} password      - Password
 * @property {boolean} show         - Show
 * @property {boolean} approve      - Approve
 * @property {token} token          - Token
 */
const adminSchema = new mongoose.Schema({
    nama:{type:String,required:true},
    tempatLahir: {type:String},
    tglLahir:{type:Date},
    alamat:{type:String},
    email:{
        type:String,
        required:true,
        lowercase: true,
        unique: true,
        validate:[validate.email, 'Please fill a valid email address']
    },
    noTelp:{
        type:String,
        validate:[validate.phone, 'Please fill a valid phone number']
    },
    role:{
        type:String,
        enum: ['admin', 'user', 'franchise'],
        default: 'user',
        lowercase: true,
    },
    photo:{type:String,default:config.apiUrl+':'+config.apiPort+'/images/default.png'},
    lokasi:{type:String},
    username:{type:String,required:true},
    password:{type:String,required:true},
    show:{type:Boolean,default:true},
    approve:{type:Boolean,default:true},
    token:{type:String}
}, {timestamps:{createdAt:'created_at',updatedAt:'updated_at'}});
/**
 * Validasi nama jika sudah pernah terdaftar sebelumnya
 * @func validateNama
 * @param {string} val - Nilai (nama) yang akan dicek 
 * @param {cbNama} cb - Callback yang menangani respon
 */
adminSchema.path('nama').validate((val,cb) => {
    AdminModel.find({nama:val},(err, docs) => {
        if(docs.length > 0) {
            cb(false, 'Nama sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai nama.
 * @callback cbNama
 * @param {boolean} status - status true/false
 * @param {string} message - pesan yang akan ditampilkan
 */

 /**
 * Validasi email jika sudah pernah terdaftar sebelumnya
 * @func validateEmail
 * @param {string} val - Nilai (email) yang akan dicek
 * @param {cbEmail} cb - Callback yang menangani respon
 */
adminSchema.path('email').validate((val,cb) => {
    AdminModel.find({email:val},(err,docs) => {
        if(docs.length > 0) {
            cb(false,'Email sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai email.
 * @callback cbEmail
 * @param {boolean} status - status true/false
 * @param {string} message - pesan yang akan ditampilkan
 */

 /**
 * Validasi username jika sudah pernah terdaftar sebelumnya
 * @func validateUsername
 * @param {string} val - Nilai (username) yang akan dicek
 * @param {cbUsername} cb - Callback yang menangani respon
 */
adminSchema.path('username').validate((val, cb) => {
    AdminModel.find({username:val},(err,docs) => {
        if(docs.length > 0) {
            cb(false,'Username sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai username.
 * @callback cbUsername
 * @param {boolean} status - status true/false
 * @param {string} message - pesan yang akan ditampilkan
 */

 /**
  * Register dan inisialisasi nama model
  * @const
  * @type {Object.<function>}
  */
const AdminModel = mongoose.model('admin',adminSchema);
/**
 * Export module Admin Model
 * @module AdminModel
 * @see adminSchema
 * @see validateNama
 * @see validateEmail
 * @see validateUsername
 */
/** Call model "AdminModel" */
export default AdminModel;