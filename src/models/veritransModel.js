/**
 * @author Artha Prihardana 
 * @Date 2017-02-25 11:44:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-03-18 11:49:12
 */
import config from '../app.conf';
import mongoose from 'mongoose';

const VertitransSchema = new mongoose.Schema({
    riderId:{type: mongoose.Schema.Types.ObjectId, ref: 'rider'},
    transactionID:{type:String},
    orderId:{type:String},
    grossAmount:{type:Number},
    paymentType:{type:String},
    transactionTime:{type:Date},
    expiredTime:{type:Date},
    transactionStatus:{type:String},
    paymentCode:{type:String}
}, {timestamps: {createdAt: 'created_at',updatedAt: 'updated_at'}});

const VeritransModel = mongoose.model('payment',VertitransSchema);
export default VeritransModel;