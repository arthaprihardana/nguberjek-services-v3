/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 07:51:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-25 06:17:42
 */
import config from '../app.conf';
import validate from '../helpers/validateHelper';
import mongoose from 'mongoose';

/**
 * Skema dokumen (tabel) Rider
 * @const
 * @namespace
 * @type {Object.<string>}
 * @property {string} nama              - Nama
 * @property {string} email             - Email
 * @property {string} noTelp            - No Telepon
 * @property {string} alamat            - Alamat
 * @property {string} jenisKelamin      - Jenis Kelamin
 * @property {string} tempatLahir       - Tempat Lahir
 * @property {date} tglLahir            - Tanggal Lahir
 * @property {string} typeKendaraan     - Tipe Kendaraaan
 * @property {string} merkKendaraan     - Merk Kendaraan
 * @property {string} noPolisi          - No Polisi
 * @property {string} role              - Role
 * @property {string} photo             - Foto
 * @property {string} lokasi            - Lokasi
 * @property {boolean} franchise        - Franchise
 * @property {string} franchiseName     - Franchise Name
 * @property {string} username          - Username
 * @property {string} password          - Password
 * @property {boolean} show             - Show
 * @property {string} deviceName        - Device Name
 * @property {string} deviceId          - Device ID
 * @property {string} senderId          - Sender ID
 * @property {boolean} ready            - Ready
 * @property {number} koordinatTerkahir - Koordinat Terakhir
 * @property {number} saldo             - Saldo
 * @property {string} token             - Token
 * @property {boolean} approve          - Approve
 */
let riderSchema = new mongoose.Schema({
    nama: {type: String, required: true},
    email: {
        type: String,
        required:true,
        lowercase: true,
        unique: true,
        validate: [validate.email, 'Please fill a valid email address']
    },
    noTelp: {
        type:String, 
        required:true,
        validate: [validate.phone, 'Please fill a valid phone number']
    },
    alamat: {type: String},
    jenisKelamin:{type:String},
    tempatLahir:{type:String},
    tglLahir:{type:Date},
    typeKendaraan:{type:String},
    merkKendaraan:{type:String},
    noPolisi:{
        type:String,
        uppercase: true
    },
    role:{
        type:String,
        enum: ['nguberjek', 'ngubertaxi', 'ngubercar'],
        default: 'nguberjek',
        lowercase: true,
    },
    photo:{type:String, default: config.apiUrl+':'+config.apiPort+'/images/default.png'},
    lokasi:{type:String},
    franchise:{type:Boolean, default:false},
    franchiseName:{type: mongoose.Schema.Types.ObjectId, ref:'admin'},
    username:{type:String, required:true},
    password:{type:String,required:true},
    show:{type:Boolean, default:true},
    deviceName:{type:String},
    deviceId:{type:String},
    senderId:{type:String},
    ready:{type:Boolean, default:true},
    koordinatTerakhir:{type:[Number],index:'2dsphere'},
    saldo:{type:Number,default:0},
    token: {type:String},
    approve: {type:Boolean, default: true}
}, {timestamps:{createdAt:'created_at',updatedAt:'updated_at'}});

/**
 * Validasi nama jika sudah pernah terdaftar sebelumnya
 * @func validateNama
 * @param {string} val - Nilai (nama) yang akan dicek
 * @param {cbNama} cb - Callback yang menangani respon
 */
riderSchema.path('nama').validate((val,cb) => {
    RiderModel.find({nama:val},(err,docs) => {
        console.log("docs==>", docs);
        if(docs.length > 0) {
            cb(false,'Nama sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai nama
 * @callback cbNama
 * @param {boolean} status - status true / false
 * @param {string} message - pesan yang akan ditampilkan
 */

/**
 * Validasi email jika sudah pernah terdaftar sebelumnya
 * @func validateEmail
 * @param {string} val - Nilai (email) yang akan dicek
 * @param {cbEmail} cb - Callback yang menangani respon
 */
riderSchema.path('email').validate((val,cb) => {
    RiderModel.find({email:val},(err,docs) => {
        if(docs.length > 0) {
            cb(false,'Email sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai email
 * @callback cbEmail
 * @param {boolean} status - status true / false
 * @param {string} message - pesan yang akan ditampilkan
 */

/**
 * Validasi noTelp jika sudah pernah terdaftar sebelumnya
 * @func validateNoTelp
 * @param {string} val - Nilai (noTelp) yang akan dicek
 * @param {cbNoTelp} cb - Callback yang menangani respon
 */
riderSchema.path('noTelp').validate((val,cb) => {
    RiderModel.find({noTelp:val},(err,docs) => {
        if(docs.length > 0) {
            cb(false,'No telepon sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai noTelp
 * @callback cbNoTelp
 * @param {boolean} status - status true / false
 * @param {string} message - pesan yang akan ditampilkan
 */

/**
 * Validasi username jika sudah pernah terdaftar sebelumnya
 * @func validateUsername
 * @param {string} val - Nilai (username) yang akan dicek
 * @param {cbUsername} cb - Callback yang menangani respon
 */
riderSchema.path('username').validate((val,cb) => {
    RiderModel.find({username:val},(err,docs) => {
        if(docs.length > 0) {
            cb(false,'Username sudah terdaftar');
        } else {
            cb(true);
        }
    });
});
/**
 * Callback akan mengembalikan nilai username.
 * @callback cbUsername
 * @param {boolean} status - status true/false
 * @param {string} message - pesan yang akan ditampilkan
 */

/**
 * Register dan inisialisasi nama model
 * @const
 * @type {Object.<function>}
 */
let RiderModel = mongoose.model('rider', riderSchema);
/**
 * Export module Rider Model
 * @module RiderModel
 * @see riderSchema
 * @see validateNama
 * @see validateEmail
 * @see validateNoTelp
 * @see validateUsername
 */
/** Call model "RiderModel" */
export default RiderModel;