/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 11:38:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-28 11:44:10
 */
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../app';

chai.config.includeStack = true;

describe('## Index Router', () => {
    describe('# GET /api', () => {
        it('should return OK', (done) => {
        request(app)
            .get('/api')
            .expect(httpStatus.OK)
            .then(res => {
            expect(res.body.data.apiName).to.equal('REST API NGUBERJEK VERSION 1.0.0');
            done();
            });
        });
    });
});