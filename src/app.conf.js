/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 12:21:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-04-06 23:55:24
 */
export default {
    // apiUrl: 'http://localhost',
    apiUrl: 'http://159.203.109.20',    // production
    // apiUrl: 'http://188.166.174.221',    // development
    // apiUrl: 'http://103.28.23.67',    // kejar server
    apiPort: 8080,
    apiVersion: '/api/v3',
    db: 'mongodb://nguberjek:nguberjek2016@localhost:27017/nguberjek',   // production
    // db: 'mongodb://nguberjek:nguberjek2016@159.203.109.20:27017/nguberjek',
    // db: 'mongodb://localhost:27017/nguberjek',  // development
    // db: 'mongodb://localhost:27017/abang',  // kejar
    dbAuth: {
        useMongoClient: true,
        autoIndex: false, // Don't build indexes
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        reconnectInterval: 500, // Reconnect every 500ms
        poolSize: 10, // Maintain up to 10 socket connections
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0


        /** deprecated */
        // db: { native_parser: true },
        // server: { poolSize: 5 },
        // replset: { rs_name: 'nguberjek_replica' },
        // user: 'nguberjek',
        // pass: 'nguberjek2016'
        /** deprecated */
    },
    limitImageSize: 3000000,
    radius: 1.24274,  // satuan panjang dalam miles (saat ini adalah 2 km)
    radiusResto: 3.10686,   // satuan panjang dalam miles (saat ini adalah 5 km)
    interval: {
        radius: 5000
    },
    secretKey: '3fbf3eacee7eea63ffe58019d1b26ecd',
    firebaseApiKey: '',
    // Dev Key : AIzaSyAFNcrmqn3G7LhG8IHmzPYWGw6KLq0CL0g
    // Prod Key : AIzaSyD6Yfay3l3YDs_8n4ymeoLuk9GNI_SgXCE
    googleApiKey: 'AIzaSyD6Yfay3l3YDs_8n4ymeoLuk9GNI_SgXCE',
    smtpConfig: {
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'nguberjek@gmail.com',
            pass: 'murahberhadiah1'
        },
        tls:{
            rejectUnauthorized: false
        }
    }
}