/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 12:34:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-05-11 13:16:41
 */
import moment from 'moment';

export default class Tanggal {
    constructor(tanggal) {
        this.tanggal = (tanggal != undefined) ? ((typeof tanggal === "object") ? tanggal : new Date(tanggal)) : new Date();
    }

    defaultFormat() {
        return moment(this.tanggal).locale('id').format();
    }

    /**
     * @function formatLong
     * @desc Format tanggal lengkap contoh : Desember 16 2016 3:45:00 sore
     * @returns {string} Desember 16 2016 3:45:00 sore
     * 
     * @memberOf Tanggal
     */
    formatLong() {
        return moment(this.tanggal).locale('id').format('Do MMMM YYYY, h:mm:ss a');
    }

    /**
     * @function getDay
     * @desc Nama hari
     * @returns {string} Jumat
     * 
     * @memberOf Tanggal
     */
    getDay() {
        return moment(this.tanggal).locale('id').format('dddd');
    }

    /**
     * @function formatShort
     * @desc Format tanggal pendek
     * @returns {string} Des 16 16
     * 
     * @memberOf Tanggal
     */
    formatShort() {
        return moment(this.tanggal).locale('id').format('Do MMMM YYYY');
    }

    /**
     * @function relative
     * @desc Menampilkan waktu yang lalu
     * @returns {string} 5 tahun yang lalu
     * 
     * @memberOf Tanggal
     */
    relative() {
        return moment(this.tanggal, 'YYYYMMDD').locale('id').fromNow();
    }

    /**
     * @function startOfDay
     * @desc Waktu dimulai nya tanggal
     * @returns {string} 16 jam yang lalu
     * 
     * @memberOf Tanggal
     */
    startOfDay() {
        return moment(this.tanggal).locale('id').startOf('day').fromNow();
    }

    /**
     * @function endOfDay
     * @desc Waktu berakhirnya tanggal
     * @returns {string} dalam 8 jam
     * 
     * @memberOf Tanggal
     */
    endOfDay() {
        return moment(this.tanggal).locale('id').endOf('day').fromNow();
    }

    /**
     * @function startOfHour
     * @desc Dimulai nya hitungan jam
     * @returns {string} 16 jam yang lalu
     * 
     * @memberOf Tanggal
     */
    startOfHour() {
        return moment(this.tanggal).locale('id').startOf('hour').fromNow();
    }

    /**
     * @function calendarSub
     * @desc Tanggal sekarang dikurangi jumlah hari
     * @param {any} day
     * @returns {string} sabtu lalu pukul 16.01
     * 
     * @memberOf Tanggal
     */
    calendarSub(day) {
        return moment(this.tanggal).locale('id').subtract(day, 'days').calendar();
    }

    /**
     * @function calendarAdd
     * @desc Tanggal sekarang ditambah jumlah hari
     * @param {any} day
     * @returns {string} Sabtu pukul 22.01
     * 
     * @memberOf Tanggal
     */
    calendarAdd(day) {
        return moment(this.tanggal).locale('id').add(day, 'days').calendar();
    }

    /**
     * @function localeLT
     * @desc Menampilkan jam
     * @returns {string} 16.00
     * 
     * @memberOf Tanggal
     */
    localeLT() {
        return moment(this.tanggal).locale('id').format('LT');
    }

    /**
     * @function localeLTS
     * @desc MEnampilkan jam beserta detiknya
     * @returns {string} 16.00.01
     * 
     * @memberOf Tanggal
     */
    localeLTS() {
        return moment(this.tanggal).locale('id').format('LTS');
    }

    /**
     * @function localeL
     * @desc Menampilkan tanggal dengan 2 digit angka
     * @returns {string} 03/11/2016
     * 
     * @memberOf Tanggal
     */
    localeL() {
        return moment(this.tanggal).locale('id').format('L');
    }

    /**
     * @function localel
     * @desc Menampilkan tanggal dengan 1 digit angka
     * @returns {string} 3//11/2016
     * 
     * @memberOf Tanggal
     */
    localel() {
        return moment(this.tanggal).locale('id').format('l');
    }

    /**
     * @function localeLL
     * @desc Menampilkan tanggal lengkap
     * @returns {string} 3 November 2016
     * 
     * @memberOf Tanggal
     */
    localeLL() {
        return moment(this.tanggal).locale('id').format('LL');
    }

    /**
     * @function localell
     * @desc Menampilkan tanggal dengan format bulan pendek
     * @returns {string} 3 Nov 2016
     * 
     * @memberOf Tanggal
     */
    localell() {
        return moment(this.tanggal).locale('id').format('ll');
    }

    /**
     * @function localeLLL
     * @desc Menampilkan tanggal beserta waktu
     * @returns {string} 3 November 2016 16.04
     * 
     * @memberOf Tanggal
     */
    localeLLL() {
        return moment(this.tanggal).locale('id').format('LLL');
    }

    /**
     * @function localelll
     * @desc Menampilkan tanggal beserta waktu dengan format bulan pendek
     * @returns {string} 3 Nov 2016 16.04
     * 
     * @memberOf Tanggal
     */
    localelll() {
        return moment(this.tanggal).locale('id').format('lll');
    }

    /**
     * @function localeLLLL
     * @desc Menampilkan hari, tanggal dan jam dengan format panjang
     * @returns {string} Kamis 3 November 2016 16.04
     * 
     * @memberOf Tanggal
     */
    localeLLLL() {
        return moment(this.tanggal).locale('id').format('LLLL');
    }

    /**
     * @function localellll
     * @desc Menampilkan hari, tanggal dan jam dengan format pendek
     * @returns {string} Kam, 3 Nov 2016 16.04
     * 
     * @memberOf Tanggal
     */
    localellll() {
        return moment(this.tanggal).locale('id').format('llll');
    }    

    startOf(string) {
        let mulai = moment().startOf(string);
        return mulai.toDate();
    } 
}