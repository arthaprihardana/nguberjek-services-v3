/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 23:48:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-27 00:14:40
 */
import config from '../app.conf';
import adminController from '../controllers/adminController';
import boncengerController from '../controllers/boncengerController';
import { realtimePosition } from '../controllers/driverController';
import { sendNotification } from './firebaseHelper';
import mongoose from 'mongoose';
import request from 'request';

// const objectID = mongoose.Types.ObjectId;

var setIntervalAndExecute = (fn, t) => {
    fn();
    return (setInterval(fn, t));
}
let queue;

var shuffle = (array) => {
    var copy = [], n = array.length, i;

    // While there remain elements to shuffle…
    while (n) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * array.length);

        // If not already shuffled, move it to the new array.
        if (i in array) {
            copy.push(array[i]);
            delete array[i];
            n--;
        }
    }

    return copy;
}

export default (io) => {
    io.on('disconnect', function(){
        io.connect();
    });
    io.on('connection', function (socket) {
        console.info('Socket connected');

        // socket gps driver
        socket.on('client-request: gps driver', (data) => {
            let set = realtimePosition(data._id, data.koordinat);
            set
                .then((response) => {
                    console.info('update koordinat==>', response);
                    socket.emit('server-response: gps driver', {
                        s:1,
                        message: 'koordinat anda up-to-date', 
                    });
                })
                .catch((err) => {
                    console.error('error update koordinat==>', err);
                    socket.emit('server-response: gps driver', {
                        s:0,
                        message: 'koordinat tidak up-to-date karena masalah koneksi', 
                    });
                });
        });

        // socket get rider dalam radius
        socket.on('client-request: radius driver', (data) => {
            let options = { 
                method: 'GET',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/radius/driver',
                qs: { 
                    koordinat: data.koordinat, 
                    role: data.role
                },
                headers: { 
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: radius driver', body);
                    }
                    console.log('radius driver dari socket ==> ', body)
                    socket.emit('server-response: radius driver', body);
                });
            }
            req();
            // setInterval(() => {
            //     req();
            // }, config.interval.radius);
        });

        // socket request order dari boncenger
        socket.on('client-request: order request', (data) => {
            console.log('ada order request');
            let options = { 
                method: 'POST',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/requestOrder',
                headers: { 
                    'content-type': 'application/x-www-form-urlencoded',
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                },
                form: {
                    boncengerId: data.boncengerId,
                    lokasiAwal: data.lokasiAwal,
                    koordinatLokasi: data.koordinatLokasi,
                    keteranganLokasiAwal: data.keteranganLokasiAwal,
                    tujuan: data.tujuan,
                    koordinatTujuan: data.koordinatTujuan,
                    keteranganLokasiAkhir: data.keteranganLokasiAkhir,
                    harga: data.harga,
                    jarak: data.jarak,
                    waktu: data.waktu,
                    kategori: data.kategori
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: order request', body);
                    }
                    // console.log('ada respon');
                    let data = JSON.parse(body);
                    let dataOrder = data.dataOrder;
                    let driverList = shuffle(data.driverList);
                    queue = setIntervalAndExecute(()=>{
                        console.log('====================================');
                        console.log('kesini');
                        console.log('====================================');
                        if(driverList.length > 0) {
                            let shiftDriver = driverList.shift();
                            // console.log("orderId==>", dataOrder.orderId);
                            // console.log('kirim notifikasi');
                            console.log('====================================');
                            console.log('shift driver==>', shiftDriver);
                            console.log('====================================');
                            sendNotification({
                                title: "request order",
                                status: 0,
                                message: "permintan order dari "+dataOrder.lokasiAwal.lokasi+" ke tujuan "+dataOrder.lokasiAkhir.tujuan,
                                orderId: dataOrder.orderId,
                                // expired: 20000 
                            }, shiftDriver.senderId, (gcmresponse) => {
                                shiftDriver.orderId = dataOrder.orderId;
                                socket.emit('server-response: order request', {
                                    s:1,
                                    message: "Kami sedang mencari driver untuk anda",
                                    data: shiftDriver
                                });
                            });
                        } else {
                            socket.emit('server-response: order request', {
                                s:0,
                                message: "Mohon maaf, driver kami tidak ada yang merespon. Ulangi Pencarian??"
                            });
                            clearInterval(queue);
                        }
                    }, config.interval.radius);
                });
            }
            req();
        });

        // socket accept order dari driver
        socket.on('client-request: confirm order', (data) => {
            // console.log('confirm order dari adi', data);
            if(data.status_confirm == "accept") {
                // clearInterval(queue);
                console.log('data==>', data);
                let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/orderDetail', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
                request(options, (error, response, body) => {
                    let odt = JSON.parse(body);
                    console.log('ini response request', odt)
                    if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                        switch (odt.dt[0].status) {
                            case 0: 
                                clearInterval(queue);
                                break;
                            case 1: 
                                clearInterval(queue);
                                socket.emit('server-response: confirm order', {
                                    s: 0,
                                    msg: "Order ini telah dibatalkan oleh penumpang"
                                }); 
                                break;
                            case 6:
                                console.log('kesini yaa: batal order');
                                socket.emit('server-response: confirm order', {
                                    s: 0,
                                    msg: "Order ini telah dibatalkan oleh penumpang"
                                }); 
                                break;
                            default:
                                console.log('kesini yaa: order udah diterima sama driver lain');
                                socket.emit('server-response: confirm order', {
                                    s: 0,
                                    msg: "Order ini telah diterima oleh driver lain"
                                }); 
                                break;
                        }
                    } else {
                        // clearInterval(queue);
                        console.log("order diterima", odt);
                        let options2 = { 
                            method: 'POST',
                            url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/acceptOrder',
                            headers: { 
                                'content-type': 'application/x-www-form-urlencoded',
                                'cache-control': 'no-cache',
                                'x-access-token': data.token
                            },
                            form: { 
                                orderId: data.order_id, 
                                driverId: data.rider_id 
                            } 
                        };
                        request(options2, function (error2, response2, body2) {
                            if (error) {
                                // socket.emit('server-response: accept order', body);
                                socket.emit('server-response: confirm order', body2);
                            }

                            console.log('request untuk update order, ', body2);
                            let odt2 = JSON.parse(body2);
                            console.log('socket emit confirm order ==>', odt2.dt.orderId);
                            socket.emit('server-response: confirm order', {
                                order_id: odt2.dt.orderId,
                                rider_id: odt2.dt.rider._id,
                                boncenger_id: odt2.dt.boncenger,
                                status_confirm: "accept"
                            });
                            // sendNotification({
                            //     title: "accept order",
                            //     status: 1,
                            //     message: "penerimaan order dengan perjalanan dari "+odt2.dt.lokasiAwal.lokasi+" dengan tujuan "+odt2.dt.lokasiAkhir.tujuan,
                            //     orderId: odt2.dt.orderId,
                            // }, odt2.dt.boncenger.senderId, (gcmresponse) => {
                            //     console.log('kirim emit ke boncenger');
                            // });
                        });
                    }
                });

                // fungsi yang berjalan (sudah benar)
                // clearInterval(queue);
                // console.log("order diterima", data);
                // // console.log("driverId==>", data.driverId);
                // let options = { 
                //     method: 'POST',
                //     url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/acceptOrder',
                //     headers: { 
                //         'content-type': 'application/x-www-form-urlencoded',
                //         'cache-control': 'no-cache',
                //         'x-access-token': data.token
                //     },
                //     form: { 
                //         orderId: data.order_id, 
                //         driverId: data.rider_id 
                //     } 
                // };
                // request(options, function (error, response, body) {
                //     if (error) {
                //         // socket.emit('server-response: accept order', body);
                //         socket.emit('server-response: confirm order', body);
                //     }

                //     console.log('request untuk update order, ', body);
                //     let data = JSON.parse(body);
                //     console.log('socket emit confirm order ==>', data.dt.orderId);
                //     // console.log("boncenger==>", data.data.boncengerId);
                //     // console.log("driver==>", data.data.driverId);
                //     // console.log("data==>", data);
                //     // socket.emit('server-response: accept order', {
                //     //     s:1,
                //     //     orderId: data.dt.orderId,
                //     //     boncenger: {
                //     //         message: "Kami telah menemukan driver untuk anda",
                //     //         data: data.dt.rider
                //     //     },
                //     //     driver: {
                //     //         message: "Anda telah menerima order dari boncenger",
                //     //         data: data.dt.boncenger
                //     //     }
                //     // });
                //     sendNotification({
                //         title: "accept order",
                //         status: 1,
                //         message: "penerimaan order oleh driver dengan perjalanan dari "+data.dt.lokasiAwal.lokasi+" dengan tujuan "+data.dt.lokasiAkhir.tujuan,
                //         orderId: data.dt.orderId,
                //         // expired: 20000 
                //     }, data.dt.boncenger.senderId, (gcmresponse) => {
                //         // console.log("gcm response==>", JSON.stringify(gcmresponse));
                //         console.log('kirim emit ke boncenger');
                //         socket.emit('server-response: confirm order', {
                //             order_id: data.dt.orderId,
                //             rider_id: data.dt.rider._id, 
                //             status_confirm: "accept"
                //         }); 
                //     });
                // });
            } else {
                socket.emit('server-response: confirm order', {
                    s: 0,
                    msg: "penerimaan order ditolak"
                }); 
            }
        });

        // socket reject order dari driver
        socket.on('client-request: reject order', (data) => {
            socket.emit('server-response: reject order', {
                s:1,
                message: "Anda telah menolak untuk menerima order ini"
            });
            if(driverList.length > 0) {
                let shiftDriver = driverList.shift();
                sendNotification({
                    title: "request order",
                    status: 0,
                    message: "permintan order dari "+dataOrder.lokasiAwal.lokasi+" ke tujuan "+dataOrder.lokasiAkhir.tujuan,
                    orderId: dataOrder.orderId,
                    // expired: 20000 
                }, shiftDriver.senderId, (gcmresponse) => {
                    // console.log("gcm response==>", JSON.stringify(gcmresponse));
                    socket.emit('server-response: order request', {
                        s:1,
                        message: "Kami sedang mencari driver untuk anda",
                        data: shiftDriver
                    });
                });
            } else {
                socket.emit('server-response: order request', {
                    s:0,
                    message: "Mohon maaf, driver kami tidak ada yang merespon. Ulangi Pencarian??"
                });
                clearInterval(queue);
            }
        });

        // socket cancel order dari boncenger
        socket.on('client-request: cancel order', (data) => {
            clearInterval(queue);
            socket.emit('server-response: cancel order', {
                s:1,
                message: "Penumpang telah membatalkan order"
            });
        });
        
        // socket cek status
        socket.on('client-request: order status', (data) => {
            let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/orderDetail', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
            request(options, (error, response, body) => {
                console.log('====================================');
                console.log('typeof', typeof body);
                console.log('====================================');

                if(body != undefined) {
                    let odt =  JSON.parse(body);
                    if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                        if(odt.dt[0].status == 1) {
                            console.log('====================================');
                            console.log('odt==>', odt);
                            console.log('====================================');
                            socket.emit("server-response: order status", {
                                title: "status order",
                                status: 1,
                                message: "penerimaan order dengan perjalanan dari "+odt.dt[0].lokasiAwal+" dengan tujuan "+odt.dt[0].lokasiTujuan,
                                orderId: odt.dt[0].orderId,
                            })
                        }
                    }
                }
            })
        });

        // socket update pendapatan order
        socket.on('client-request: pendapatan order', (data) => {
            let options = { 
                method: 'GET',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/pendapatanOrder',
                qs: { driverId: data.driverId, rentang: data.rentang },
                headers: { 
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: pendapatan order', body);
                    }
                    socket.emit('server-response: pendapatan order', body);
                });
            }
            req();
        });

        // socket update pendapatan selfie order
        socket.on('client-request: pendapatan selfie order', (data) => {
            let options = { 
                method: 'GET',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/pendapatanSelfieOrder',
                qs: { driverId: data.driverId, rentang: data.rentang },
                headers: { 
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: pendapatan selfie order', body);
                    }
                    socket.emit('server-response: pendapatan selfie order', body);
                });
            }
            req();
        });

        // socket update rating driver
        socket.on('client-request: rating driver', (data) => {
            let options = { 
                method: 'GET',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/ratingDriver',
                qs: { driverId: data.driverId },
                headers: { 
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: rating driver', body);
                    }
                    socket.emit('server-response: rating driver', body);
                });
            }
            req();
        });

        /** FOOD ORDER START HERE */
        // FOOD ORDER REQUEST
        socket.on('client-request: food order request', (data) => {
            console.log('food order request ==> ', data);
            let options = { 
                method: 'POST',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/requestFoodOrder',
                headers: { 
                    'content-type': 'application/x-www-form-urlencoded',
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                },
                form: {
                    namaResto: data.namaResto,
                    koordinatResto: data.koordinatResto,
                    namaMenu: data.namaMenu,
                    koordinatPengantaran: data.koordinatPengantaran,
                    alamatPengantaran: data.alamatPengantaran,
                    jarak: data.jarak,
                    waktu: data.waktu,
                    perkiraanHarga: data.perkiraanHarga,
                    biayaAntar: data.biayaAntar,
                    totalHarga: data.totalHarga,
                    boncengerId: data.boncengerId,
                    // rating: '',
                    // komentar: '',
                    // status: '0'
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: food order request', body);
                        // console.log('error ==> ')
                    }
                    // console.log('ada respon', response);
                    console.log('body food order==> ', body)
                    let data = JSON.parse(body);
                    let dataOrder = data.dataOrder;
                    let driverList = shuffle(data.driverList);
                    queue = setIntervalAndExecute(()=>{
                        console.log('====================================');
                        console.log('driver food ==> ', driverList);
                        console.log('driver food ==> ', driverList.length)
                        console.log('====================================');
                        if(driverList.length > 0) {
                            let shiftDriver = driverList.shift();
                            // console.log("orderId==>", dataOrder.orderId);
                            // console.log('kirim notifikasi');
                            console.log('====================================');
                            console.log('shift driver==>', shiftDriver);
                            console.log('====================================');
                            sendNotification({
                                title: "request order",
                                status: 0,
                                message: "permintan pesanan makanan",
                                orderId: 'food_'+dataOrder.orderId,
                                // expired: 20000 
                            }, shiftDriver.senderId, (gcmresponse) => {
                                shiftDriver.orderId = dataOrder.orderId;
                                socket.emit('server-response: food order request', {
                                    s:1,
                                    message: "Kami sedang mencari driver untuk anda",
                                    data: shiftDriver
                                });
                            });
                        } else {
                            socket.emit('server-response: order request', {
                                s:0,
                                message: "Mohon maaf, driver kami tidak ada yang merespon. Ulangi Pencarian??"
                            });
                            clearInterval(queue);
                        }
                    }, config.interval.radius);
                });
            }
            req();
        });
        // ACCEPT FOOD ORDER
        socket.on('client-request: confirm food order', (data) => {
            // console.log('confirm order dari adi', data);
            if(data.status_confirm == "accept") {
                // clearInterval(queue);
                console.log('confirm food order==>', data);
                let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/riwayatFoodOrder', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
                request(options, (error, response, body) => {
                    let odt = JSON.parse(body);
                    console.log('ini response confirm', odt)
                    if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                        switch (odt.dt[0].status) {
                            case 0: 
                                clearInterval(queue);
                                break;
                            case 1: 
                                clearInterval(queue);
                                socket.emit('server-response: confirm food order', {
                                    s: 0,
                                    msg: "Order ini telah dibatalkan oleh driver"
                                }); 
                                break;
                            case 6:
                                console.log('kesini yaa: batal order');
                                socket.emit('server-response: confirm food order', {
                                    s: 0,
                                    msg: "Order ini telah dibatalkan oleh penumpang"
                                }); 
                                break;
                            default:
                                console.log('kesini yaa: order udah diterima sama driver lain');
                                socket.emit('server-response: confirm food order', {
                                    s: 0,
                                    msg: "Order ini telah diterima oleh driver lain"
                                }); 
                                break;
                        }
                    } else {
                        // clearInterval(queue);
                        console.log("order diterima", odt);
                        let options2 = { 
                            method: 'POST',
                            url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/acceptFoodOrder',
                            headers: { 
                                'content-type': 'application/x-www-form-urlencoded',
                                'cache-control': 'no-cache',
                                'x-access-token': data.token
                            },
                            form: { 
                                orderId: data.order_id, 
                                driverId: data.rider_id 
                            } 
                        };
                        request(options2, function (error2, response2, body2) {
                            if (error) {
                                // socket.emit('server-response: accept order', body);
                                socket.emit('server-response: confirm food order', body2);
                            }

                            console.log('request untuk update order, ', body2);
                            let odt2 = JSON.parse(body2);
                            console.log('socket emit confirm order ==>', odt2.dt.orderId);
                            socket.emit('server-response: confirm food order', {
                                order_id: odt2.dt.orderId,
                                rider_id: odt2.dt.rider._id,
                                boncenger_id: odt2.dt.boncenger,
                                status_confirm: "accept"
                            });
                        });
                    }
                });
            } else {
                socket.emit('server-response: confirm food order', {
                    s: 0,
                    msg: "penerimaan order ditolak"
                }); 
            }
        });
        // STATUS FOOD ORDER
        socket.on('client-request: order food status', (data) => {
            let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/riwayatFoodOrder', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
            request(options, (error, response, body) => {
                console.log('====================================');
                console.log('typeof order food', typeof body);
                console.log('====================================');

                if(typeof body != undefined) {
                    let odt =  JSON.parse(body);
                    if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                        if(odt.dt[0].status == 1) {
                            console.log('====================================');
                            console.log('jika ada order food==>', odt);
                            console.log('====================================');
                            socket.emit("server-response: order food status", {
                                title: "status order food",
                                status: 1,
                                message: "Penerimaan order makanan",
                                orderId: "food_"+odt.dt[0].orderId,
                            })
                        }
                    }
                }
            })
        });
        /** FOOD ORDER END HERE  */

    });
}