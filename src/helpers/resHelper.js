/**
 * @author Artha Prihardana 
 * @Date 2017-02-04 09:08:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-02-04 10:10:59
 */
export default (status, statusCode, message, option, response, formData, data, error, token, expires) => {
    let stat = {};
    stat['s'] = status;
    stat['statusCode'] = statusCode;
    stat['msg'] = message;
    if(token) {
        stat['token'] = token;
    }
    if(expires) {
        stat['expires'] = expires;
    }
    if(option) {
        stat['opt'] = option;
    }
    if(response) {
        stat['response'] = response;
    }
    if(formData) {
        stat['formData'] = formData;
    }
    if(data) {
        stat['dt'] = data;
    }
    if(error) {
        stat['err'] = error;
    }
    return stat;
};