/**
 * @author Artha Prihardana 
 * @Date 2017-02-25 16:57:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-05 05:46:35
 */
import request from 'request';
import btoa from 'btoa'

const vtCall = (pth,method,prm,cb) => {
    // let auth = btoa('VT-server-qnBdqY1co_7V9itlf0CD-5fd:')      // sandbox
    let auth = btoa('VT-server-56q0II1E6IvQOPff6FaYEQXb:')      // live
    let opts={
        method: method,
        headers:{
            'cache-control': 'no-cache',
            authorization: 'Basic '+auth,
            accept: 'application/json',
            'content-type': 'application/json'
        },
        // url: 'https://api.sandbox.midtrans.com/v2/'+ pth,  // sandbox
        url: 'https://api.midtrans.com/v2/' + pth    // live
    };
    prm=prm||{};
    for(let i in prm){
        opts[i]=prm[i];
    }
    request(opts,function (err, res, body){
        if (err) {
            return cb(err);
        }
        return cb(null,body);
    });
}

export const chargeHelper = (data, cb) => {
    vtCall('charge', 'post', {
        body: data,
        json: true
    }, cb);
}

export const statusHelper = (orderId, cb) => {
    vtCall(orderId+'/status', 'get', {}, cb);
}

export const notificationHelper = (data, cb) => {
    if(data.transaction_status=="settlement") {
        console.log("sukes melakukan pembayaran dengan type "+data.payment_type);
        cb(null,{message:"sukes melakukan pembayaran dengan type "+data.payment_type});
    } else if(data.transaction_status=="pending") {
        console.log("menunggu untuk melakukan pembayaran dengan type "+data.payment_type);
        cb(null,{message:"menunggu untuk melakukan pembayaran dengan type "+data.payment_type});
    } else if(data.transaction_status=="deny") {
        console.log("pembayaran dengan type "+data.payment_type+" ditolak");
        cb(null,{message:"pembayaran dengan type "+data.payment_type+" ditolak"});
    } else if(data.transaction_status=="expire") {
        console.log("pembayaran dengan type "+data.payment_type+" kedaluwarsa");
        cb(null,{message:"pembayaran dengan type "+data.payment_type+" kedaluwarsa"});
    } else if(data.transaction_status=="cancel") {
        console.log("pembayaran dengan type "+data.payment_type+" dibatalkan");
        cb(null,{message:"pembayaran dengan type "+data.payment_type+" dibatalkan"});
    }
}