/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 12:40:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-28 21:47:24
 */
import config from '../app.conf';
import jwt from 'jwt-simple';
import bCrypt from 'bcrypt-nodejs';

export default class Auth {
    constructor(username, password) {
        this.username = username;
        this.password = password;
    }

    setExpired(num) {
        let date = new Date();
        this.expired = date.setMinutes(date.getMinutes() + num);
        return this.expired;
    }

    createHash() {
        return bCrypt.hashSync(this.password, bCrypt.genSaltSync(10), null);
    }

    compareSync(passwordFromDb) {
        return bCrypt.compareSync(this.password, passwordFromDb);
    }

    genToken() {
        let self = this;
        let expires = self.setExpired(720);
        let token = jwt.encode({
            exp: expires
        }, config.secretKey);
        return {
            token: token,
            expires: expires
        };
    }

    decode(token) {
        return jwt.decode(token, config.secretKey);
    }

}