/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 19:56:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 06:57:42
 */
export default class ObjectHelper {
    constructor(obj, key, val) {
        this.obj = obj;
        this.key = key;
        this.val = val;
    }

    objekFilter() {
        let filter = {};
        for (let i in this.obj) {
            if (this.key.indexOf(i) >= 0) continue;
            if (!Object.prototype.hasOwnProperty.call(this.obj, i)) continue;
            filter[i] = this.obj[i];
        }
        return filter;
    }

    objectGrouping() {
        let i = 0, val, index, values = [], result = [];
        for (; i < collection.length; i++) {
            val = collection[i][property];
            index = values.indexOf(val);
            if (index > -1)
                result[index].push(collection[i]);
            else {
                values.push(val);
                result.push([collection[i]]);
            }
        }
        return result;
    }
}