/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 19:56:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-30 14:14:05
 */

export default class GenerateHelper {
    constructor() {
        this.string = '';
    }

    setVtOrderId() {
        let possible = 'abcdefghijklmnopqrstuvwxyz0123456789';
        for( let i=0; i < 8; i++ ) {
            this.string += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    getVtOrderId() {
        return this.string;
    }

    setVtPaymentCode() {
        let possible = '0123456789';
        for( let i=0; i < 12; i++ ) {
            this.string += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    getVtPaymentCode() {
        return this.string;
    }

    setOrderId() {
        let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for( let i=0; i < 10; i++ ) {
            this.string += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    getOrderId() {
        return this.string;
    }

    setRandomPassword() {
        let possible = "0123456789";
		for( let i=0; i < 5; i++ ) {
            this.string += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    getRandomPassword() {
        return this.string;
    }

    setRandomNameImage() {
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( let i=0; i < 10; i++ ) {
            this.string += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }

    getRandomNameImage() {
        return 'UserPhoto-'+this.string;
    }
}