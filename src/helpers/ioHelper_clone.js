/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 23:48:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-07-08 01:08:04
 */
import config from '../app.conf';
import adminController from '../controllers/adminController';
import boncengerController from '../controllers/boncengerController';
import { realtimePosition } from '../controllers/driverController';
import { sendNotification } from './firebaseHelper';
import mongoose from 'mongoose';
import request from 'request';

// const objectID = mongoose.Types.ObjectId;

var setIntervalAndExecute = (fn, t) => {
    fn();
    return (setInterval(fn, t));
}
let queue;

var shuffle = (array) => {
    var copy = [], n = array.length, i;

    // While there remain elements to shuffle…
    while (n) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * array.length);

        // If not already shuffled, move it to the new array.
        if (i in array) {
            copy.push(array[i]);
            delete array[i];
            n--;
        }
    }

    return copy;
}

export default (io) => {
    io.on('connection', function (socket) {
        console.info('Socket connected');

        // socket request order dari boncenger
        socket.on('client-request: order request', (data) => {
            console.log('ada order request');
            let options = { 
                method: 'POST',
                url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/requestOrder',
                headers: { 
                    'content-type': 'application/x-www-form-urlencoded',
                    'cache-control': 'no-cache',
                    'x-access-token': data.token
                },
                form: {
                    boncengerId: data.boncengerId,
                    lokasiAwal: data.lokasiAwal,
                    koordinatLokasi: data.koordinatLokasi,
                    keteranganLokasiAwal: data.keteranganLokasiAwal,
                    tujuan: data.tujuan,
                    koordinatTujuan: data.koordinatTujuan,
                    keteranganLokasiAkhir: data.keteranganLokasiAkhir,
                    harga: data.harga,
                    jarak: data.jarak,
                    waktu: data.waktu,
                    kategori: data.kategori
                } 
            };
            let req = () => {
                request(options, (error, response, body) => {
                    if (error) {
                        socket.emit('server-response: order request', body);
                    }
                    let data = JSON.parse(body);
                    let dataOrder = data.dataOrder;
                    let driverList = shuffle(data.driverList);
                    queue = setIntervalAndExecute(()=>{
                        if(driverList.length > 0) {
                            let shiftDriver = driverList.shift();
                            sendNotification({
                                title: "request order",
                                status: 0,
                                message: "permintan order dari "+dataOrder.lokasiAwal.lokasi+" ke tujuan "+dataOrder.lokasiAkhir.tujuan,
                                orderId: dataOrder.orderId
                            }, shiftDriver.senderId, (gcmresponse) => {
                                shiftDriver.orderId = dataOrder.orderId;
                                socket.emit('server-response: order request', {
                                    s:1,
                                    message: "Kami sedang mencari driver untuk anda",
                                    data: shiftDriver
                                });
                            });
                        } else {
                            socket.emit('server-response: order request', {
                                s:0,
                                message: "Mohon maaf, driver kami tidak ada yang merespon. Ulangi Pencarian??"
                            });
                            clearInterval(queue);
                        }
                    }, 30000);
                });
            }
            req();
        });

        // socket accept order dari driver
        socket.on('client-request: confirm order', (data) => {
            if(data.status_confirm == "accept") {
                clearInterval(queue);
                let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/orderDetail', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
                request(options, (error, response, body) => {
                    let odt = JSON.parse(body);
                    if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                        switch (odt.dt[0].status) {
                            case 6:
                                socket.emit('server-response: confirm order', {
                                    s: 0,
                                    msg: "Order ini telah dibatalkan oleh penumpang"
                                }); 
                                break;
                            default:
                                socket.emit('server-response: confirm order', {
                                    s: 0,
                                    msg: "Order ini telah diterima oleh driver lain"
                                }); 
                                break;
                        }
                        
                    } else {
                        let options2 = { 
                            method: 'POST',
                            url: config.apiUrl+':'+config.apiPort+'/api/v3/ws/acceptOrder',
                            headers: { 
                                'content-type': 'application/x-www-form-urlencoded',
                                'cache-control': 'no-cache',
                                'x-access-token': data.token
                            },
                            form: { 
                                orderId: data.order_id, 
                                driverId: data.rider_id 
                            } 
                        };
                        request(options2, function (error2, response2, body2) {
                            if (error) {
                                socket.emit('server-response: confirm order', body2);
                            }
                            console.log('====================================');
                            console.log('request untuk update order, ', body2);
                            console.log('====================================');
                            let odt2 = JSON.parse(body2);
                            socket.emit('server-response: confirm order', {
                                order_id: odt2.dt.orderId,
                                rider_id: odt2.dt.rider._id,
                                boncenger_id: odt2.dt.boncenger,
                                status_confirm: "accept"
                            });

                            sendNotification({
                                title: "accept order",
                                status: 1,
                                message: "penerimaan order dengan perjalanan dari "+odt2.dt.lokasiAwal.lokasi+" dengan tujuan "+odt2.dt.lokasiAkhir.tujuan,
                                orderId: odt2.dt.orderId
                            }, odt2.dt.boncenger.senderId, (gcmresponse) => {
                                console.log('kirim emit ke boncenger');
                                socket.emit('server-response: confirm order', {
                                    s:1,
                                    message: "Order telah diterima oleh driver",
                                    rider_id: odt2.dt.rider._id,
                                    status: 1,
                                    title: "accept order",
                                    orderId: odt2.dt.orderId
                                });
                            });
                        });
                    }
                });
            } else {
                socket.emit('server-response: confirm order', {
                    s: 0,
                    msg: "penerimaan order ditolak"
                }); 
            }
        });

        // socket reject order dari driver
        socket.on('client-request: reject order', (data) => {
            socket.emit('server-response: reject order', {
                s:1,
                message: "Anda telah menolak untuk menerima order ini"
            });
            if(driverList.length > 0) {
                let shiftDriver = driverList.shift();
                sendNotification({
                    title: "request order",
                    status: 0,
                    message: "permintan order dari "+dataOrder.lokasiAwal.lokasi+" ke tujuan "+dataOrder.lokasiAkhir.tujuan,
                    orderId: dataOrder.orderId
                }, shiftDriver.senderId, (gcmresponse) => {
                    socket.emit('server-response: order request', {
                        s:1,
                        message: "Kami sedang mencari driver untuk anda",
                        data: shiftDriver
                    });
                });
            } else {
                socket.emit('server-response: order request', {
                    s:0,
                    message: "Mohon maaf, driver kami tidak ada yang merespon. Ulangi Pencarian??"
                });
                clearInterval(queue);
            }
        });

        // socket cancel order dari boncenger
        socket.on('client-request: cancel order', (data) => {
            clearInterval(queue);
            socket.emit('server-response: cancel order', {
                s:1,
                message: "Penumpang telah membatalkan order"
            });
        });

        // socket cek status
        socket.on('client-request: order status', (data) => {
            console.log('====================================');
            console.log('kesini cek status order');
            console.log('====================================');
            let options = {
                    method: 'GET',
                    url: config.apiUrl+':'+config.apiPort+'/api/v3/ns/orderDetail', // http://localhost:8080/api/v3/ns/orderDetail?orderId=S1QapYrhRp
                    qs: { orderId: data.order_id },
                    headers: { 
                        'content-type': 'application/x-www-form-urlencoded',
                        'cache-control': 'no-cache',
                        'x-access-token': data.token
                    }
                };
            request(options, (error, response, body) => {
                let odt = JSON.parse(body);
                if(odt.dt.length > 0 && odt.dt[0].status != 0) {
                    if(odt.dt[0].status == 1) {
                        socket.emit("server-response: order status", {
                            pesan: "order anda diterima"
                        })
                    }
                }
            })
        });

    });
}