/**
 * @author Artha Prihardana 
 * @Date 2017-01-31 11:12:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-04-27 07:34:30
 */
import config from '../app.conf';
import nodegcm from 'node-gcm';

export const sendNotification = (message,receiverId,cb) => {
    let msg = new nodegcm.Message({
        data: {
            title: message.title,
            status: message.status,
            message: message.message,
            orderId: (message.orderId) ? message.orderId : ''
        }
    });
    let sender = (receiverId instanceof Array) ? receiverId : [receiverId];
    let apikey = new nodegcm.Sender(config.googleApiKey);
    apikey.send(msg, {registrationTokens: sender}, (err, response) => {
        if (err) {
            console.log('send message ==> error');
            cb({
                s:0,
                message: "Ooops Something Went Wrong!!!",
                error: err
            });
        } else {
            console.log('send message ==> berhasil');
            // console.log('response==>', response);
            // console.log('msg==>', msg);
            cb({
                s:1,
                message: "message send success",
                response: response,
                data: msg
            });
        }
    });
}

export const sendChat = (pengirim, penerima, pesan, cb) => {
    console.log('ke const sendChat');
    console.log('pengirim', pengirim.senderId);
    console.log('penerima', penerima.senderId);
    let msg = new nodegcm.Message({
        data: {
            title: "Anda mendapatkan pesan dari " + pengirim.nama,
            status: pesan.status,
            message: pesan.message,
            orderId: pesan.orderId
        }
    });
    // let sender = (receiverId instanceof Array) ? receiverId : [receiverId];
    let sender = new nodegcm.Sender(config.googleApiKey);
    sender.send(msg, {registrationTokens: [penerima.senderId]}, (err, response) => {
        if (err) {
            cb({
                s:0,
                message: "Ooops Something Went Wrong!!!",
                error: err
            });
        } else {
            cb({
                s:1,
                message: "message send success",
                response: response,
                data: msg
            });
        }
    });
}
