/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 05:49:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-01-29 05:53:19
 */
import config from '../app.conf';
import jwt from 'jwt-simple';

export default (req, res, next) => {
    let token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
    let key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];

    if (token || key) {
        try {
            let decoded = jwt.decode(token, config.secretKey);
            if (decoded.exp <= Date.now()) {
                res.status(400);
                res.send({
                    s:0,
                    statusCode: 400,
                    message: 'Token atau key telah kedaluarsa'
                });
                return;
            }
            next();
        } catch(e) {
            res.status(500);
            res.send({
                s:0,
                statusCode: 500,
                error: e
            });
        }
    } else {
        res.status(401);
        res.send({
            s:0,
            statusCode: 401,
            message: 'Token atau key tidak sah'
        });
    }
}