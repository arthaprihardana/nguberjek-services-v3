/**
 * @author Artha Prihardana 
 * @Date 2017-02-15 21:59:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-05-11 13:19:06
 */
import config from '../app.conf';
import ChatModel from '../models/chatModel';
import OrderModel from '../models/orderModel';
import resMessage from '../helpers/resHelper';
import {
    sendNotification,
    sendChat
} from '../helpers/firebaseHelper';
import TanggalHelper from '../helpers/tanggalHelper';

export const sendMessage = (req, res, next) => {
    console.log('kesini 1');
    let body = req.body;
    let getOrder = OrderModel.findOne({orderId: body.orderId}).populate("boncengerId").populate("riderId").exec();
    let tgl = new TanggalHelper();
    getOrder
        .then((response) => {
            // console.log('response 1');
            let chatRoom = ChatModel.find({orderId: body.orderId}).populate("boncengerId").populate("riderId").exec();
            return [response, chatRoom];
        })
        .spread((response, chatRoom) => {
            console.log('spread 2');
            if(chatRoom.length > 0) {
                console.log('update jika chat atas order id sudah ada');
                ChatModel.update({orderId: body.orderId}, {
                    $push: {
                        message: {
                            rider: (body.pengirim == 2) ? true : false,
                            boncenger: (body.pengirim == 1) ? true : false,
                            msg: body.message,
                            date: tgl.defaultFormat()
                        }
                    }
                }, {upsert:true}).exec();
            } else {
                console.log('input chat baru', tgl.defaultFormat());
                let c = new ChatModel();
                c.orderId = response.orderId;
                c.riderId = response.riderId;
                c.boncengerId = response.boncengerId;
                c.message.push({
                    rider: (body.pengirim == 2) ? true : false,
                    boncenger: (body.pengirim == 1) ? true : false,
                    msg: body.message,
                    date: tgl.defaultFormat()
                });
                c.save();
            }
            let pengirim = (body.pengirim == 1) ? response.boncengerId : response.riderId;
            let penerima = (body.pengirim == 1) ? response.riderId : response.boncengerId;
            let msg = {
                title: "Pesan Chat dari " + pengirim.nama,
                status: 14,
                message: body.message,
                orderId: response.orderId,
                date: tgl.defaultFormat()
            };
            // console.log('pengirim', pengirim.senderId);
            // console.log('penerima', penerima.senderId);
            sendChat(pengirim, penerima, msg, (result) => {
                console.log('send chat 1');
                res.send(result);
            });
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const listMessage = (req, res, next) => {
    let query = req.query;
    console.log('kesini', query.orderId);
    let ChatRoom = ChatModel.find({orderId: query.orderId}).populate('boncengerId').populate('riderId').exec();
    ChatRoom
        .then((response) => {
            // console.log('response', response);
            let msg = resMessage(
                1,
                res.statusCode,
                'Get list message',
                null,
                null,
                null,
                response[0].message,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            console.log('error');
            let msg = resMessage(
                0,
                res.statusCode,
                'Ooops, something went wrong!!!',
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const sendNotificationTest = (req, res, next) => {
    // res.send({msg: 'hai'});
    let message = {
        title: "Info",
        status: 10,
        message: req.body.message
    };
    let receiverId = req.body.receiverId;
    sendNotification(message, receiverId, (result) => {
        console.log('result', result);
        res.send(result);
    });
}