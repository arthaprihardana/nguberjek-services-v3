/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 00:24:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-02-04 11:39:17
 */
import priceModel from '../models/priceModel';
import driverModel from '../models/driverModel';
import objectHelper from '../helpers/objectHelper';
import {
    ceil10,
    round10
} from '../helpers/mathHelper';
import resMessage from '../helpers/resHelper';

export const getPrice = (req, res) => {
    let objQry = new objectHelper(req.query, ["_id","limit","page", "all", "search"]);
    let limitPerPage = parseInt(req.query.limit)||25;
    let page = parseInt(req.query.page)||1;
    let dateStart = (req.query.dateStart) ? new Date(req.query.dateStart) : '';
    let dateEnd = (req.query.dateEnd) ? new Date(req.query.dateEnd) : '';
    let findByDate = (dateStart && dateEnd) ? {
        created_at: {
            $gte: dateStart,
            $lte: dateEnd
        }
    } : {};
    let search = {
        $and: [
            {show:true},
            {
                $or:[
                    {role: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    }
    let query = objQry.objekFilter();
    let find = {};
    
    if(query.search) {
        find = search;
    } else if(query.all) {
        limitPerPage = 0;
    } else {
        find = {
            $and: [
                {show:true},
                query||{}
            ]
        }
    }

    let getData = priceModel.find(find).sort({role: 1}).limit(limitPerPage).skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Daftar Harga',
                {
                    total: response.length,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(response.length / limitPerPage)
                },
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const addPrice = (req, res) => {
    let body = req.body;
    let pricemodel = new priceModel();
    pricemodel.role = body.role;
    pricemodel.tarif_buka_pintu = body.tarif_buka_pintu;
    pricemodel.flat_jarak = body.flat_jarak;
    pricemodel.flat_harga = body.flat_harga;
    pricemodel.placePrice = (body.placePrice) ? body.placePrice : "";
    pricemodel.placeKoordinat = (body.placeKoordinat) ? body.placeKoordinat.split(",").map(Number) : [];
    for (let i=0;i<3;i++) {
        let dt = body.price;
        pricemodel.price.push({
            "kalkulasi_jarak": dt[i].kalkulasi_jarak,
            "kalkulasi_harga": dt[i].kalkulasi_harga
        });
    }
    let save = pricemodel.save();
    save
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data harga berhasil disimpan',
                null,
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const updatePrice = (req, res) => {
    let qry = req.query;
    let body = req.body;
    if(qry && qry._id) {
        let update = priceModel.update({_id:qry._id}, body, {}).exec();
        update
            .then((response) => {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Harga telah diperbaharui',
                    null,
                    response,
                    body,
                    null,
                    null,
                    null,
                    null
                );
                res.send(msg);
            })
            .catch((err) => {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message,
                    null,
                    null,
                    null,
                    null,
                    err.errors,
                    null,
                    null
                );
                res.send(msg);
            });
    }
}

export const delPrice = (req, res) => {
    let qry = req.query;
    if(qry && qry._id) {
        let del = priceModel.update({_id:qry._id}, {show:false}, {}).exec();
        del
            .then((response) => {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Data harga telah dihapus',
                    null,
                    null,
                    null,
                    response,
                    null,
                    null,
                    null
                );
                res.send(msg);
            })
            .catch((err) => {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message,
                    null,
                    null,
                    null,
                    null,
                    err.errors,
                    null,
                    null
                );
                res.send(msg);
            });
    }
}

export const hitPrice = (req, res) => {
    let query = req.query;
    let find = priceModel.findOne({
        $and: [{
            role: query.role
        }, {
            show: true
        }]
    }).exec();
    find
        .then((response) => {
            let jarak = parseFloat(query.jarak);
            let detail = response.price;
            let harga;
            if(jarak <= parseFloat(response.flat_jarak)) {
                harga = parseFloat(response.flat_harga);
            } else if(jarak <= parseFloat(detail[0].kalkulasi_jarak)) {
                harga = parseFloat(response.flat_harga) + 
                        (( jarak - parseFloat(response.flat_jarak) ) * parseFloat(detail[0].kalkulasi_harga ));
            } else if(jarak <= parseFloat(detail[1].kalkulasi_jarak)) {
                harga = parseFloat(response.flat_harga) + 
                        ( ( jarak - parseFloat(detail[0].kalkulasi_jarak) ) * parseFloat(detail[1].kalkulasi_harga) ) + 
                        ( (parseFloat(detail[0].kalkulasi_jarak) - parseFloat(response.flat_jarak)) * detail[0].kalkulasi_harga  ) ;
            } else if(jarak > detail[2].kalkulasi_jarak) {
                harga = parseFloat(response.flat_harga) + 
                        ( (jarak - parseFloat(detail[1].kalkulasi_jarak)) * parseFloat(detail[2].kalkulasi_harga) ) +
                        ( ( parseFloat(detail[1].kalkulasi_jarak) - parseFloat(detail[0].kalkulasi_jarak) ) * parseFloat(detail[1].kalkulasi_harga) ) + 
                        ( ( parseFloat(detail[0].kalkulasi_jarak) - response.flat_jarak) * parseFloat(detail[0].kalkulasi_harga)  ) ;
            }
            let msg = resMessage(
                1,
                res.statusCode,
                'Perkiraan harga',
                null,
                null,
                null,
                round10(harga, 3),
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}