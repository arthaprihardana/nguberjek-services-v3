/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-21 10:12:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 11:57:58
 */
import mongoose from 'mongoose'
import config from '../app.conf'
import menuModel from '../models/menuModel'
import objectHelper from '../helpers/objectHelper'
import resMessage from '../helpers/resHelper'

const ObjectId = mongoose.Types.ObjectId;

const detailMenu = (data) => {
    let arr = [];
    // let cnt = 0;
    for(let i in data) {
        arr.push({
            "_id": data[i]._id,
            "updated_at": data[i].updated_at,
            "created_at": data[i].created_at,
            "namaResto": data[i].resto ? data[i].resto.namaResto : "-",
            "jamBuka": data[i].resto ? data[i].resto.jamBuka : "-",
            "koordinat": {
                "latitude": data[i].resto ? data[i].resto.koordinat[1] : "-",
                "longitude": data[i].resto ? data[i].resto.koordinat[0] : "-"
            },
            "alamat": data[i].resto ? data[i].resto.alamat : "-",
            "kategoriMenu": data[i].kategori ? data[i].kategori.namaKategori : "-",
            "foto": data[i].foto,
            "harga": data[i].harga,
            "deskripsi": data[i].deskripsi,
            "namaMenu": data[i].namaMenu,
            "__v": data[i].__v,
            "show": data[i].show
        })
    }
    return arr;
}

const detailMenuByResto = (data) => {
    let arr = [];
    // let cnt = 0;
    for(let i in data) {
        let dt = []
        for(let j in data[i].data) {
            dt.push({
                "_id": data[i].data[j]._id,
                "updated_at": data[i].data[j].updated_at,
                "created_at": data[i].data[j].created_at,
                "namaResto": data[i].data[j].namaResto,
                "jamBuka": data[i].data[j].jamBuka,
                "koordinat": {
                    latitude: data[i].data[j].koordinat[1],
                    longitude: data[i].data[j].koordinat[0],
                },
                "alamat": data[i].data[j].alamat,
                "foto": data[i].data[j].foto,
                "harga": data[i].data[j].harga,
                "deskripsi": data[i].data[j].deskripsi,
                "namaMenu": data[i].data[j].namaMenu,
            })
        }
        arr.push({
            kategori: data[i]._id,
            // "data": data[i].data
            data: dt
        })
    }
    return arr;
}

export const createMenu = (req, res, next) => {
    let body = req.body;
    let menu = new menuModel()

    menu.namaMenu = body.namaMenu ? body.namaMenu : '';
    menu.deskripsi = body.deskripsi ? body.deskripsi : '';
    menu.harga = body.harga ? body.harga : '';
    menu.foto = body.foto ? body.foto : '';
    menu.kategori = body.kategori ? body.kategori : '';
    menu.resto = body.resto ? body.resto : '';

    const reg = menu.save();
    reg
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Menu di resto '+ body.resto +' berhasil ditambhakan',
                null,
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let dtVal = Object.values(err.errors);
            let arr = [];
            let cnt = 0;
            for(var i=0; i<dtVal.length; i++) {
                cnt++;
                arr.push(dtVal[i].message);
                if(cnt == dtVal.length) {
                    let msg = resMessage(
                        0,
                        res.statusCode,
                        err.message+':\n'+ arr.toString().replace(new RegExp(',', 'g'),"\n"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    );
                    res.send(msg);
                }
            }
        });
}

export const putMenu = (req, res, next) => {
    let params = req.params;
    let query = req.query;
    let body = req.body;
    let put = menuModel.update({_id:query._id || params.id}, body, {}).exec();
    put
        .then((response) => {
            let get = menuModel.findOne({_id:query._id || params.id}).exec();
            return [response,get];
        })
        .spread((response, get) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Menu telah diperbaharui',
                null,
                response,
                body,
                get,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const delMenu = (req, res, next) => {
    let params = req.params;
    let query = req.query;
    let del = menuModel.update({_id:query._id || params.id}, {show: false}, {}).exec();
    del
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Menu telah dihapus',
                null,
                response,
                null,
                null,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const getMenu = (req, res, next) => {
    let objQry = new objectHelper(req.query, ['limit','page'])
    let limitPerPage = parseInt(req.query.limit) || 25;
    let page = parseInt(req.query.page) || 1;
    let search = {
        $and: [
            {show: true},
            {
                $or: [
                    {namaMenu: new RegExp(req.query.search, "i")},
                    {deskripsi: new RegExp(req.query.search, "i")},
                    {harga: typeof req.query.search == "number" ? new RegExp(parseInt(req.query.search), "i") : ""},
                    // {kategori: new RegExp(req.query.search, "i")}
                    // {resto: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    }
    let query = objQry.objekFilter()
    let find = {}

    if(query.search) {
        find = search
    } else if (query.all) {
        limitPerPage = 0
    } else {
        find = {
            $and: [
                {show: true},
                query||{}
            ]
        }
    }

    let getData = menuModel.find(find).populate('kategori').populate('resto').limit(limitPerPage).skip(limitPerPage * ( page - 1 )).exec();
    getData
        .then((response) => {
            let dataCount = menuModel.count(find).exec();
            // console.log('response ==>', response)
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data Menu',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                detailMenu(response),
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const groupMenuByCategory = (req, res, next) => {
    let key = req.query || req.params
    console.log('key==>', key);
    let getData = menuModel
        // .find()
        // .populate('kategori')
        // .populate('resto')
        .aggregate([
            {
                "$match": {
                    "resto": new ObjectId(key.namaResto)
                }
            },
            {
                "$lookup": {
                    "from": "kategorimenus",
                    "localField": "kategori",
                    "foreignField": "_id",
                    "as": "kategoriMenu"
                }
            },
            { "$unwind": "$kategoriMenu" },
            {
                "$lookup": {
                    "from": "restos",
                    "localField": "resto",
                    "foreignField": "_id",
                    "as": "resto"
                }
            },
            { "$unwind": "$resto" },
            {
                $group : {
                    _id : "$kategoriMenu.namaKategori",
                    data : { 
                        $push : {
                            "_id": "$_id",
                            "updated_at": "$updated_at",
                            "created_at": "$created_at",
                            "namaResto": "$resto.namaResto",
                            "jamBuka": "$resto.jamBuka",
                            "koordinat": "$resto.koordinat",
                            "alamat": "$resto.alamat",
                            // "kategoriMenu": "$kategoriMenu",
                            "foto": "",
                            "harga": "$harga",
                            "deskripsi": "",
                            "namaMenu": "$namaMenu",
                            "__v": "$__v",
                            "show": "$show"
                        } 
                    }
                }
            }
        ])
        .exec();
    getData
        .then((response) => {
            // let dataCount = menuModel.count(find).exec();
            // return [dataCount, response];
            // res.send(response)
            let msg = resMessage(
                1,
                res.statusCode,
                'Data Menu By Resto',
                {},
                null,
                null,
                detailMenuByResto(response),
                null,
                null,
                null
            );
            res.send(msg);
        })
        // .spread((dataCount, response) => {
        //     res.send(response)
        // })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}