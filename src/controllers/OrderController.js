/**
 * @author Artha Prihardana
 * @Date 2017-01-30 18:37:22
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-07-15 18:15:49
 */
import config from "../app.conf";
import request from "request";
import mongoose from "mongoose";
import orderModel from "../models/orderModel";
import driverModel from "../models/driverModel";
import boncengerModel from "../models/boncengerModel";
import objectHelper from "../helpers/objectHelper";
import generate from "../helpers/generateHelper";
import { sendNotification } from "../helpers/firebaseHelper";
import resMessage from "../helpers/resHelper";

const ObjectId = mongoose.Types.ObjectId;

class OrderController {
  constructor(data) {
    this.data = data;
  }

  getDetailHistori(cb) {
    let count = 0;
    let data = [];
    this.data.forEach((val, key, array) => {
      count++;
      let dt = {
        _id: val._id,
        orderId: val.orderId,
        kategori: val.kategori,
        tanggal: val.tanggal,
        lokasiAwal: val.lokasiAwal.lokasi,
        latitudeLA: val.lokasiAwal.koordinatLokasi[1],
        longitudeLA: val.lokasiAwal.koordinatLokasi[0],
        lokasiTujuan: val.lokasiAkhir.tujuan,
        latitudeLT: val.lokasiAkhir.koordinatTujuan[1],
        longitudeLT: val.lokasiAkhir.koordinatTujuan[0],
        jarak: val.jarak,
        waktu: val.waktu,
        harga: val.harga,
        status: val.status
      };
      if (val.riderId && val.riderId != undefined) {
        dt["driver"] = {
          _id: val.riderId._id,
          nama: val.riderId.nama,
          noTelp: val.riderId.noTelp,
          email: val.riderId.email,
          photo: val.riderId.photo,
          jenisKelamin: val.riderId.jenisKelamin,
          typeKendaraan: val.riderId.typeKendaraan,
          merkKendaraan: val.riderId.merkKendaraan,
          noPolisi: val.riderId.noPolisi,
          lokasi: val.riderId.lokasi,
          role: val.riderId.role,
          rating: 0
        };
      }
      if (val.boncengerId && val.boncengerId != undefined) {
        dt["boncenger"] = {
          _id: val.boncengerId._id,
          nama: val.boncengerId.nama,
          noTelp: val.boncengerId.noTelp,
          email: val.boncengerId.email,
          jenisKelamin: val.boncengerId.jenisKelamin,
          photo: val.boncengerId.photo
        };
      }
      if (val.riderId && val.riderId != undefined) {
        request(
          config.apiUrl +
            ":" +
            config.apiPort +
            "/api/v3/ns/ratingDriver?riderId=" +
            val.riderId._id,
          (err, res, body) => {
            let bd = JSON.parse(body);
            dt.driver.rating = bd.dt != undefined ? bd.dt : 0;
            console.log("rating==>", bd.dt);
            // console.log('type', dt.driver.rating);
            data.push(dt);

            if (array.length == count) {
              cb(data);
            }
          }
        );
      } else {
        data.push(dt);
        if (array.length == count) {
          cb(data);
        }
      }
    });
  }

  getRadiusDriver(header, body, cb) {
    let options = {
      method: "GET",
      url: config.apiUrl + ":" + config.apiPort + "/api/v3/ws/radius/driver",
      qs: {
        koordinat: body.koordinatLokasi,
        role: body.kategori
      },
      headers: {
        "cache-control": "no-cache",
        "x-access-token": header["x-access-token"]
      }
    };
    request(options, (error, response, body) => {
      try {
        if (error) {
          // throw new Error(error);
          console.error(error);
          cb({
            s: 0,
            message: "Ooop Something Went Wrong!!!"
          });
        }
        cb(JSON.parse(body));
      } catch (e) {
        // console.error('e==>', e);
        cb({
          s: 0,
          message: "Ooop Something Went Wrong!!!"
        });
      }
    });
  }

  getDetailOrder(cb) {
    // console.log('====================================');
    // console.log('this data ==>', this.data);
    // console.log('====================================');
    cb({
      _id: this.data._id,
      updated_at: this.data.updated_at,
      created_at: this.data.created_at,
      status: this.data.status,
      kategori: this.data.kategori,
      waktu: this.data.waktu,
      jarak: this.data.jarak,
      harga: this.data.harga,
      boncenger: {
        _id: this.data.boncengerId._id,
        senderId: this.data.boncengerId.senderId,
        username: this.data.boncengerId.username,
        jenisKelamin: this.data.boncengerId.jenisKelamin,
        noTelp: this.data.boncengerId.noTelp,
        email: this.data.boncengerId.email,
        nama: this.data.boncengerId.nama,
        photo: this.data.boncengerId.photo
      },
      tanggal: this.data.tanggal,
      orderId: this.data.orderId,
      rider: {
        _id: this.data.riderId._id,
        senderId: this.data.riderId.senderId,
        username: this.data.riderId.username,
        lokasi: this.data.riderId.lokasi,
        noPolisi: this.data.riderId.noPolisi,
        merkKendaraan: this.data.riderId.merkKendaraan,
        typeKendaraan: this.data.riderId.typeKendaraan,
        jenisKelamin: this.data.riderId.jenisKelamin,
        noTelp: this.data.riderId.noTelp,
        email: this.data.riderId.email,
        nama: this.data.riderId.nama,
        photo: this.data.riderId.photo,
        role: this.data.riderId.role
      },
      lokasiAkhir: {
        keteranganLokasiAkhir: this.data.lokasiAkhir.keteranganLokasiAkhir,
        koordinatLat: this.data.lokasiAkhir.koordinatTujuan[1],
        koordinatLong: this.data.lokasiAkhir.koordinatTujuan[0],
        tujuan: this.data.lokasiAkhir.tujuan
      },
      lokasiAwal: {
        keteranganLokasiAwal: this.data.lokasiAwal.keteranganLokasiAwal,
        koordinatLat: this.data.lokasiAwal.koordinatLokasi[1],
        koordinatLong: this.data.lokasiAwal.koordinatLokasi[0],
        lokasi: this.data.lokasiAwal.lokasi
      },
      komentar: this.data.komentar ? this.data.komentar : "",
      rating: this.data.rating ? this.data.rating : ""
    });
  }

  getDetailOrderFood(cb) {
    console.log('detail order food ==>', this.data)
    let data = this.data.dt[0];
    let response = {
      _id: data._id,
      updated_at: data.updated_at,
      created_at: data.created_at,
      status: data.status,
      komentar: data.komentar,
      rating: data.rating,
      boncenger: {
        _id: data.boncengerId._id,
        senderId: data.boncengerId.senderId,
        username: data.boncengerId.username,
        jenisKelamin: data.boncengerId.jenisKelamin,
        noTelp: data.boncengerId.noTelp,
        email: data.boncengerId.email,
        nama: data.boncengerId.nama,
        photo: data.boncengerId.photo
      },
      totalHarga: data.totalHarga,
      biayaAntar: data.biayaAntar,
      perkiraanHarga: data.perkiraanHarga,
      waktu: data.waktu,
      jarak: data.jarak,
      alamatPengantaran: data.alamatPengantaran,
      koordinatPengantaran: data.koordinatPengantaran,
      // "namaResto": data.namaResto,
      resto: {
        _id: data.namaResto._id,
        foto: data.namaResto.foto,
        jamBuka: data.namaResto.jamBuka,
        koordinat: data.namaResto.koordinat,
        alamat: data.namaResto.alamat,
        namaResto: data.namaResto.namaResto,
        noTelp: data.namaResto.noTelp
      },
      tanggal: data.tanggal,
      orderId: data.orderId,
      show: data.show
      
    //   lokasiAwal: data.namaResto.alamat,
    //   latitudeLA: data.namaResto.koordinat[1],
    //   longitudeLA: data.namaResto.koordinat[0],
    //   lokasiTujuan: data.alamatPengantaran,
    //   latitudeLT: data.koordinatPengantaran[1],
    //   longitudeLT: data.koordinatPengantaran[0]
    };

    if(data.riderId != undefined) {
      response.rider = {
        _id: data.riderId._id,
        senderId: data.riderId.senderId,
        username: data.riderId.username,
        lokasi: data.riderId.lokasi,
        noPolisi: data.riderId.noPolisi,
        merkKendaraan: data.riderId.merkKendaraan,
        typeKendaraan: data.riderId.typeKendaraan,
        jenisKelamin: data.riderId.jenisKelamin,
        noTelp: data.riderId.noTelp,
        email: data.riderId.email,
        nama: data.riderId.nama,
        photo: data.riderId.photo,
        role: data.riderId.role
      }
    }

    if (data.namaMenu.length > 0) {
      let arr = [];
      for (let i = 0; i < data.namaMenu.length; i++) {
        // console.log("data nama Menu ==> ", data.namaMenu[i].menuId);
        arr.push({
          namaMenu: data.namaMenu[i].menuId.namaMenu,
          deskripsi: data.namaMenu[i].menuId.deskripsi,
          harga: data.namaMenu[i].menuId.harga,
          foto: data.namaMenu[i].menuId.foto
        });
      }
      // console.log("arr ==> ", arr);
      response.namaMenu = arr;
    }
    cb(response);
  }

  getDetailHistoryOrder(cb) {
    let count = 0;
    let data = [];
    this.data.forEach((val, key, array) => {
      count++;
      let dt = {
        _id: val._id,
        orderId: val.orderId,
        kategori: val.kategori,
        tanggal: val.tanggal,
        lokasiAwal: val.lokasiAwal.lokasi,
        latitudeLA: val.lokasiAwal.koordinatLokasi[1],
        longitudeLA: val.lokasiAwal.koordinatLokasi[0],
        lokasiTujuan: val.lokasiAkhir.tujuan,
        latitudeLT: val.lokasiAkhir.koordinatTujuan[1],
        longitudeLT: val.lokasiAkhir.koordinatTujuan[0],
        jarak: val.jarak,
        waktu: val.waktu,
        harga: val.harga,
        status: val.status
      };
      if (val.riderId && val.riderId != undefined) {
        dt["driver"] = {
          _id: val.riderId._id,
          nama: val.riderId.nama,
          noTelp: val.riderId.noTelp,
          email: val.riderId.email,
          photo: val.riderId.photo,
          jenisKelamin: val.riderId.jenisKelamin,
          typeKendaraan: val.riderId.typeKendaraan,
          merkKendaraan: val.riderId.merkKendaraan,
          noPolisi: val.riderId.noPolisi,
          lokasi: val.riderId.lokasi,
          role: val.riderId.role
        };
      } else {
        dt["driver"] = undefined
      }
      if (val.boncengerId && val.boncengerId != undefined) {
        dt["boncenger"] = {
          _id: val.boncengerId._id,
          nama: val.boncengerId.nama,
          noTelp: val.boncengerId.noTelp,
          email: val.boncengerId.email,
          jenisKelamin: val.boncengerId.jenisKelamin,
          photo: val.boncengerId.photo
        };
      } else {
        dt["boncenger"] = undefined
      }
      data.push(dt);
      if (array.length == count) {
        cb(data);
      }
    });
  }

  getDataHistoryFranchise(franchise, cb) {
    let data = this.data;
    let arr = [];
    let cnt = 0;
    for(let i=0; i < data.length; i++) {
      if(data[i].riderId != undefined && data[i].riderId.franchise == true && data[i].riderId.franchiseName == franchise) {
        cnt++;
        var dt = {
          "_id": data[i]._id,
          "orderId": data[i].orderId,
          "kategori": data[i].kategori,
          "tanggal": data[i].tanggal,
          "lokasiAwal": data[i].lokasiAwal.lokasi,
          "latitudeLA": data[i].lokasiAwal.koordinatLokasi[1],
          "longitudeLA": data[i].lokasiAwal.koordinatLokasi[0],
          "lokasiTujuan": data[i].lokasiAkhir.tujuan,
          "latitudeLT": data[i].lokasiAkhir.koordinatTujuan[1],
          "longitudeLT": data[i].lokasiAkhir.koordinatTujuan[0],
          "jarak": data[i].jarak,
          "waktu": data[i].waktu,
          "harga": data[i].harga,
          "status": data[i].status,
        }
        if(data[i].riderId && data[i].riderId != undefined) {
          dt["driver"] = {
              "_id": data[i].riderId._id,
              "nama": data[i].riderId.nama,
              "noTelp": data[i].riderId.noTelp,
              "email": data[i].riderId.email,
              "photo": data[i].riderId.photo,
              "jenisKelamin": data[i].riderId.jenisKelamin,
              "typeKendaraan": data[i].riderId.typeKendaraan,
              "merkKendaraan": data[i].riderId.merkKendaraan,
              "noPolisi": data[i].riderId.noPolisi,
              "lokasi": data[i].riderId.lokasi
          };
        }
        if(data[i].boncengerId && data[i].boncengerId != undefined) {
          dt["boncenger"] = {
            "_id": data[i].boncengerId._id,
            "nama": data[i].boncengerId.nama,
            "noTelp": data[i].boncengerId.noTelp,
            "email": data[i].boncengerId.email,
            "jenisKelamin": data[i].boncengerId.jenisKelamin,
            "photo": data[i].boncengerId.photo
          }
        }
        arr.push(dt);
        // if(cnt == data.length) {
        //   cb(arr);
        // }
      }
    }
    cb(arr);
  }

}

export const getHistory = (req, res) => {
  // console.log("kesini");
  let objQry = new objectHelper(req.query, [
    "_id",
    "limit",
    "page",
    "dateStart",
    "dateEnd",
    "token",
    "all",
    "franchise",
    "penumpang",
    "driver"
  ]);
  let limitPerPage = parseInt(req.query.limit) || 25;
  let page = parseInt(req.query.page) || 1;
  let dateStart = req.query.dateStart ? new Date(req.query.dateStart) : "";
  let dateEnd = req.query.dateEnd ? new Date(req.query.dateEnd) : "";
  let findByDate =
    dateStart && dateEnd
      ? {
          created_at: {
            $gte: dateStart,
            $lte: dateEnd
          }
        }
      : {};
  let search = {
    $and: [
      { show: true },
      {
        $or: [
          { orderId: new RegExp(req.query.search, "i") },
          { kategori: new RegExp(req.query.search, "i") }
        ]
      }
    ]
  };
  let query = objQry.objekFilter();
  let find = {};
  // console.log('req.query.penumpang ==>', req.query.penumpang);
  if (req.query.search) {
    find = search;
  } else if (req.query.all) {
    limitPerPage = 0;
  } else if (req.query.penumpang || req.query.driver) {
    limitPerPage = 0;
    find = {
      $and: [
        { show: true },
        // {
        //   $or: [{ status: 4 }, { status: 5 }, { status: 6 }, { status: 7 }]
        // },
        query || {},
        findByDate
      ]
    };
  } else if(req.query.franchise) {
    limitPerPage = 0;
    // find = {
    //   $and: [
    //     { show: true },
    //     { "riderId.franchiseName": req.query.franchise },
    //     query || {}
    //   ]
    // }
  } else {
    find = {
      $and: [
        { show: true },
        {
          $or: [{ status: 4 }, { status: 5 }, { status: 6 }, { status: 7 }]
        },
        findByDate,
        query || {}
      ]
    };
  }

  // console.log('find ==>', find);
  let getData = orderModel
    .find(find)
    // .populate("riderId")
    // .populate("boncengerId", "nama")
    // .populate("boncengerId")
    .populate({
      path: "riderId",
      match: {
        "nama": new RegExp(req.query.driver, "i")
      }
    })
    .populate({
      path: "boncengerId",
      // select: "nama",
      match: {
        "nama": new RegExp(req.query.penumpang, "i")
      }
    })
    .sort({ tanggal: -1 })
    .limit(limitPerPage)
    .skip(limitPerPage * (page - 1))
    .exec();
  getData
    .then(response => {
      let dataCount = orderModel.count(find).exec();
      return [dataCount, response];
    })
    .spread((dataCount, response) => {
      if (dataCount > 0) {
        if(req.query.penumpang && response.length > 0) {
          let arr = [];
          let cnt = 0;
          for(let i=0; i<response.length; i++) {
            cnt++;
            if(response[i].boncengerId != undefined && response[i].boncengerId != null) { 
              arr.push(response[i]);
            }
            if(cnt == response.length) {
              if(arr.length > 0) {
                let order = new OrderController(arr);
                order.getDetailHistoryOrder(data => {
                  let msg = resMessage(
                    1,
                    res.statusCode,
                    "Data histori order",
                    {
                      total: arr.length,
                      limit: limitPerPage,
                      page: page,
                      pages: Math.ceil(arr.length / limitPerPage)
                    },
                    null,
                    null,
                    data,
                    null,
                    null,
                    null
                  );
                  res.send(msg);
                }); 
              } else {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Data histori order",
                  {
                    total: arr.length,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(arr.length / limitPerPage)
                  },
                  null,
                  null,
                  arr,
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            }
          }
        } else if(req.query.driver && response.length > 0) {
          let arr = [];
          let cnt = 0;
          for(let i=0; i<response.length; i++) {
            cnt++;
            if(response[i].riderId != undefined && response[i].riderId != null) { 
              arr.push(response[i]);
            }
            if(cnt == response.length) {
              if(arr.length > 0) {
                let order = new OrderController(arr);
                order.getDetailHistoryOrder(data => {
                  let msg = resMessage(
                    1,
                    res.statusCode,
                    "Data histori order",
                    {
                      total: arr.length,
                      limit: limitPerPage,
                      page: page,
                      pages: Math.ceil(arr.length / limitPerPage)
                    },
                    null,
                    null,
                    data,
                    null,
                    null,
                    null
                  );
                  res.send(msg);
                });
              } else {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Data histori order",
                  {
                    total: arr.length,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(arr.length / limitPerPage)
                  },
                  null,
                  null,
                  arr,
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            }
          }
        } else {
          let order = new OrderController(response);
          if(req.query.franchise) {
            order.getDataHistoryFranchise(req.query.franchise, data => {
              let msg = resMessage(
                1,
                res.statusCode,
                "Data histori order",
                {
                  total: data.length,
                  limit: limitPerPage,
                  page: page,
                  pages: Math.ceil(data.length / limitPerPage)
                },
                null,
                null,
                data,
                null,
                null,
                null
              );
              res.send(msg);
            });
          } else {
            order.getDetailHistoryOrder(data => {
              let msg = resMessage(
                1,
                res.statusCode,
                "Data histori order",
                {
                  total: dataCount,
                  limit: limitPerPage,
                  page: page,
                  pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                data,
                null,
                null,
                null
              );
              res.send(msg);
            });
          }
        }
      } else {
        let msg = resMessage(
          1,
          res.statusCode,
          "Data histori order",
          {
            total: dataCount,
            limit: limitPerPage,
            page: page,
            pages: Math.ceil(dataCount / limitPerPage)
          },
          null,
          null,
          response,
          null,
          null,
          null
        );
        res.send(msg);
      }
    })
    .catch(err => {
      let msg = resMessage(
        0,
        res.statusCode,
        err.message,
        null,
        null,
        null,
        null,
        err.errors,
        null,
        null
      );
      res.send(msg);
    });
};

export const requestOrder = (req, res) => {
  // dari boncenger
  let header = req.headers;
  let body = req.body;
  let gen = new generate();
  gen.setOrderId();
  let order = new OrderController();
  order.getRadiusDriver(header, body, response => {
    console.log("response==>", response);
    let orderId = gen.getOrderId();
    let save_request = new orderModel();
    save_request.orderId = orderId;
    // save_request.orderId = "HgEn5kxNwE";
    save_request.tanggal = new Date();
    (save_request.boncengerId = body.boncengerId),
      (save_request.lokasiAwal["lokasi"] = body.lokasiAwal);
    save_request.lokasiAwal["koordinatLokasi"] = body.koordinatLokasi
      .split(",")
      .map(Number);
    save_request.lokasiAwal["keteranganLokasiAwal"] = body.keteranganLokasiAwal;
    save_request.lokasiAkhir["tujuan"] = body.tujuan;
    save_request.lokasiAkhir["koordinatTujuan"] = body.koordinatTujuan
      .split(",")
      .map(Number);
    save_request.lokasiAkhir["keteranganLokasiAkhir"] =
      body.keteranganLokasiAkhir;
    save_request.harga = body.harga;
    save_request.jarak = body.jarak;
    save_request.waktu = body.waktu;
    save_request.kategori = body.kategori;
    save_request.status = 0;
    let simpan = save_request.save();

    let driverList = response.dt;
    // console.log("driverList length==>", driverList.length);
    // console.log("form data==>", save_request);
    res.send({
      dataOrder: save_request,
      driverList: driverList
    });
  });
};

export const acceptRequest = (req, res) => {
  // dari driver
  let body = req.body;
  let accept = orderModel
    .update(
      { orderId: body.orderId },
      { riderId: body.driverId, status: 1 },
      {}
    )
    .exec();
  accept
    .then(response => {
      let driverStatus = driverModel
        .update({ _id: body.driverId }, { ready: false }, {})
        .exec();
      let get = orderModel
        .findOne({ orderId: body.orderId })
        .populate("riderId")
        .populate("boncengerId")
        .exec();
      return [response, get];
    })
    .spread((response, get) => {
      let order = new OrderController(get);
      order.getDetailOrder(data => {
        let msg = resMessage(
          1,
          res.statusCode,
          "Detail Order",
          null,
          response,
          null,
          data,
          null,
          null,
          null
        );
        res.send(msg);
      });
    })
    .catch(err => {
      let msg = resMessage(
        0,
        res.statusCode,
        err.message,
        null,
        null,
        null,
        null,
        err.errors,
        null,
        null
      );
      res.send(msg);
    });
};

export const updateOrder = (req, res) => {
  let body = req.body;
  if (body && body.orderId && body.status /*&& body.riderId*/) {
    // food order
    let reg = new RegExp("food_");
    if (reg.test(body.orderId)) {
      let orderid = body.orderId.split("_")[1];
      let options = {
        method: 'POST',
        url: config.apiUrl + ":" + config.apiPort + config.apiVersion + '/ns/updateFoodOrder',
        headers: { 
          'cache-control': 'no-cache',
          'content-type': 'application/x-www-form-urlencoded' 
        },
        form: { 
          orderId: orderid, 
          riderId: body.riderId,
          status: body.status, 
          komentar: body.komentar != "" || body.komentar != undefined ? body.komentar : "", 
          rating: body.rating != "" || body.rating != undefined ? body.rating : 0
        }
      };
      request(options, function(error, response, body) {
        try {
          if (error) {
            res.send(error);
          }
          // console.log('response ==>', response.body);
          // console.log('body ==>', body);
          let dt = JSON.parse(response.body)
          let order = new OrderController(dt);
          // kirim notifikasi
          // kirim notifikasi
          order.getDetailOrderFood(data => {
            let msg = resMessage(
              1,
              res.statusCode,
              "Update Status Food Order",
              null,
              dt.response,
              null,
              data,
              null,
              null,
              null
            );
            res.send(msg);
          });
        } catch (error) {
          console.log("error", error);
          res.send(error);
        }
      })
    } /** food order */ else {
      let findOrder;
      if (body.riderId) {
        findOrder = orderModel
          .findOne({ orderId: body.orderId })
          .populate("riderId")
          .populate("boncengerId")
          .exec();
      } else {
        findOrder = orderModel
          .findOne({ orderId: body.orderId })
          .populate("boncengerId")
          .exec();
      }
      findOrder
        .then(response => {
          orderModel
            .update(
              {
                orderId: body.orderId
              },
              {
                status: body.status,
                komentar:
                  body.komentar != "" || body.komentar != undefined
                    ? body.komentar
                    : "",
                rating:
                  body.rating != "" || body.rating != undefined ? body.rating : 0
              }
            )
            .exec();
          switch (body.status) {
            case "2":
              sendNotification(
                {
                  title: "Order Status",
                  status: 2,
                  message: "Driver sedang menuju lokasi anda",
                  orderId: response.orderId
                  // expired: 20000
                },
                response.boncengerId.senderId,
                gcmresponse => {
                  let msg = resMessage(
                    1,
                    res.statusCode,
                    "Driver sedang menuju lokasi anda",
                    null,
                    gcmresponse,
                    null,
                    {},
                    null,
                    null,
                    null
                  );
                  res.send(msg);
                }
              );
              break;
            case "3":
              sendNotification(
                {
                  title: "Order Status",
                  status: 3,
                  message:
                    "Perjalanan telah dimulai. Hati-hati dalam perjalanan, semoga selamat sampai tujuan",
                  orderId: response.orderId
                  // expired: 20000
                },
                [response.boncengerId.senderId, response.riderId.senderId],
                gcmresponse => {
                  let msg = resMessage(
                    1,
                    res.statusCode,
                    "Perjalanan telah dimulai. Hati-hati dalam perjalanan, semoga selamat sampai tujuan",
                    null,
                    gcmresponse,
                    null,
                    {},
                    null,
                    null,
                    null
                  );
                  res.send(msg);
                }
              );
              break;
            case "4":
              sendNotification(
                {
                  title: "Order Status",
                  status: 4,
                  message:
                    "Perjalanan telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
                  orderId: response.orderId
                  // expired: 20000
                },
                [response.boncengerId.senderId, response.riderId.senderId],
                gcmresponse => {
                  driverModel.findOne({ _id: body.riderId }).exec((err, data) => {
                    let saldo = data.saldo;
                    let ps = parseInt(saldo) - parseInt(response.harga) * 0.2;
                    driverModel
                      .update(
                        { _id: body.riderId },
                        { ready: true, saldo: ps },
                        {}
                      )
                      .exec();
                    let detailorder = new OrderController(response);
                    detailorder.getDetailOrder(data => {
                      let msg = resMessage(
                        1,
                        res.statusCode,
                        "Perjalanan telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
                        null,
                        gcmresponse,
                        null,
                        data,
                        null,
                        null,
                        null
                      );
                      res.send(msg);
                    });
                  });
                }
              );
              break;
            case "5":
              sendNotification(
                {
                  title: "Order Status",
                  status: 5,
                  message: "Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. Mohon maaf atas ketidaknyaman anda.",
                  harga:
                    "Harga potongan " +
                    response.harga +
                    " x 50% = " +
                    parseInt(response.harga) / 2,
                  orderId: response.orderId
                  // expired: 20000
                },
                [response.boncengerId.senderId, response.riderId.senderId],
                gcmresponse => {
                  driverModel.findOne({ _id: body.riderId }).exec((err, data) => {
                    let saldo = data.saldo;
                    let ps = parseInt(saldo) - parseInt(response.harga) / 2 * 0.2;
                    driverModel.update({ _id: body.riderId }, { ready: true, saldo: ps }, {}).exec();
                    orderModel.update({ orderId: body.orderId},{ harga: parseInt(response.harga) / 2 }).exec();
                    let detailorder = new OrderController(response);
                    detailorder.getDetailOrder(data => {
                      let msg = resMessage(
                        1,
                        res.statusCode,
                        `Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. 
                        Harga Trip mendapat potongan 50% dari 
                        ${parseInt(data.harga)} menjadi 
                        ${(parseInt(data.harga) / 2)}`,
                        // "Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. Mohon maaf atas ketidaknyaman anda.",
                        // {
                        //   harga:
                        //     "Harga potongan " +
                        //     data.harga +
                        //     " x 50% = " +
                        //     parseInt(data.harga) / 2
                        // },
                        gcmresponse,
                        null,
                        data,
                        null,
                        null,
                        null
                      );
                      res.send(msg);
                    });
                  });
                }
              );
              break;
            case "6":
              if (response.riderId == undefined) {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Perjalanan telah dibatalkan oleh penumpang",
                  null,
                  null,
                  null,
                  {},
                  null,
                  null,
                  null
                );
                res.send(msg);
              } else {
                console.log("====================================");
                console.log("penumpang membatalkan order");
                console.log("senderID Driver ==> ", response.riderId.senderId);
                console.log("====================================");
                sendNotification(
                  {
                    title: "Order Status",
                    status: 6,
                    message: "Perjalanan telah dibatalkan oleh penumpang",
                    orderId: response.orderId
                    // expired: 20000
                  },
                  [response.riderId.senderId],
                  gcmresponse => {
                    driverModel
                      .update({ _id: body.riderId }, { ready: true }, {})
                      .exec();
                    let msg = resMessage(
                      1,
                      res.statusCode,
                      "Perjalanan telah dibatalkan oleh penumpang",
                      null,
                      null,
                      null,
                      {},
                      null,
                      null,
                      null
                    );
                    res.send(msg);
                  }
                );
              }
              break;
            case "7":
              sendNotification(
                {
                  title: "Order Status",
                  status: 7,
                  message: "Perjalanan telah dibatalkan oleh rider",
                  orderId: response.orderId
                  // expired: 20000
                },
                [response.boncengerId.senderId],
                gcmresponse => {
                  driverModel
                    .update({ _id: body.riderId }, { ready: true }, {})
                    .exec();
                  let msg = resMessage(
                    1,
                    res.statusCode,
                    "Perjalanan telah dibatalkan oleh rider",
                    null,
                    null,
                    null,
                    {},
                    null,
                    null,
                    null
                  );
                  res.send(msg);
                }
              );
              break;
            default:
              let msg = resMessage(
                0,
                res.statusCode,
                "Ooops. Something went wrong!!!",
                null,
                null,
                null,
                null,
                {},
                null,
                null
              );
              res.send(msg);
              break;
          }
        })
        .catch(err => {
          let msg = resMessage(
            0,
            res.statusCode,
            err.message,
            null,
            null,
            null,
            null,
            err.errors,
            null,
            null
          );
          res.send(msg);
        });
    }
    // ori start
    // if (body.riderId) {
    //   findOrder = orderModel
    //     .findOne({ orderId: body.orderId })
    //     .populate("riderId")
    //     .populate("boncengerId")
    //     .exec();
    // } else {
    //   findOrder = orderModel
    //     .findOne({ orderId: body.orderId })
    //     .populate("boncengerId")
    //     .exec();
    // }
    // findOrder
    //   .then(response => {
    //     orderModel
    //       .update(
    //         {
    //           orderId: body.orderId
    //         },
    //         {
    //           status: body.status,
    //           komentar:
    //             body.komentar != "" || body.komentar != undefined
    //               ? body.komentar
    //               : "",
    //           rating:
    //             body.rating != "" || body.rating != undefined ? body.rating : 0
    //         }
    //       )
    //       .exec();
    //     switch (body.status) {
    //       case "2":
    //         sendNotification(
    //           {
    //             title: "Order Status",
    //             status: 2,
    //             message: "Driver sedang menuju lokasi anda",
    //             orderId: response.orderId
    //             // expired: 20000
    //           },
    //           response.boncengerId.senderId,
    //           gcmresponse => {
    //             let msg = resMessage(
    //               1,
    //               res.statusCode,
    //               "Driver sedang menuju lokasi anda",
    //               null,
    //               gcmresponse,
    //               null,
    //               {},
    //               null,
    //               null,
    //               null
    //             );
    //             res.send(msg);
    //           }
    //         );
    //         break;
    //       case "3":
    //         sendNotification(
    //           {
    //             title: "Order Status",
    //             status: 3,
    //             message:
    //               "Perjalanan telah dimulai. Hati-hati dalam perjalanan, semoga selamat sampai tujuan",
    //             orderId: response.orderId
    //             // expired: 20000
    //           },
    //           [response.boncengerId.senderId, response.riderId.senderId],
    //           gcmresponse => {
    //             let msg = resMessage(
    //               1,
    //               res.statusCode,
    //               "Perjalanan telah dimulai. Hati-hati dalam perjalanan, semoga selamat sampai tujuan",
    //               null,
    //               gcmresponse,
    //               null,
    //               {},
    //               null,
    //               null,
    //               null
    //             );
    //             res.send(msg);
    //           }
    //         );
    //         break;
    //       case "4":
    //         sendNotification(
    //           {
    //             title: "Order Status",
    //             status: 4,
    //             message:
    //               "Perjalanan telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
    //             orderId: response.orderId
    //             // expired: 20000
    //           },
    //           [response.boncengerId.senderId, response.riderId.senderId],
    //           gcmresponse => {
    //             driverModel.findOne({ _id: body.riderId }).exec((err, data) => {
    //               let saldo = data.saldo;
    //               let ps = parseInt(saldo) - parseInt(response.harga) * 0.2;
    //               driverModel
    //                 .update(
    //                   { _id: body.riderId },
    //                   { ready: true, saldo: ps },
    //                   {}
    //                 )
    //                 .exec();
    //               let detailorder = new OrderController(response);
    //               detailorder.getDetailOrder(data => {
    //                 let msg = resMessage(
    //                   1,
    //                   res.statusCode,
    //                   "Perjalanan telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
    //                   null,
    //                   gcmresponse,
    //                   null,
    //                   data,
    //                   null,
    //                   null,
    //                   null
    //                 );
    //                 res.send(msg);
    //               });
    //             });
    //           }
    //         );
    //         break;
    //       case "5":
    //         sendNotification(
    //           {
    //             title: "Order Status",
    //             status: 5,
    //             message:
    //               "Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. Mohon maaf atas ketidaknyaman anda.",
    //             harga:
    //               "Harga potongan " +
    //               data.harga +
    //               " x 50% = " +
    //               parseInt(data.harga) / 2,
    //             orderId: response.orderId
    //             // expired: 20000
    //           },
    //           [response.boncengerId.senderId, response.riderId.senderId],
    //           gcmresponse => {
    //             driverModel.findOne({ _id: body.riderId }).exec((err, data) => {
    //               let saldo = data.saldo;
    //               let ps = parseInt(saldo) - parseInt(response.harga) / 2 * 0.2;
    //               driverModel
    //                 .update(
    //                   { _id: body.riderId },
    //                   { ready: true, saldo: ps },
    //                   {}
    //                 )
    //                 .exec();
    //               let detailorder = new OrderController(response);
    //               detailorder.getDetailOrder(data => {
    //                 let msg = resMessage(
    //                   1,
    //                   res.statusCode,
    //                   "Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. Mohon maaf atas ketidaknyaman anda.",
    //                   {
    //                     harga:
    //                       "Harga potongan " +
    //                       data.harga +
    //                       " x 50% = " +
    //                       parseInt(data.harga) / 2
    //                   },
    //                   gcmresponse,
    //                   null,
    //                   data,
    //                   null,
    //                   null,
    //                   null
    //                 );
    //                 res.send(msg);
    //               });
    //             });
    //           }
    //         );
    //         break;
    //       case "6":
    //         if (response.riderId == undefined) {
    //           let msg = resMessage(
    //             1,
    //             res.statusCode,
    //             "Perjalanan telah dibatalkan oleh penumpang",
    //             null,
    //             null,
    //             null,
    //             {},
    //             null,
    //             null,
    //             null
    //           );
    //           res.send(msg);
    //         } else {
    //           console.log("====================================");
    //           console.log("penumpang membatalkan order");
    //           console.log("senderID Driver ==> ", response.riderId.senderId);
    //           console.log("====================================");
    //           sendNotification(
    //             {
    //               title: "Order Status",
    //               status: 6,
    //               message: "Perjalanan telah dibatalkan oleh penumpang",
    //               orderId: response.orderId
    //               // expired: 20000
    //             },
    //             [response.riderId.senderId],
    //             gcmresponse => {
    //               driverModel
    //                 .update({ _id: body.riderId }, { ready: true }, {})
    //                 .exec();
    //               let msg = resMessage(
    //                 1,
    //                 res.statusCode,
    //                 "Perjalanan telah dibatalkan oleh penumpang",
    //                 null,
    //                 null,
    //                 null,
    //                 {},
    //                 null,
    //                 null,
    //                 null
    //               );
    //               res.send(msg);
    //             }
    //           );
    //         }
    //         break;
    //       case "7":
    //         sendNotification(
    //           {
    //             title: "Order Status",
    //             status: 7,
    //             message: "Perjalanan telah dibatalkan oleh driver",
    //             orderId: response.orderId
    //             // expired: 20000
    //           },
    //           [response.boncengerId.senderId],
    //           gcmresponse => {
    //             driverModel
    //               .update({ _id: body.riderId }, { ready: true }, {})
    //               .exec();
    //             let msg = resMessage(
    //               1,
    //               res.statusCode,
    //               "Perjalanan telah dibatalkan oleh driver",
    //               null,
    //               null,
    //               null,
    //               {},
    //               null,
    //               null,
    //               null
    //             );
    //             res.send(msg);
    //           }
    //         );
    //         break;
    //       default:
    //         let msg = resMessage(
    //           0,
    //           res.statusCode,
    //           "Ooops. Something went wrong!!!",
    //           null,
    //           null,
    //           null,
    //           null,
    //           {},
    //           null,
    //           null
    //         );
    //         res.send(msg);
    //         break;
    //     }
    //   })
    //   .catch(err => {
    //     let msg = resMessage(
    //       0,
    //       res.statusCode,
    //       err.message,
    //       null,
    //       null,
    //       null,
    //       null,
    //       err.errors,
    //       null,
    //       null
    //     );
    //     res.send(msg);
    //   });
    // ori end
  } else {
    let msg = resMessage(
      0,
      res.statusCode,
      "parameter orderId, riderId dan status harus diisi",
      null,
      null,
      null,
      null,
      {},
      null,
      null
    );
    res.send(msg);
  }
};

export const orderDetail = (req, res) => {
  let query = req.query;
  // console.log('query order ID ==> ', query.orderId)
  let reg = new RegExp("food_");
  if (reg.test(query.orderId)) {
    let orderid = query.orderId.split("_")[1];
    let options = {
      method: "GET",
      url: config.apiUrl + ":" + config.apiPort + config.apiVersion + "/ns/riwayatFoodOrder",
      qs: { orderId: orderid },
      headers: {
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded"
      }
    };
    // console.log('request ke ==>', config.apiUrl + ":" + config.apiPort + config.apiVersion + "/ns/riwayatFoodOrder");
    request(options, function(error, response, body) {
      try {
        if (error) {
          res.send(error);
        }
        let order = new OrderController(JSON.parse(body));
        order.getDetailOrderFood(data => {
          let msg = resMessage(
            1,
            res.statusCode,
            "Data Food Order Detail",
            null,
            null,
            null,
            [data],
            null,
            null,
            null
          );
          res.send(msg);
        });
      } catch (error) {
        console.log("error", error);
        res.send(error);
      }
    });
  } else {
    let findOrderDetail = orderModel
      .find({
        $and: [{ show: true }, { orderId: query.orderId }]
      })
      .populate("riderId")
      .populate("boncengerId")
      .exec();
    findOrderDetail
      .then(response => {
        let order = new OrderController(response);
        order.getDetailHistori(data => {
          let msg = resMessage(
            1,
            res.statusCode,
            "Data Order Detail",
            null,
            null,
            null,
            data,
            null,
            null,
            null
          );
          res.send(msg);
        });
      })
      .catch(err => {
        let msg = resMessage(
          0,
          res.statusCode,
          "Data Order Detail",
          null,
          null,
          null,
          null,
          err,
          null,
          null
        );
        res.send(msg);
      });
  }
};

export const orderInProgress = (req, res, next) => {
  let objQry = new objectHelper(req.query, [
    "_id",
    "limit",
    "page",
    "dateStart",
    "dateEnd",
    "token",
    "all",
    "franchise"
  ]);
  let limitPerPage = parseInt(req.query.limit) || 25;
  let page = parseInt(req.query.page) || 1;
  let dateStart = req.query.dateStart ? new Date(req.query.dateStart) : "";
  let dateEnd = req.query.dateEnd ? new Date(req.query.dateEnd) : "";
  let findByDate =
    dateStart && dateEnd
      ? {
          created_at: {
            $gte: dateStart,
            $lte: dateEnd
          }
        }
      : {};
  let search = {
    $and: [
      { show: true },
      {
        $or: [
          { orderId: new RegExp(req.query.search, "i") },
          { kategori: new RegExp(req.query.search, "i") },
        ]
      }
    ]
  };
  let query = objQry.objekFilter();
  let find = {};
  if (req.query.search) {
    find = search;
  } else if (req.query.all) {
    limitPerPage = 0;
  } else {
    find = {
      $and: [
        { show: true },
        { status: 3 },
        findByDate,
        query || {}
      ]
    };
  }

  let getData = orderModel
    .find(find)
    .populate({
      path:"riderId",
      // match: {
      //   nama: new RegExp(req.query.search, "i")
      // }
    })
    // .populate("riderId")
    .populate("boncengerId")
    .sort({ tanggal: -1 })
    .limit(limitPerPage)
    .skip(limitPerPage * (page - 1))
    .exec();
  getData
    .then(response => {
      // console.log("response", response);
      let dataCount = orderModel.count(find).exec();
      return [dataCount, response];
    })
    .spread((dataCount, response) => {
      if (dataCount > 0) {
        let order = new OrderController(response);
        order.getDetailHistoryOrder(data => {
          let msg = resMessage(
            1,
            res.statusCode,
            "Data histori order",
            {
              total: dataCount,
              limit: limitPerPage,
              page: page,
              pages: Math.ceil(dataCount / limitPerPage)
            },
            null,
            null,
            data,
            null,
            null,
            null
          );
          res.send(msg);
        });
      } else {
        let msg = resMessage(
          1,
          res.statusCode,
          "Data histori order",
          {
            total: dataCount,
            limit: limitPerPage,
            page: page,
            pages: Math.ceil(dataCount / limitPerPage)
          },
          null,
          null,
          response,
          null,
          null,
          null
        );
        res.send(msg);
      }
    })
    .catch(err => {
      let msg = resMessage(
        0,
        res.statusCode,
        err.message,
        null,
        null,
        null,
        null,
        err.errors,
        null,
        null
      );
      res.send(msg);
    });
}

export const pingOrder = (req, res) => {
  // let ObjectId = require('mongoose').Types.ObjectId;
  let prm = req.params;
  console.log("id==>", prm.id);
  driverModel
    .find({
      $and: [
        {
          _id: prm.id
        },
        {
          ready: false
        }
      ]
    })
    .then(result => {
      if (result.length > 0) {
        let ord = orderModel
          .find({ riderId: prm.id })
          .populate("boncengerId")
          .populate("riderId")
          .sort({ tanggal: -1 })
          .exec();
        return [result, ord];
      }
    })
    .spread((resule, ord) => {
      if (ord[0].status == 1 || ord[0].status == 2 || ord[0].status == 3) {
        let order = new OrderController(ord[0]);
        order.getDetailOrder(x => {
          res.send({
            s: 1,
            statusCode: res.statusCode,
            message: "Order Id terakhir",
            dt: x
          });
        });
      } else {
        res.send({
          s: 1,
          statusCode: res.statusCode,
          message: "Tidak ditemukan Order terakhir"
        });
      }
    })
    .catch(err => {
      res.send({
        s: 0,
        statusCode: res.statusCode,
        dt: []
      });
    });
};

export const simulasiCancelOrder = (req, res) => {
  let query = req.query;
  let data = typeof query == "object" ? query : JSON.parse(query);
  let options = {
    method: "GET",
    url: config.apiUrl + ":" + config.apiPort + "/api/v3/ns/orderDetail",
    qs: { orderId: data.order_id },
    headers: {
      "content-type": "application/x-www-form-urlencoded",
      "cache-control": "no-cache",
      "x-access-token": data.token
    }
  };
  request(options, (error, response, body) => {
    let data = JSON.parse(body);
    if (data.dt.length > 0 && data.dt[0].status == 6) {
      // console.log('kesini yaa: batal order');
      res.send({
        msg: "batal order"
      });
    } else {
      // console.log('kesini yaa: terima order');
      res.send({
        msg: "terima order"
      });
    }
  });
};
