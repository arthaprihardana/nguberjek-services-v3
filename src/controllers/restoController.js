/**
 * @author: Artha Prihardana 
 * @Date: 2017-08-26 14:02:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-04 14:56:37
 */
import config from '../app.conf';
import restoModel from '../models/restoModel';
import objectHelper from '../helpers/objectHelper';
import resMessage from '../helpers/resHelper'

let detailResto = (data) => {
    let arr = [];
    for(let i in data) {
        arr.push({
            "_id": data[i]._id,
            "updated_at": data[i].updated_at,
            "created_at": data[i].created_at,
            // "foto": "https://media-cdn.tripadvisor.com/media/photo-s/05/58/fb/5a/ijen-resto-guest-house.jpg",
            "image": data[i].image || "https://media-cdn.tripadvisor.com/media/photo-s/05/58/fb/5a/ijen-resto-guest-house.jpg",
            "jamBuka": data[i].jamBuka,
            "kategori": (data[i].kategori != undefined) ? data[i].kategori.namaKategori : '',
            "koordinat": {
                "latitude": data[i].koordinat[1],
                "longitude": data[i].koordinat[0]
            },
            "alamat": data[i].alamat,
            "noTelp": data[i].noTelp,
            "namaResto": data[i].namaResto,
            "__v": data[i].__v,
            "show": data[i].show
        })
    }
    return arr;
}

export const createResto = (req, res, next) => {
    let body = req.body;
    let resto = new restoModel();
    
    resto.namaResto = (body.namaResto) ? body.namaResto : '';
    resto.kategori = (body.kategori) ? body.kategori : '';
    resto.alamat = (body.alamat) ? body.alamat : '';
    resto.noTelp = (body.noTelp) ? body.noTelp : '';
    resto.koordinat = (body.koordinat) ? body.koordinat.split(",").map(Number) : '';
    resto.jamBuka = (body.jamBuka) ? body.jamBuka : '';
    resto.image = (body.image) ? body.image : [];

    const reg = resto.save();
    reg
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Resto berhasil didaftarkan.\nSelamat datang di NGUBERJEK :)',
                null,
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let dtVal = Object.values(err.errors);
            let arr = [];
            let cnt = 0;
            for(var i=0; i<dtVal.length; i++) {
                cnt++;
                arr.push(dtVal[i].message);
                if(cnt == dtVal.length) {
                    let msg = resMessage(
                        0,
                        res.statusCode,
                        err.message+':\n'+ arr.toString().replace(new RegExp(',', 'g'),"\n"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    );
                    res.send(msg);
                }
            }
        });
} 

export const putResto = (req, res, next) => {
    let query = req.query;
    let body = req.body;
    if(body.koordinat) {
        body.koordinat = body.koordinat.split(",").map(Number)
    }
    let put = restoModel.update({_id:query._id}, body, {}).exec();
    put
        .then((response) => {
            let get = restoModel.findOne({_id:query._id}).exec();
            return [response,get];
        })
        .spread((response, get) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Profil Resto telah diperbaharui',
                null,
                response,
                body,
                get,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const delResto = (req, res, next) => {
    let query = req.query;
    let del = restoModel.update({_id:query._id}, {show: false}, {}).exec();
    del
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Profil Resto telah dihapus',
                null,
                response,
                null,
                null,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const getResto = (req, res, next) => {
    let objQry = new objectHelper(req.query, ['limit','page'])
    let limitPerPage = parseInt(req.query.limit) || 25;
    let page = parseInt(req.query.page) || 1;
    let search = {
        $and: [
            {show: true},
            {
                $or: [
                    {namaResto: new RegExp(req.query.search, "i")},
                    {alamat: new RegExp(req.query.search, "i")},
                    {jamBuka: new RegExp(req.query.search, "i")},
                ]
            }
        ]
    }
    let query = objQry.objekFilter()
    let find = {}

    if(query.search) {
        find = search
    } else if (query.all) {
        limitPerPage = 0
    } else {
        find = {
            $and: [
                {show: true},
                query||{}
            ]
        }
    }

    let getData = restoModel.find(find).populate('kategori').limit(limitPerPage).skip(limitPerPage * ( page - 1 )).exec();
    getData
        .then((response) => {
            let dataCount = restoModel.count(find).exec();
            // console.log('response ==>', response)
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data Resto',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                detailResto(response),
                // response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const getRadiusResto = (req, res, next) => {
    let objQry = new objectHelper(req.query, ['limit','page'])
    let limitPerPage = parseInt(req.query.limit) || 25;
    let page = parseInt(req.query.page) || 1;
    let query = objQry.objekFilter()
    let find = {
        $and: [
            {show: true},
            {
                koordinat: {
                    $geoWithin: {
                        $centerSphere: [ query.koordinat.split(',').map(Number), parseFloat(config.radiusResto)/3963.2 ]
                    }
                }
            }
        ]
    }

    let getData = restoModel.find(find).populate('kategori').limit(limitPerPage).skip(limitPerPage * ( page - 1 )).exec();
    getData
        .then((response) => {
            let dataCount = restoModel.count(find).exec();
            // console.log('response ==>', response)
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data Resto',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                detailResto(response),
                // response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}