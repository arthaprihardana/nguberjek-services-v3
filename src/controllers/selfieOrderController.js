/**
 * @author Artha Prihardana 
 * @Date 2017-01-30 09:18:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-04-27 08:00:03
 */
import selfieOrderModel from '../models/selfieOrderModel';
import driverModel from '../models/driverModel';
import objectHelper from '../helpers/objectHelper';
import generate from '../helpers/generateHelper';
import resMessage from '../helpers/resHelper';

class SelfieOrder {

    constructor(data) {
        this.data = data;
    }

    getDetailHistory(cb) {
        let dt = [];
        let count = 0;
        this.data.forEach((val, key, array) => {
            count++;
            dt.push({
                "_id": val._id,
                "updated_at": val.updated_at,
                "created_at": val.created_at,
                "riderId": {
                    "_id": val.riderId._id,
                    "senderId": val.riderId.senderId,
                    "lokasi": val.riderId.lokasi,
                    "noPolisi": val.riderId.noPolisi,
                    "merkKendaraan": val.riderId.merkKendaraan,
                    "typeKendaraan": val.riderId.typeKendaraan,
                    "jenisKelamin": val.riderId.jenisKelamin,
                    "role": val.riderId.role,
                    "noTelp": val.riderId.noTelp,
                    "email": val.riderId.email,
                    "nama": val.riderId.nama,
                    "photo": val.riderId.photo
                },
                "status": val.status,
                "waktu": val.waktu,
                "jarak": val.jarak,
                "harga": val.harga,
                "namaKonsumen": val.namaKonsumen,
                "noTelpKonsumen": val.noTelpKonsumen,
                "tanggal": val.tanggal,
                "orderId": val.orderId,
                "show": val.show,
                "lokasiAwal": val.lokasiAwal.lokasi,
                "latitudeLA": val.lokasiAwal.koordinatLokasi[1],
                "longitudeLA": val.lokasiAwal.koordinatLokasi[0],
                "tujuan": val.lokasiAkhir.tujuan,
                "latitudeLT": val.lokasiAkhir.koordinatTujuan[1],
                "longitudeLT": val.lokasiAkhir.koordinatTujuan[0]
            });

            if(array.length == count) {
                cb(dt);
            }
        });
        
    }

    getDetailHistoryFranchise(franchise, cb) {
        let dt = [];
        let count = 0;
        this.data.forEach((val, key, array) => {
            if(val.riderId != null && val.riderId.franchise == true && val.riderId.franchiseName == franchise) {
                count++;
                dt.push({
                    "_id": val._id,
                    "updated_at": val.updated_at,
                    "created_at": val.created_at,
                    "riderId": {
                        "_id": val.riderId._id,
                        "senderId": val.riderId.senderId,
                        "lokasi": val.riderId.lokasi,
                        "noPolisi": val.riderId.noPolisi,
                        "merkKendaraan": val.riderId.merkKendaraan,
                        "typeKendaraan": val.riderId.typeKendaraan,
                        "jenisKelamin": val.riderId.jenisKelamin,
                        "role": val.riderId.role,
                        "noTelp": val.riderId.noTelp,
                        "email": val.riderId.email,
                        "nama": val.riderId.nama,
                        "photo": val.riderId.photo
                    },
                    "status": val.status,
                    "waktu": val.waktu,
                    "jarak": val.jarak,
                    "harga": val.harga,
                    "namaKonsumen": val.namaKonsumen,
                    "noTelpKonsumen": val.noTelpKonsumen,
                    "tanggal": val.tanggal,
                    "orderId": val.orderId,
                    "show": val.show,
                    "lokasiAwal": val.lokasiAwal.lokasi,
                    "latitudeLA": val.lokasiAwal.koordinatLokasi[1],
                    "longitudeLA": val.lokasiAwal.koordinatLokasi[0],
                    "tujuan": val.lokasiAkhir.tujuan,
                    "latitudeLT": val.lokasiAkhir.koordinatTujuan[1],
                    "longitudeLT": val.lokasiAkhir.koordinatTujuan[0]
                });

                if(array.length == count) {
                    cb(dt, count);
                }
            } else {
                cb(dt, 0);
            }
        })
    }

    getDetailRequest(cb) {
        let dt = {
            "updated_at": this.data.updated_at,
            "created_at": this.data.created_at,
            "status": this.data.status,
            "waktu": this.data.waktu,
            "jarak": this.data.jarak,
            "harga": this.data.harga,
            "riderId": this.data.riderId,
            "namaKonsumen": this.data.namaKonsumen,
            "noTelpKonsumen": this.data.noTelpKonsumen,
            "tanggal": this.data.tanggal,
            "orderId": this.data.orderId,
            "_id": this.data._id,
            "show": true,
            "lokasiAwal": this.data.lokasiAwal.lokasi,
            "latitudeLA": this.data.lokasiAwal.koordinatLokasi[1],
            "longitudeLA": this.data.lokasiAwal.koordinatLokasi[0],
            "tujuan": this.data.lokasiAkhir.tujuan,
            "latitudeLT": this.data.lokasiAkhir.koordinatTujuan[1],
            "longitudeLT": this.data.lokasiAkhir.koordinatTujuan[0]
        }
        cb(dt);
    }

}

export const getHistory = (req, res) => {
    let objQry = new objectHelper(req.query, ["_id","limit","page", "dateStart", "dateEnd", "token", "all", "franchise"]);
    let limitPerPage = parseInt(req.query.limit)||25;
    let page = parseInt(req.query.page)||1;
    let dateStart = (req.query.dateStart) ? new Date(req.query.dateStart) : '';
    let dateEnd = (req.query.dateEnd) ? new Date(req.query.dateEnd) : '';
    let findByDate = (dateStart && dateEnd) ? {
        created_at: {
            $gte: dateStart,
            $lte: dateEnd
        }
    } : {};
    let search = {
        $and: [
            {show:true},
            {
                $or:[
                    {namaKonsumen: new RegExp(req.query.search, "i")},
                    {noTelpKonsumen: new RegExp(req.query.search, "i")},
                    {orderId: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    }
    let query = objQry.objekFilter();
    let find = {};
    if(req.query.search) {
        find = search;
    } else if(req.query.all) {
        limitPerPage = 0;
        find = {show:true};
    } else {
        find = {
            $and: [
                {show:true},
                findByDate,
                query||{}
            ]
        };
    }
    console.log('kesini');
    let getData = selfieOrderModel.find(find).populate('riderId').sort({"tanggal": -1}).limit(limitPerPage).skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let dataCount = selfieOrderModel.count(find).exec();
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            console.log('count', dataCount);
            if(dataCount > 0) {
                const selfie = new SelfieOrder(response);
                if(req.query.franchise) {
                    selfie.getDetailHistoryFranchise(req.query.franchise, (data, count) => {
                        let msg = resMessage(
                            1,
                            res.statusCode,
                            'Histori selfie order',
                            {
                                total: dataCount,
                                limit: limitPerPage,
                                page: page,
                                pages: Math.ceil(dataCount / limitPerPage)
                            },
                            null,
                            null,
                            data,
                            null,
                            null,
                            null
                        );
                        res.send(msg);
                    });
                } else {
                    selfie.getDetailHistory((data) => {
                        let msg = resMessage(
                            1,
                            res.statusCode,
                            'Histori selfie order',
                            {
                                total: dataCount,
                                limit: limitPerPage,
                                page: page,
                                pages: Math.ceil(dataCount / limitPerPage)
                            },
                            null,
                            null,
                            data,
                            null,
                            null,
                            null
                        );
                        res.send(msg);
                    });
                }
            } else {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Histori selfie order',
                    {
                        total: dataCount,
                        limit: limitPerPage,
                        page: page,
                        pages: Math.ceil(dataCount / limitPerPage)
                    },
                    null,
                    null,
                    response,
                    null,
                    null,
                    null
                );
                res.send(msg);
            }
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        })
}

export const addSelfieOrder = (req, res) => {
    let body = req.body;
    let gen = new generate();
    gen.setOrderId();
    let findDriver = driverModel.findOne({_id: body.riderId}).exec();
    findDriver
        .then((response) => {
            console.log('orderId==>', gen.getOrderId());
            let saldo = parseInt(response.saldo);
            if (saldo >= (parseInt(body.harga) * 0.2)) {
                let add = new selfieOrderModel();
                add.orderId = gen.getOrderId();
                add.tanggal = new Date();
		        add.noTelpKonsumen =  body.noTelpKonsumen;
		        add.namaKonsumen = body.namaKonsumen;
		        add.riderId = body.riderId;
		        add.lokasiAwal.lokasi = body.lokasiAwal.lokasi;
		        add.lokasiAwal.koordinatLokasi = body.lokasiAwal.koordinatLokasi.split(",").map(Number);
		        add.lokasiAkhir.tujuan = body.lokasiAkhir.tujuan;
		        add.lokasiAkhir.koordinatTujuan = body.lokasiAkhir.koordinatTujuan.split(",").map(Number);
		        add.harga = body.harga;
		        add.jarak = body.jarak;
		        add.waktu = body.waktu;
		        add.status = body.status;
                let simpan = add.save();
                return simpan;
            } else {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    'Saldo anda tidak mencukupi untuk permintaan order ini',
                    null,
                    null,
                    null,
                    null,
                    {},
                    null,
                    null
                );
                res.send(msg);
            }
        })
        .then((response) => {
            // let updateStatusDriver = driverModel.update({_id: body.riderId}, {ready: false}, {}).exec();
            driverModel.update({_id: body.riderId}, {ready: false}, {}).exec();
            const selfie = new SelfieOrder(response);
            selfie.getDetailRequest((data) => {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Selfie order telah berhasil diproses. Selamat Jalan :)',
                    null,
                    null,
                    null,
                    data,
                    null,
                    null,
                    null
                );
                res.send(msg);
            });
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const updateSelfieOrder = (req, res) => {
    let body = req.body;
    console.log("kesini", typeof body.status);
    if(body && body.orderId && body.status && body.riderId) {
        let findSelfieOrder = selfieOrderModel.findOne({orderId: body.orderId}).exec();
        findSelfieOrder
            .then((response) => {
                selfieOrderModel.update({orderId: body.orderId}, {status: body.status}).exec();
                switch (body.status) {
                    case "1":
                        let msg = resMessage(
                            1,
                            res.statusCode,
                            'Perjalanan telah dimulai. Hati-hati dalam perjalanan, semoga selamat sampai tujuan',
                            null,
                            null,
                            null,
                            {},
                            null,
                            null,
                            null
                        );
                        res.send(msg);
                        break;
                    case "2":
                        driverModel.findOne({_id:body.riderId}).exec((err, data) => {
                            let saldo = data.saldo;
                            let ps = parseInt(saldo) - (parseInt(response.harga) * 0.2);
                            driverModel.update({_id:body.riderId}, {ready: true, saldo: ps}, {}).exec();
                            let msg = resMessage(
                                1,
                                res.statusCode,
                                'Perjalanan telah selesai. Terima kasih telah menggunakan layanan selfie order. :)',
                                null,
                                null,
                                null,
                                {harga: parseInt(response.harga)},
                                null,
                                null,
                                null
                            );
                            res.send(msg);
                        });
                        break;
                    case "3":
                        driverModel.findOne({_id:body.riderId}).exec((err, data) => {
                            let saldo = data.saldo;
                            let ps = parseInt(saldo) - ((parseInt(response.harga) / 2) * 0.2);
                            driverModel.update({_id:body.riderId}, {ready: true, saldo: ps}, {}).exec();
                            selfieOrderModel.update({orderId: body.orderId}, {harga: (parseInt(response.harga) / 2) }).exec();
                            let msg = resMessage(
                                1,
                                res.statusCode,
                                `Perjalanan berakhir dikarenakan terjadi suatu hal dalam perjalanan anda. 
                                Harga Trip mendapat potongan 50% dari 
                                ${parseInt(response.harga)} menjadi 
                                ${(parseInt(response.harga) / 2)}`,
                                {harga: (parseInt(response.harga) / 2)},
                                null,
                                null,
                                {harga: (parseInt(response.harga) / 2)},
                                null,
                                null,
                                null
                            );
                            res.send(msg);
                        });
                        break;
                    default:
                        let msg2 = resMessage(
                            0,
                            res.statusCode,
                            'Ooops, Something went wrong!!!',
                            null,
                            null,
                            null,
                            null,
                            {},
                            null,
                            null
                        );
                        res.send(msg2);
                        break;
                }
            })
            .catch((err) => {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message,
                    null,
                    null,
                    null,
                    null,
                    res.errors,
                    null,
                    null
                );
                res.send(msg);
            });
    } else {
        let msg = resMessage(
            0,
            res.statusCode,
            'parameter orderId, riderId dan status harus diisi',
            null,
            null,
            null,
            null,
            {},
            null,
            null
        );
        res.send(msg);
    }
}

export const delSelfiOrder = (req, res) => {
    
}
