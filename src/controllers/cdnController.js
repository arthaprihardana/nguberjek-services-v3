/**
 * @author Artha Prihardana 
 * @Date 2017-02-03 21:50:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-05-13 17:28:20
 */
import config from '../app.conf';
import generate from '../helpers/generateHelper';
import fs from 'fs';
import multer from 'multer';
import Jimp from 'jimp';
import cdnModel from '../models/cdnModel';
import resMessage from '../helpers/resHelper';

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './temp_uploads');
    },
    filename: (req, file, callback) => {
        let originalname = file.originalname;
        let extension = originalname.split(".");
        callback(null, file.fieldname + '-' + Date.now() + '.' + extension[extension.length-1]);
    }
});
const uploadFile = multer({ storage : storage, limits: {fileSize: config.limitImageSize}}).single('userImage');
const cdnConfig = (base64image, cb) => {
    let imageBuffer = decodeBase64Image(base64image);
    Jimp.read(imageBuffer.data, function (err, image) {
        try {
            if(err) {
                cb(err);
            }
            let ext = image._originalMime.split("/");
            let newName = new generate();
            newName.setRandomNameImage();
            image.quality(100).write("./public/images/"+newName.getRandomNameImage()+"."+ext[1]);
            // cb(config.apiUrl+":"+config.apiPort+"/images/"+newName.getRandomNameImage()+"."+ext[1]);
            cb("images/"+newName.getRandomNameImage()+"."+ext[1]);
        } catch (e) {
            cb(e);
        }
    });
};
const decodeBase64Image = (dataString) => {
    let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }
    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    return response;
}

export const upload = (req, res) => {
    let filesBase64 = [];
    uploadFile(req, res, (err) => {
        try {
            if(err) {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message,
                    null,
                    null,
                    null,
                    null,
                    err.errors,
                    null,
                    null
                );
                res.send(msg);
            }
            filesBase64.push({
                fileName: req.file.originalname,
                base64: new Buffer(fs.readFileSync(req.file.path)).toString('base64')
            });
            fs.unlink(req.file.path);
            let dt = req.file;
            let base64 = "data:"+dt.mimetype+";base64,"+filesBase64[0].base64;
            let cdnmodel = new cdnModel();
            cdnmodel.fieldname = dt.fieldname;
            cdnmodel.originalname = dt.originalname;
            cdnmodel.encoding = dt.encoding;
            cdnmodel.mimetype = dt.mimetype;
            cdnmodel.destination = dt.destination;
            cdnmodel.filename = dt.filename;
            cdnmodel.path = dt.path;
            cdnmodel.size = dt.size;
            cdnmodel.base64 = base64;
            cdnConfig(base64, (x) => {
                cdnmodel.link = x;
                let simpan = cdnmodel.save();
                simpan
                    .then((response) => {
                        let msg = resMessage(
                            1,
                            res.statusCode,
                            'Gambar telah disimpan',
                            null,
                            null,
                            null,
                            {
                                "_id": response._id,
                                "fieldname": response.fieldname,
                                "originalname": response.originalname,
                                "mimetype": response.mimetype,
                                "size": response.size,
                                "path": response.path,
                                "filename": response.filename,
                                "destination": response.destination,
                                "encoding": response.encoding,
                                "link": config.apiUrl+":"+config.apiPort+"/"+x
                            },
                            null,
                            null,
                            null
                        );
                        res.send(msg);
                    })
                    .catch((err) => {
                        let msg = resMessage(
                            0,
                            res.statusCode,
                            err.message,
                            null,
                            null,
                            null,
                            null,
                            err.errors,
                            null,
                            null
                        );
                        res.send(msg);
                    });
            });
        } catch (error) {
            let msg = resMessage(
                0,
                res.statusCode,
                error,
                null,
                null,
                null,
                null,
                error,
                null,
                null
            );
            res.send(msg);
        }

        
    });
}

// export const getImage = (req, res) => {
//     let query = req.query;
//     if(query && query.link) {
//         let ceklink = cdnModel.findOne({link: query.link}).exec();
//         ceklink
//             .find((response) => {

//             })
//             .catch((err) => {

//             });

//         fs.stat("./public"+query.link, (err, response) => {
//             try {
//                 if(err) {
//                     res.send("ga ada");
//                 }

//                 res.send(response);
//             } catch(e) {
                
//             }
//         })
//     }
// }