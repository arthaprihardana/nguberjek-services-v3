/**
 * @author: Artha Prihardana 
 * @Date: 2017-10-08 15:33:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-21 15:58:29
 */
import config from '../app.conf'
import kategoriMenuModel from '../models/kategoriMenuModel'
import objectHelper from '../helpers/objectHelper'
import resMessage from '../helpers/resHelper'

let detailKategoriMenu = (data) => {
    let arr = [];
    for(let i in data) {
        arr.push({
            "_id": data[i]._id,
            "updated_at": data[i].updated_at,
            "created_at": data[i].created_at,
            "namaKategori": data[i].namaKategori,
            "image": data[i].image||"https://images.pexels.com/photos/70497/pexels-photo-70497.jpeg",
            "__v": data[i].__v,
            "show": data[i].show
        })
    }
    return arr;
}

export const createKategori = (req, res, next) => {
    let body = req.body;
    let kategori = new kategoriMenuModel()

    kategori.namaKategori = (body.namaKategori) ? body.namaKategori : '';
    kategori.image = (body.image) ? body.image : '';

    const reg = kategori.save()
    reg
    .then((response) => {
        let msg = resMessage(
            1,
            res.statusCode,
            'Kategori Menu berhasil disimpan',
            null,
            null,
            null,
            response,
            null,
            null,
            null
        );
        res.send(msg);
    })
    .catch((err) => {
        let dtVal = Object.values(err.errors);
        let arr = [];
        let cnt = 0;
        for(var i=0; i<dtVal.length; i++) {
            cnt++;
            arr.push(dtVal[i].message);
            if(cnt == dtVal.length) {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message+':\n'+ arr.toString().replace(new RegExp(',', 'g'),"\n"),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                );
                res.send(msg);
            }
        }
    });
}

export const putKategori = (req, res, next) => {
    let body = req.body;
    let query = req.query;

    const put = kategoriMenuModel.update({_id: query._id}, body, {}).exec();
    put
    .then((response) => {
        let msg = resMessage(
            1,
            res.statusCode,
            'Kategori Menu berhasil diperbaharui',
            null,
            null,
            null,
            response,
            null,
            null,
            null
        );
        res.send(msg);
    })
    .catch((err) => {
        let msg = resMessage(
            0,
            res.statusCode,
            err.message,
            null,
            null,
            null,
            null,
            err.errors,
            null,
            null
        );
        res.send(msg);
    });
}

export const delKategori = (req, res, next) => {
    let body = req.body;
    let query = req.query;

    const put = kategoriMenuModel.update({_id: query._id}, {show:false}, {}).exec();
    put
    .then((response) => {
        let msg = resMessage(
            1,
            res.statusCode,
            'Kategori Menu berhasil diphapus',
            null,
            null,
            null,
            response,
            null,
            null,
            null
        );
        res.send(msg);
    })
    .catch((err) => {
        let msg = resMessage(
            0,
            res.statusCode,
            err.message,
            null,
            null,
            null,
            null,
            err.errors,
            null,
            null
        );
        res.send(msg);
    });
}

export const getKategori = (req, res, next) => {
    let objQry = new objectHelper(req.query, ['limit','page'])
    let limitPerPage = parseInt(req.query.limit) || 25;
    let page = parseInt(req.query.page) || 1;
    let search = {
        $and: [
            {show: true},
            {
                $or: [
                    {namaKategori: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    }
    let query = objQry.objekFilter()
    let find = {}

    if(query.search) {
        find = search
    } else if (query.all) {
        limitPerPage = 0
    } else {
        find = {
            show: true,
            $and: [
                query||{}
            ]
        }
    }

    let getData = kategoriMenuModel.find(find).limit(limitPerPage).skip(limitPerPage * ( page - 1 )).exec();
    getData
        .then((response) => {
            let dataCount = kategoriMenuModel.count(find).exec();
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data Kategori Menu',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                detailKategoriMenu(response),
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}