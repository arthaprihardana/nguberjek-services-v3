/**
 * @author Artha Prihardana 
 * @Date 2017-02-25 16:40:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-11-12 12:56:03
 */
import config from '../app.conf';
import VeritransModel from '../models/veritransModel';
import GenerateHelper from '../helpers/generateHelper';
import DriverModel from '../models/driverModel';
import {
    chargeHelper,
    statusHelper,
    notificationHelper
} from '../helpers/veritransHelper';
import TanggalHelper from '../helpers/tanggalHelper';
import resMessage from '../helpers/resHelper';
import objectHelper from '../helpers/objectHelper';
import {
    sendNotification
} from '../helpers/firebaseHelper';

class VeritransController {
    constructor(data) {
        this.data = data;
    }
}

export const charge = (req, res, next) => {
    let body = req.body;
    let payment = new VeritransModel();
    let findDriver = DriverModel.findById(body.riderId).exec();
    findDriver
        .then((response) => {
            let namaCustomer = response.nama.split(" ");
            let genOrderID = new GenerateHelper();
            genOrderID.setVtOrderId();
            let orderID =  genOrderID.getVtOrderId();
            // console.log('generate order id ==>', orderID);
            let data = {
                "payment_type": "cstore",
                "transaction_details": {
                    "order_id": orderID,
                    "gross_amount": body.amount
                },
                "cstore": {
                  "store": "Indomaret",
                  "message": "TopUp " + response.nama + " Rp." + body.amount
                },
                "customer_details": {
                    "first_name": namaCustomer[0] || "-",
                    "last_name": namaCustomer[1] || "-",
                    "email": response.email,
                    "phone": response.phone
                },
                "item_details": [
                  {
                    "id": orderID,
                    "name": "Pembayaran topup nguberjek",
                    "price": body.amount,
                    "quantity": 1
                  }
                ]
              }


            // let data = {
            //     "payment_type": "cstore",
            //     "cstore": {
            //         "store": "Indomaret",
            //         "message": "TopUp " + response.nama + " Rp." + body.amount
            //     },
            //     "transaction_details": {
            //         "order_id": orderID,
            //         "gross_amount": body.amount
            //     },
            //     "customer_details": {
            //         "first_name": namaCustomer[0] || "-",
            //         "last_name": namaCustomer[1] || "-",
            //         "email": response.email,
            //         "phone": response.phone
            //     },
            //     "item_details": [{
            //         "id": orderID,
            //         "name": "Pembayaran topup nguberjek",
            //         "price": body.amount,
            //         "quantity": 1
            //     }]
            // };
            chargeHelper(data, (e, r) => {
                if(e) {
                    console.log('e==>', e)
                    res.send(e);
                } else {
                    console.log('result==>', r);
                    let expired = new Date(new Date(r.transaction_time).getTime() + 60 * 60 * 24 * 1000);
                    console.log('expired==>', expired);
                    payment.riderId = body.riderId;
                    payment.transactionID = r.transaction_id;
                    payment.orderId = r.order_id;
                    payment.grossAmount = r.gross_amount;
                    payment.paymentType = r.payment_type;
                    payment.transactionTime = r.transaction_time;
                    payment.expiredTime = expired;
                    payment.transactionStatus = r.transaction_status;
                    payment.paymentCode = r.payment_code;
                    let simpan = payment.save();
                    simpan
                        .then((response) => {
                            let expiredTime = new TanggalHelper(response.expiredTime);
                            let formatExpiredTime = expiredTime.localeLLL();
                            let msg = resMessage(
                                1,
                                res.statusCode,
                                "Topup berhasil",
                                {
                                    info: "Mohon selesaikan pembayaran sebelum tanggal "+formatExpiredTime+". Apabila melewati batas waktu, pesanan Anda akan otomatis dibatalkan.",
                                    cara_pembayaran: `1. Pergi ke Indomaret terdekat dan berikan Kode Pembayaran Anda ke kasir
                                                      2. Kasir Indomaret akan mengkonfirmasi transaksi dengan menanyakan Jumlah Tagihan dan Nama Toko
                                                      3. Bayar sesuai Jumlah Tagihan Anda
                                                      4. Setelah pembayaran diterima, Anda akan menerima konfirmasi yang dikirimkan ke email
                                                      5. Simpanlah struk transaksi Indomaret Anda sebagai bukti pembayaran`
                                },
                                response,
                                payment,
                                null,
                                null,
                                null,
                                null
                            );
                            res.send(msg);
                        })
                        .catch((err) => {
                            let msg = resMessage(
                                0,
                                res.statusCode,
                                err,
                                null,
                                null,
                                null,
                                null,
                                err.messages,
                                null,
                                null
                            );
                            res.send(msg);
                        });
                }
            })
        })
        .catch((err) => {
            console.log('err', err);
            let msg = resMessage(
                0,
                res.statusCode,
                err,
                null,
                null,
                null,
                null,
                err.messages,
                null,
                null
            );
            res.send(msg);
        })
}

export const status = (req, res, next) => {
    let orderId = req.params.order_id;
    statusHelper(orderId, (e, r) => {
        if(e) {
            res.json(e);
        } else {
            let result = JSON.parse(r);
            let expired = new Date(new Date(result.transaction_time).getTime() + 60 * 60 * 24 * 1000);
            // let tanggalHelper = new TanggalHelper(expired);
            // let formatDate = tanggalHelper.localeLLL();
            let response = {
                transactionID: result.transaction_id,
                store: result.store,
                orderId: result.order_id,
                grossAmount: result.gross_amount,
                paymentType: result.payment_type,
                transactionTime: result.transaction_time,
                expiredTime: expired,
                transactionStatus: result.transaction_status,
                paymentCode: result.payment_code,
            };
            // console.log('result==>', response);
            let msg = resMessage(
                1,
                res.statusCode,
                'Status Pembayaran Midtrans',
                null,
                {
                    cara_pembayaran: (result.transaction_status == "pending") ? "1. Pergi ke Indomaret terdekat dan berikan Kode Pembayaran Anda ke kasir \n" +
                    "2. Kasir Indomaret akan mengkonfirmasi transaksi dengan menanyakan Jumlah Tagihan dan Nama Toko \n"+
                    "3. Bayar sesuai Jumlah Tagihan Anda \n" +
                    "4. Setelah pembayaran diterima, Anda akan menerima konfirmasi yang dikirimkan ke email \n"+
                    "5. Simpanlah struk transaksi Indomaret Anda sebagai bukti pembayaran" : result.transaction_status
                },
                null,
                response,
                null,
                null,
                null,
            )
            res.send(msg);
        }
    });
}

export const getPayment = (req, res, next) => {
    let qry=req.query
        , limitPerPage = parseInt(qry.limit)||25
        , page = parseInt(qry.page)||1;
    let objQry = new objectHelper(qry, ["limit","page", "all", "search"]);
    let newQry = objQry.objekFilter();
    let find = (newQry) ? newQry : {};
    if(qry.all) {
        limitPerPage = 0;
    } else {
        find = {
            $and: [
                newQry||{}
            ]
        }
    }
    let getData = VeritransModel
                    .find(find)
                    .populate('riderId')
                    .populate({
                        path: 'riderId.franchiseName',
                        model: 'admin',
                    })
                    .sort({transactionTime: -1})
                    .limit(limitPerPage)
                    .skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let dataCount = VeritransModel.count(find).exec();
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                "Get Pendapatan",
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                "Error Get Pendapatan",
                null,
                null,
                null,
                null,
                err,
                null,
                null
            );
            res.send(msg);
        });
}

export const midtransNotifikasi = (req, res, next) => {
    var body=req.body;
    notificationHelper(body, function(e,resp) {
        if(e) {
            let msg = resMessage(
                0,
                res.statusCode,
                "Error Notifikasi",
                null,
                null,
                null,
                null,
                e,
                null,
                null
            )
            res.send(msg);
        } else {
            var status_change = body.transaction_status;
            var order_id = body.order_id;
            if(status_change=="settlement") {
                let updatePayment = VeritransModel.update({orderId:order_id}, {transactionStatus:status_change}, {}).exec();
                updatePayment
                    .then((response) => {
                        var saldobefore = parseFloat(result.riderId.saldo);
                        var saldoafter = saldobefore + parseFloat(body.gross_amount);
                        let updateDriver = DriverModel.update({_id:result.riderId},{saldo: saldoafter},{}).exec();
                        return [response, updateDriver];
                    })
                    .spread((response, driver) => {
                        var message = {
                            title: "Info",
                            status: 12,
                            message: "Pembayaran TopUp Melalui Indomaret telah berhasil"
                        };
                        var receiverId = result.riderId.senderId;
                        sendNotification(message, receiverId, function(rslt) {
                            let msg = resMessage(
                                1,
                                res.statusCode,
                                "Pembayaran TopUp Melalui Indomaret telah berhasil",
                                null,
                                null,
                                null,
                                rslt,
                                null,
                                null,
                                null
                            );
                            res.send(msg);
                        });
                    })
                    .catch((err) => {
                        let msg = resMessage(
                            0,
                            res.statusCode,
                            "Error",
                            null,
                            null,
                            null,
                            null,
                            err,
                            null,
                            null
                        );
                        res.send(msg);
                    });
            } else {
                res.send({
                    s:1,
                    msg:"Notification from veritrans",
                    statusCode: res.statusCode,
                    response: resp
                });
            }
        }
    });
}

export const bisaTopUp = (req, res, next) => {
    var body = req.body;
    let findDriver = DriverModel.find({ 
        "$or": [
            {username: body.id},
            {noTelp: body.id},
            {email: body.id}
        ]
    }).exec();
    findDriver
        .then((response) => {
            let namaCustomer = response[0].nama.split(" ");
            let genOrderID = new GenerateHelper();
            let orderID =  genOrderID.getVtOrderId();
            let transactionId = genOrderID.getVtOrderId();
            let expired = new Date(new Date().getTime() + 60 * 60 * 24 * 1000);
            let payment = new VeritransModel();

            payment.riderId = response[0]._id;
            payment.transactionID = transactionId;
            payment.orderId = orderID;
            payment.grossAmount = body.gross_amount;
            payment.paymentType = body.payment_type;
            payment.transactionTime = new Date();
            payment.expiredTime = expired;
            payment.transactionStatus = body.transaction_status;
            payment.paymentCode = body.payment_code;
            let simpan = payment.save();
            simpan
                .then((response2) => {
                    let put = DriverModel.update(
                        {_id: response[0]._id}, 
                        { saldo: (parseFloat(response[0].saldo) + parseFloat(body.gross_amount)) },
                        {}
                    ).exec();
                    // return [response2, put];
                    // return response2
                    var message = {
                        title: "Info",
                        status: 12,
                        message: "Pembayaran TopUp Melalui "+body.payment_type+" telah berhasil"
                    };
                    var receiverId = response[0].senderId;
                    sendNotification(message, receiverId, (rslt) => {
                        // console.log('rslt ==>', rslt);
                        let msg = resMessage(
                            1,
                            res.statusCode,
                            "Pembayaran TopUp Melalui "+body.payment_type+" telah berhasil",
                            null,
                            null,
                            null,
                            response2,
                            null,
                            null,
                            null
                        );
                        res.send(msg);
                    });
                    // let msg = resMessage(
                    //     1,
                    //     res.statusCode,
                    //     "Pembayaran TopUp Melalui "+body.payment_type+" telah berhasil",
                    //     null,
                    //     null,
                    //     null,
                    //     response2,
                    //     null,
                    //     null,
                    //     null
                    // );
                    // res.send(msg);
                })
                // .spread((response2, put) => {
                //     // res.send(response2)
                //     // var message = {
                //     //     title: "Info",
                //     //     status: 12,
                //     //     message: "Pembayaran TopUp Melalui "+body.payment_type+" telah berhasil"
                //     // };
                //     // var receiverId = response[0].senderId;
                //     // sendNotification(message, receiverId, (rslt) => {
                //     //     let msg = resMessage(
                //     //         1,
                //     //         res.statusCode,
                //     //         "Pembayaran TopUp Melalui "+body.payment_type+" telah berhasil",
                //     //         null,
                //     //         null,
                //     //         null,
                //     //         rslt,
                //     //         null,
                //     //         null,
                //     //         null
                //     //     );
                //     //     res.send(msg);
                //     // });
                // })
                .catch((error) => {
                    let msg = resMessage(
                        0,
                        res.statusCode,
                        "Error isi saldo",
                        null,
                        null,
                        null,
                        null,
                        error,
                        null,
                        null
                    );
                    res.send(msg);
                })
        })
        .catch((error) => {
            let msg = resMessage(
                0,
                res.statusCode,
                "Error mencari driver",
                null,
                null,
                null,
                null,
                error,
                null,
                null
            );
            res.send(msg);
        })
}

export const pendapatan = (req, res, next) => {

}

export const grafik = (req, res, next) => {
    
}