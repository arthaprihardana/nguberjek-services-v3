/**
 * @author Artha Prihardana 
 * @Date 2017-01-29 07:51:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-08-05 13:49:25
 */
import config from '../app.conf';
import driverModel from '../models/driverModel';
import orderModel from '../models/orderModel';
import selfieorderModel from '../models/selfieOrderModel';
import auth from '../helpers/authHelper';
import objectHelper from '../helpers/objectHelper';
import tanggalHelper from '../helpers/tanggalHelper';
import resMessage from '../helpers/resHelper';
import mongoose from 'mongoose';
import nodemailer from 'nodemailer';
import generate from '../helpers/generateHelper';

const tanggal = new tanggalHelper();
const ObjectId = mongoose.Types.ObjectId;
const rentang = {
    hari: {
        "$gte": new Date(tanggal.startOf('day').toISOString())
    },
    bulan: {
        "$gte": new Date(tanggal.startOf('month').toISOString())
    },
    tahun: {
        "$gte": new Date(tanggal.startOf('year').toISOString())
    }
};

class DriverController {
    constructor(data) {
        this.data = data;
    }

    getDetail() {
        let data=this.data;
        let dt=[];
        let cnt=0;
        for(let i=0; i<data.length; i++) {
            cnt++;
            dt.push({
                _id: data[i]._id,
                username: data[i].username,
                nama: data[i].nama,
                email: data[i].email,
                noTelp: data[i].noTelp,
                jenisKelamin: data[i].jenisKelamin,
                saldo: data[i].saldo,
                photo: data[i].photo,
                role: data[i].role,
                senderId: data[i].senderId,
                noPolisi: data[i].noPolisi,
                merkKendaraan: data[i].merkKendaraan,
                typeKendaraan: data[i].typeKendaraan,
                franchise: data[i].franchise,
                franchiseName: data[i].franchise && data[i].franchiseName != null ? data[i].franchiseName.nama : "-",
                lokasi: data[i].lokasi,
                latitude: (data[i].koordinatTerakhir && data[i].koordinatTerakhir.length > 0) ? data[i].koordinatTerakhir[1] : 0,
                longitude: (data[i].koordinatTerakhir && data[i].koordinatTerakhir.length > 0) ? data[i].koordinatTerakhir[0] : 0,
                show: data[i].show
            });
            if(cnt == data.length) {
                return dt;
            }
        }
    }
}

export const registrasiDriver = (req, res) => {
    const body = req.body;
    const drivermodel = new driverModel();
    const generatePassword = new auth(body.username, body.password);

    drivermodel.nama = (body.nama) ? body.nama : '';
    drivermodel.email = (body.email) ? body.email : '';
    drivermodel.noTelp = (body.noTelp) ? body.noTelp : '';
    drivermodel.tempatLahir = (body.tempatLahir) ? body.tempatLahir : '';
    drivermodel.tglLahir = (body.tglLahir) ? body.tglLahir : '';
    drivermodel.alamat = (body.alamat) ? body.alamat : '';
    drivermodel.role = (body.role) ? body.role.replace(/\s/g, '') : '';
    drivermodel.jenisKelamin = (body.jenisKelamin) ? body.jenisKelamin : '';
    drivermodel.typeKendaraan = (body.typeKendaraan) ? body.typeKendaraan : '';
    drivermodel.merkKendaraan = (body.merkKendaraan) ? body.merkKendaraan : '';
    drivermodel.noPolisi = (body.noPolisi) ? body.noPolisi : '';
    drivermodel.lokasi = (body.lokasi) ? body.lokasi : '';
    drivermodel.franchise = (body.franchise) ? body.franchise : false;
    drivermodel.franchiseName = (body.franchiseName) ? body.franchiseName : null;
    drivermodel.username = (body.username) ? body.username : '';
    drivermodel.password = generatePassword.createHash();
    drivermodel.deviceName = (body.deviceName) ? body.deviceName : '';
    drivermodel.deviceId = (body.deviceId) ? body.deviceId : '';
    drivermodel.senderId = (body.senderId) ? body.senderId : '';
    drivermodel.koordinatTerakhir = (drivermodel.koordinatTerakhir) ? body.koordinatTerakhir.split(",").map(Number) : [];
    
    let save = drivermodel.save();
    save
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Akun rider anda berhasil didaftarkan.\nAnda terdaftar sebagai '+body.role+'.\nSelamat datang di NGUBERJEK. :)',
                null,
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const loginDriver = (req, res) => {
    let username = req.body.username || '';
    let password = req.body.password || '';
    let senderId = req.body.senderId;
    let find = driverModel.findOne({
        $and: [{
            $or: [{
                username: username
            }, {
                email: username
            }],
            show: true
        }]
    }).populate('franchiseName').exec();
    find
        .then((response) => {
            return response;
        })
        .then((response) => {
            let authHelper = new auth(username, password);
            let pwdvalidate = authHelper.compareSync(response.password);
            if(pwdvalidate) {
                console.log("response", response);
                let dt = response;
                let token = authHelper.genToken();
                let rating = orderModel
                    .aggregate([
                        {
                            "$match": {
                                "riderId": new ObjectId(dt._id),
                            }
                        },
                        {
                            $group: {
                                _id: "$riderId",
                                "avgRating": { 
                                    "$avg": "$rating" 
                                }
                            }
                        }
                    ]).exec();
                return [dt, token, rating];
            } else {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    'Password yang anda masukan salah',
                    null,
                    null,
                    null,
                    null,
                    null,
                    '',
                    null
                );
                res.send(msg);
            }
        })
        .spread((dt, token, rating) => {
            driverModel.update({_id:dt._id}, {token:token.token, senderId:senderId},{},() => {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Login berhasil',
                    null,
                    null,
                    null,
                    {
                        _id: dt._id,
                        username: dt.username,
                        nama: dt.nama,
                        email: dt.email,
                        noTelp: dt.noTelp,
                        jenisKelamin: dt.jenisKelamin,
                        lokasi: dt.lokasi,
                        show: dt.show,
                        role: dt.role,
                        photo: dt.photo,
                        deviceName: dt.deviceName,
                        deviceId: dt.deviceId,
                        senderId: senderId,
                        typeKendaraan: dt.typeKendaraan,
                        merkKendaraan: dt.merkKendaraan,
                        noPolisi: dt.noPolisi,
                        saldo: dt.saldo,
                        ready: dt.ready,
                        latitude: (dt.koordinatTerakhir) ? dt.koordinatTerakhir[1] : 0,
                        longitude: (dt.koordinatTerakhir) ? dt.koordinatTerakhir[0] : 0,
                        // franchise: (dt.franchiseName) ? true : false,
                        // franchiseDetail: (dt.franchiseName) ? {
                        //     _id: dt.franchiseName._id,
                        //     nama: dt.franchiseName.nama
                        // } : {},
                        franchise: (dt.franchiseName) ? dt.franchiseName.nama : '',
                        rating: (rating.length > 0) ? Math.ceil(rating[0].avgRating) : 0,
                    },
                    null,
                    token.token,
                    token.expires
                );
                res.send(msg);
            });
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                'Username yang anda masukan salah',
                null,
                null,
                null,
                null,
                err.errors,
                '',
                null
            );
            res.send(msg);
        });
}

export const getDriver = (req, res) => {
    let objQry = new objectHelper(req.query, ['limit','page','dateStart','dateEnd','token','password']);
    let limitPerPage = parseInt(req.query.limit)||25;
    let page = parseInt(req.query.page)||1;
    let dateStart = (req.query.dateStart) ? new Date(req.query.dateStart) : '';
    let dateEnd = (req.query.dateEnd) ? new Date(req.query.dateEnd) : '';
    let findByDate = (dateStart && dateEnd) ? {
        created_at: {
            $gte: dateStart,
            $lte: dateEnd
        }
    } : {};
    let query = objQry.objekFilter();
    let find = {};
    let search = {
        $and: [
            {show: query.show||true},
            {role: query.role||new RegExp("ng", "i")},
            {
                $or:[
                    {nama: new RegExp(req.query.search, "i")},
                    {email: new RegExp(req.query.search, "i")},
                    {username: new RegExp(req.query.search, "i")},
                    {noTelp: new RegExp(req.query.search, "i")},
                    {lokasi: new RegExp(req.query.search, "i")},
                    {role: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    };
    if(query.search) {
        find = search;
    } else if(query.all) {
        find = { show: true };
        limitPerPage = 0;
    } else {
        console.log('kesini 2');
        find = {
            $and: [
                // {show:true},
                {show: req.query.show != undefined ? req.query.show : true},
                findByDate,
                query||{}
            ]
        }
    }
    
    let getData = driverModel.find(find).populate("franchiseName").limit(limitPerPage).skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let dataCount = driverModel.count(find).exec();
            return [dataCount,response];
        })
        .spread((dataCount, response) => {
            let detailDriver = new DriverController(response);
            let msg = resMessage(
                1,
                res.statusCode,
                'Data driver',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                response.length > 0 ? detailDriver.getDetail() : response,
                // response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const updateDriver = (req, res) => {
    let query = req.query;
    let body = req.body;
    if(body.password) {
        let genPwd = new auth(null, body.password);
        body.password = genPwd.createHash();
    }
    let put = driverModel.update({_id:query._id}, body, {}).exec();
    put
        .then((response) => {
            let get = driverModel.findOne({_id:query._id}).exec();
            return [response, get];
        })
        .spread((response, get) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Profil driver telah diperbaharui',
                null,
                response,
                body,
                get,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const deleteDriver = (req, res) => {
    let query = req.query;
    let del = driverModel.update({_id:query._id},{show:false},{}).exec();
    del
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Profil driver telah dihapus',
                null,
                response,
                null,
                null,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const realtimeRadiusDriver = (req, res) => {
    let limitPerPage = parseInt(req.query.limit)||25;
    let page = parseInt(req.query.page)||1;
    let dateStart = (req.query.dateStart) ? new Date(req.query.dateStart) : '';
    let dateEnd = (req.query.dateEnd) ? new Date(req.query.dateEnd) : '';
    let objQry = new objectHelper(req.query, ['limit','page','dateStart','dateEnd','token','password']);
    let findDate = (dateStart && dateEnd) ? {
        created_at: {
            $gte: dateStart,
            $lte: dateEnd
        }
    } : {};
    let search = {
        $and: [
            {show:true},
            {
                $or:[
                    {nama: new RegExp(req.query.search, "i")},
                    {email: new RegExp(req.query.search, "i")},
                    {username: new RegExp(req.query.search, "i")},
                    {noTelp: new RegExp(req.query.search, "i")},
                    {deviceName: new RegExp(req.query.search, "i")},
                    {jenisKelamin: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    };
    let query = objQry.objekFilter();
    let find = {
            $and: [{
                koordinatTerakhir: {
                    $geoWithin: {
                        $centerSphere: [ query.koordinat.split(',').map(Number), parseFloat(config.radius)/3963.2 ]
                    }
                },
                ready: true,
                show: true,
                role: query.role,
                senderId : { $ne: "" },
                token: { $ne: "" },
                saldo: { $gt : 0 }
            }]
        };
    let getData = driverModel.find(find).limit(limitPerPage).skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let dataCount = driverModel.count(find).exec();
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            // console.log('====================================');
            // console.log('response', response);
            // console.log('====================================');
            let detailData = new DriverController(response);
            let msg = resMessage(
                1,
                res.statusCode,
                'Radius driver',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                (response.length > 0) ? detailData.getDetail() : response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const realtimePosition = (/**req, res*/_id, koordinat) => {
    // let query = req.query;
    // let body = req.body;
    // let realPosition = driverModel.update({"_id":query._id},{"koordinatTerakhir":body.koordinat.split(',').map(Number)}).exec();
    // realPosition
    //     .then((response) => {
    //         let msg = resMessage(
    //             1,
    //             res.statusCode,
    //             'Update koordinat sukses',
    //             null,
    //             response,
    //             null,
    //             null,
    //             null,
    //             null,
    //             null
    //         );
    //         res.send(msg);
    //     })
    //     .catch((err) => {
    //         let msg = resMessage(
    //             0,
    //             res.statusCode,
    //             'Update koordinat gagal',
    //             null,
    //             null,
    //             null,
    //             null,
    //             null,
    //             null,
    //             null
    //         );
    //         res.send(msg);
    //     });
    // console.log('id', _id);
    // console.log('koordinat', koordinat);
    // console.log('koordinat', koordinat.split(',').map(Number));
    let realPosition = driverModel.update({_id:_id},{koordinatTerakhir:koordinat.split(',').map(Number)}).exec();
    return realPosition;
}

export const realtimeDriverOn = (req, res) => {
    let limitPerPage = parseInt(req.query.limit)||25;
    let page = parseInt(req.query.page)||1;
    let dateStart = (req.query.dateStart) ? new Date(req.query.dateStart) : '';
    let dateEnd = (req.query.dateEnd) ? new Date(req.query.dateEnd) : '';
    let objQry = new objectHelper(req.query, ['limit','page','dateStart','dateEnd','token','password']);
    let findDate = (dateStart && dateEnd) ? {
        created_at: {
            $gte: dateStart,
            $lte: dateEnd
        }
    } : {};
    let search = {
        $and: [
            {show:true},
            {
                $and: [
                    {senderId: {$ne: ""}},
                    {senderId: {$ne: null}},
                    {senderId: {$ne: undefined}},
                ]
            },
            {
                $and: [
                    {token: {$ne: ""}},
                    {token: {$ne: null}},
                    {token: {$ne: undefined}},
                ]
            },
            {
                $or:[
                    {nama: new RegExp(req.query.search, "i")},
                    {email: new RegExp(req.query.search, "i")},
                    {username: new RegExp(req.query.search, "i")},
                    {noTelp: new RegExp(req.query.search, "i")},
                    {deviceName: new RegExp(req.query.search, "i")},
                    {jenisKelamin: new RegExp(req.query.search, "i")}
                ]
            }
        ]
    };
    let query = objQry.objekFilter();
    let find = {};
    if(query.search) {
        find = search;
    } else if(query.all) {
        limitPerPage = 0;
    } else {
        find = {
            $and: [
                {show:true},
                {
                    $and: [
                        {senderId: {$ne: ""}},
                        {senderId: {$ne: null}},
                        {senderId: {$ne: undefined}},
                    ]
                },
                {
                    $and: [
                        {token: {$ne: ""}},
                        {token: {$ne: null}},
                        {token: {$ne: undefined}},
                    ]
                },
                findDate,
                query||{}
            ]
        }
    }
    let getData = driverModel.find(find).limit(limitPerPage).skip(limitPerPage * (page - 1)).exec();
    getData
        .then((response) => {
            let dataCount = driverModel.count(find).exec();
            return [dataCount, response];
        })
        .spread((dataCount, response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Data driver on status',
                {
                    total: dataCount,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(dataCount / limitPerPage)
                },
                null,
                null,
                response,
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                err.message,
                null,
                null,
                null,
                null,
                err.errors,
                null,
                null
            );
            res.send(msg);
        });
}

export const testLogout = (req, res) => {
    res.send({});
}

export const logoutDriver = (req, res) => {
    let token = req.headers.token;
    let query = req.query;
    if(query && query._id) {
        let logout = driverModel.update({_id: new ObjectId(query._id)},{token:"", senderId: ""}, {}).exec();
        logout
            .then((response) => {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Anda telah keluar dari sistem',
                    null,
                    null,
                    null,
                    response,
                    null,
                    null,
                    null
                );
                res.send(msg);
            })
            .catch((err) => {
                let msg = resMessage(
                    0,
                    res.statusCode,
                    err.message,
                    null,
                    null,
                    null,
                    null,
                    err.errors,
                    null,
                    null
                );
                res.send(msg);
            });
    }
}

export const pendapatanOrder = (req, res) => {
    let query = req.query;
    let pendapatan = orderModel
                        .aggregate([
                            {
                                "$match": {
                                    "riderId": new ObjectId(query.driverId),
                                    "$or": [
                                        {status: 4},
                                        {status: 5}
                                    ],
                                    "tanggal": (query.rentang) ? rentang[query.rentang] : rentang["hari"]
                                }
                            },
                            {
                                $group: {
                                    _id: "$riderId",
                                    "TotalPendapatan": {
                                        "$sum": "$harga"
                                    }
                                }
                            }
                        ]).exec();
                        console.log("rentang==>", rentang[query.rentang]);
    pendapatan
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan order',
                null,
                null,
                null,
                parseFloat(response[0].TotalPendapatan),
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            console.log(3)
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan order',
                null,
                null,
                null,
                "0",
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const pendapatanSelfieOrder = (req, res) => {
    let query = req.query;
    let pendapatan = selfieorderModel
                        .aggregate([
                            {
                                "$match": {
                                    "riderId": new ObjectId(query.driverId),
                                    "$or": [
                                        {status: 2},
                                        {status: 3}
                                    ],
                                    "tanggal": (query.rentang) ? rentang[query.rentang] : rentang["hari"]
                                }
                            },
                            {
                                $group: {
                                    _id: "$riderId",
                                    "TotalPendapatan": {
                                        "$sum": "$harga"
                                    }
                                }
                            }
                        ]).exec();
    pendapatan
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan selfie order',
                null,
                null,
                null,
                parseFloat(response[0].TotalPendapatan),
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan selfie order',
                null,
                null,
                null,
                0,
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const totalPendapatan = (req, res) => {
    let query = req.query;
    let pendapatanOrder = orderModel
        .aggregate([
            {
                "$match": {
                    "riderId": new ObjectId(query.driverId),
                    "$or": [
                        {status: 4},
                        {status: 5}
                    ],
                    "tanggal": (query.rentang) ? rentang[query.rentang] : rentang["hari"]
                }
            },
            {
                $group: {
                    _id: "$riderId",
                    "TotalPendapatan": {
                        "$sum": "$harga"
                    }
                }
            }
        ]).exec();
    pendapatanOrder
        .then((order) => {
            let pendapatanSelfieOrder = selfieorderModel
                .aggregate([
                    {
                        "$match": {
                            "riderId": new ObjectId(query.driverId),
                            "$or": [
                                {status: 2},
                                {status: 3}
                            ],
                            "tanggal": (query.rentang) ? rentang[query.rentang] : rentang["hari"]
                        }
                    },
                    {
                        $group: {
                            _id: "$riderId",
                            "TotalPendapatan": {
                                "$sum": "$harga"
                            }
                        }
                    }
                ]).exec();
            return [order, pendapatanSelfieOrder];
        })
        .spread((order, selfieOrder) => {
            
            // console.log('====================================');
            // console.log('order==>', order);
            // console.log('selfieOrder==>', selfieOrder);
            // console.log('====================================');
            let txtOrder = (order.length > 0 && order[0].TotalPendapatan != undefined) ? parseFloat(order[0].TotalPendapatan) : 0;
            let txtSelfieOrder = (selfieOrder.length > 0 && selfieOrder[0].TotalPendapatan != undefined) ? parseFloat(selfieOrder[0].TotalPendapatan) : 0;
            // res.send({
            //     order: txtOrder,
            //     selfieOrder: txtSelfieOrder,
            //     total: txtOrder + txtSelfieOrder
            // });
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan order',
                null,
                null,
                null,
                {
                    order: txtOrder,
                    selfieOrder: txtSelfieOrder,
                    total: txtOrder + txtSelfieOrder
                },
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            console.log(3)
            let msg = resMessage(
                1,
                res.statusCode,
                'Total pendapatan order',
                null,
                null,
                null,
                {
                    order: "0",
                    selfieOrder: "0",
                    total: "0"
                },
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const ratingDriver = (req, res) => {
    let query = req.query;
    let rating = orderModel
                    .aggregate([
                        {
                            "$match": {
                                "riderId": new ObjectId(query.riderId),
                            }
                        },
                        {
                            $group: {
                                _id: "$riderId",
                                "avgRating": { 
                                    "$avg": "$rating" 
                                }
                            }
                        }
                    ]).exec();
    rating
        .then((response) => {
            console.log('response==>', response);
            let msg = resMessage(
                1,
                res.statusCode,
                'Rating driver',
                null,
                null,
                null,
                (response.length > 0 && response[0].avgRating != undefined) ? Math.ceil(response[0].avgRating) : "0",
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Rating driver',
                null,
                null,
                null,
                0,
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const lupaPassword = (req, res) => {
    let find = driverModel.find({email: req.body.email}).exec();
    find
        .then((response) => {
            const generatePassword = new generate();
            generatePassword.setRandomPassword();
            const newPassword = generatePassword.getRandomPassword();
            return [response, newPassword];
        })
        .spread((response, newPassword) => {
            const encryptPassword = new auth("", newPassword);
            driverModel.update({_id: response[0]._id},{password: encryptPassword.createHash()}, {}).exec();
            process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
            let transporter = nodemailer.createTransport(config.smtpConfig);
            let mailOptions = {
                            from: '"Nguberjek Technical Support " <nguberjek@gmail.com>', // sender address
                            to: response[0].email, // list of receivers
                            subject: 'Driver Reset Password', // Subject line
                            text: 'Driver Reset Password', // plaintext body
                            html: '<html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <meta name="viewport" content="width=device-width"> <title>Title</title> <style>.wrapper{width: 100%;}#outlook a{padding: 0;}body{width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; Margin: 0; padding: 0; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;}.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}#backgroundTable{margin: 0; Margin: 0; padding: 0; width: 100% !important; line-height: 100% !important;}img{outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; clear: both; display: block;}center{width: 100%; min-width: 580px;}a img{border: none;}p{margin: 0 0 0 10px; Margin: 0 0 0 10px;}table{border-spacing: 0; border-collapse: collapse;}td{word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important;}table, tr, td{padding: 0; vertical-align: top; text-align: left;}@media only screen{html{min-height: 100%; background: #f3f3f3;}}table.body{background: #f3f3f3; height: 100%; width: 100%;}table.container{background: #fefefe; width: 580px; margin: 0 auto; Margin: 0 auto; text-align: inherit;}table.row{padding: 0; width: 100%; position: relative;}table.spacer{width: 100%;}table.spacer td{mso-line-height-rule: exactly;}table.container table.row{display: table;}td.columns,td.column,th.columns,th.column{margin: 0 auto; Margin: 0 auto; padding-left: 16px; padding-bottom: 16px;}td.columns .column, td.columns .columns, td.column .column, td.column .columns, th.columns .column, th.columns .columns, th.column .column, th.column .columns{padding-left: 0 !important; padding-right: 0 !important;}td.columns .column center, td.columns .columns center, td.column .column center, td.column .columns center, th.columns .column center, th.columns .columns center, th.column .column center, th.column .columns center{min-width: none !important;}td.columns.last,td.column.last,th.columns.last,th.column.last{padding-right: 16px;}td.columns table:not(.button),td.column table:not(.button),th.columns table:not(.button),th.column table:not(.button){width: 100%;}td.large-1,th.large-1{width: 32.33333px; padding-left: 8px; padding-right: 8px;}td.large-1.first,th.large-1.first{padding-left: 16px;}td.large-1.last,th.large-1.last{padding-right: 16px;}.collapse > tbody > tr > td.large-1,.collapse > tbody > tr > th.large-1{padding-right: 0; padding-left: 0; width: 48.33333px;}.collapse td.large-1.first,.collapse th.large-1.first,.collapse td.large-1.last,.collapse th.large-1.last{width: 56.33333px;}td.large-1 center,th.large-1 center{min-width: 0.33333px;}.body .columns td.large-1,.body .column td.large-1,.body .columns th.large-1,.body .column th.large-1{width: 8.33333%;}td.large-2,th.large-2{width: 80.66667px; padding-left: 8px; padding-right: 8px;}td.large-2.first,th.large-2.first{padding-left: 16px;}td.large-2.last,th.large-2.last{padding-right: 16px;}.collapse > tbody > tr > td.large-2,.collapse > tbody > tr > th.large-2{padding-right: 0; padding-left: 0; width: 96.66667px;}.collapse td.large-2.first,.collapse th.large-2.first,.collapse td.large-2.last,.collapse th.large-2.last{width: 104.66667px;}td.large-2 center,th.large-2 center{min-width: 48.66667px;}.body .columns td.large-2,.body .column td.large-2,.body .columns th.large-2,.body .column th.large-2{width: 16.66667%;}td.large-3,th.large-3{width: 129px; padding-left: 8px; padding-right: 8px;}td.large-3.first,th.large-3.first{padding-left: 16px;}td.large-3.last,th.large-3.last{padding-right: 16px;}.collapse > tbody > tr > td.large-3,.collapse > tbody > tr > th.large-3{padding-right: 0; padding-left: 0; width: 145px;}.collapse td.large-3.first,.collapse th.large-3.first,.collapse td.large-3.last,.collapse th.large-3.last{width: 153px;}td.large-3 center,th.large-3 center{min-width: 97px;}.body .columns td.large-3,.body .column td.large-3,.body .columns th.large-3,.body .column th.large-3{width: 25%;}td.large-4,th.large-4{width: 177.33333px; padding-left: 8px; padding-right: 8px;}td.large-4.first,th.large-4.first{padding-left: 16px;}td.large-4.last,th.large-4.last{padding-right: 16px;}.collapse > tbody > tr > td.large-4,.collapse > tbody > tr > th.large-4{padding-right: 0; padding-left: 0; width: 193.33333px;}.collapse td.large-4.first,.collapse th.large-4.first,.collapse td.large-4.last,.collapse th.large-4.last{width: 201.33333px;}td.large-4 center,th.large-4 center{min-width: 145.33333px;}.body .columns td.large-4,.body .column td.large-4,.body .columns th.large-4,.body .column th.large-4{width: 33.33333%;}td.large-5,th.large-5{width: 225.66667px; padding-left: 8px; padding-right: 8px;}td.large-5.first,th.large-5.first{padding-left: 16px;}td.large-5.last,th.large-5.last{padding-right: 16px;}.collapse > tbody > tr > td.large-5,.collapse > tbody > tr > th.large-5{padding-right: 0; padding-left: 0; width: 241.66667px;}.collapse td.large-5.first,.collapse th.large-5.first,.collapse td.large-5.last,.collapse th.large-5.last{width: 249.66667px;}td.large-5 center,th.large-5 center{min-width: 193.66667px;}.body .columns td.large-5,.body .column td.large-5,.body .columns th.large-5,.body .column th.large-5{width: 41.66667%;}td.large-6,th.large-6{width: 274px; padding-left: 8px; padding-right: 8px;}td.large-6.first,th.large-6.first{padding-left: 16px;}td.large-6.last,th.large-6.last{padding-right: 16px;}.collapse > tbody > tr > td.large-6,.collapse > tbody > tr > th.large-6{padding-right: 0; padding-left: 0; width: 290px;}.collapse td.large-6.first,.collapse th.large-6.first,.collapse td.large-6.last,.collapse th.large-6.last{width: 298px;}td.large-6 center,th.large-6 center{min-width: 242px;}.body .columns td.large-6,.body .column td.large-6,.body .columns th.large-6,.body .column th.large-6{width: 50%;}td.large-7,th.large-7{width: 322.33333px; padding-left: 8px; padding-right: 8px;}td.large-7.first,th.large-7.first{padding-left: 16px;}td.large-7.last,th.large-7.last{padding-right: 16px;}.collapse > tbody > tr > td.large-7,.collapse > tbody > tr > th.large-7{padding-right: 0; padding-left: 0; width: 338.33333px;}.collapse td.large-7.first,.collapse th.large-7.first,.collapse td.large-7.last,.collapse th.large-7.last{width: 346.33333px;}td.large-7 center,th.large-7 center{min-width: 290.33333px;}.body .columns td.large-7,.body .column td.large-7,.body .columns th.large-7,.body .column th.large-7{width: 58.33333%;}td.large-8,th.large-8{width: 370.66667px; padding-left: 8px; padding-right: 8px;}td.large-8.first,th.large-8.first{padding-left: 16px;}td.large-8.last,th.large-8.last{padding-right: 16px;}.collapse > tbody > tr > td.large-8,.collapse > tbody > tr > th.large-8{padding-right: 0; padding-left: 0; width: 386.66667px;}.collapse td.large-8.first,.collapse th.large-8.first,.collapse td.large-8.last,.collapse th.large-8.last{width: 394.66667px;}td.large-8 center,th.large-8 center{min-width: 338.66667px;}.body .columns td.large-8,.body .column td.large-8,.body .columns th.large-8,.body .column th.large-8{width: 66.66667%;}td.large-9,th.large-9{width: 419px; padding-left: 8px; padding-right: 8px;}td.large-9.first,th.large-9.first{padding-left: 16px;}td.large-9.last,th.large-9.last{padding-right: 16px;}.collapse > tbody > tr > td.large-9,.collapse > tbody > tr > th.large-9{padding-right: 0; padding-left: 0; width: 435px;}.collapse td.large-9.first,.collapse th.large-9.first,.collapse td.large-9.last,.collapse th.large-9.last{width: 443px;}td.large-9 center,th.large-9 center{min-width: 387px;}.body .columns td.large-9,.body .column td.large-9,.body .columns th.large-9,.body .column th.large-9{width: 75%;}td.large-10,th.large-10{width: 467.33333px; padding-left: 8px; padding-right: 8px;}td.large-10.first,th.large-10.first{padding-left: 16px;}td.large-10.last,th.large-10.last{padding-right: 16px;}.collapse > tbody > tr > td.large-10,.collapse > tbody > tr > th.large-10{padding-right: 0; padding-left: 0; width: 483.33333px;}.collapse td.large-10.first,.collapse th.large-10.first,.collapse td.large-10.last,.collapse th.large-10.last{width: 491.33333px;}td.large-10 center,th.large-10 center{min-width: 435.33333px;}.body .columns td.large-10,.body .column td.large-10,.body .columns th.large-10,.body .column th.large-10{width: 83.33333%;}td.large-11,th.large-11{width: 515.66667px; padding-left: 8px; padding-right: 8px;}td.large-11.first,th.large-11.first{padding-left: 16px;}td.large-11.last,th.large-11.last{padding-right: 16px;}.collapse > tbody > tr > td.large-11,.collapse > tbody > tr > th.large-11{padding-right: 0; padding-left: 0; width: 531.66667px;}.collapse td.large-11.first,.collapse th.large-11.first,.collapse td.large-11.last,.collapse th.large-11.last{width: 539.66667px;}td.large-11 center,th.large-11 center{min-width: 483.66667px;}.body .columns td.large-11,.body .column td.large-11,.body .columns th.large-11,.body .column th.large-11{width: 91.66667%;}td.large-12,th.large-12{width: 564px; padding-left: 8px; padding-right: 8px;}td.large-12.first,th.large-12.first{padding-left: 16px;}td.large-12.last,th.large-12.last{padding-right: 16px;}.collapse > tbody > tr > td.large-12,.collapse > tbody > tr > th.large-12{padding-right: 0; padding-left: 0; width: 580px;}.collapse td.large-12.first,.collapse th.large-12.first,.collapse td.large-12.last,.collapse th.large-12.last{width: 588px;}td.large-12 center,th.large-12 center{min-width: 532px;}.body .columns td.large-12,.body .column td.large-12,.body .columns th.large-12,.body .column th.large-12{width: 100%;}td.large-offset-1,td.large-offset-1.first,td.large-offset-1.last,th.large-offset-1,th.large-offset-1.first,th.large-offset-1.last{padding-left: 64.33333px;}td.large-offset-2,td.large-offset-2.first,td.large-offset-2.last,th.large-offset-2,th.large-offset-2.first,th.large-offset-2.last{padding-left: 112.66667px;}td.large-offset-3,td.large-offset-3.first,td.large-offset-3.last,th.large-offset-3,th.large-offset-3.first,th.large-offset-3.last{padding-left: 161px;}td.large-offset-4,td.large-offset-4.first,td.large-offset-4.last,th.large-offset-4,th.large-offset-4.first,th.large-offset-4.last{padding-left: 209.33333px;}td.large-offset-5,td.large-offset-5.first,td.large-offset-5.last,th.large-offset-5,th.large-offset-5.first,th.large-offset-5.last{padding-left: 257.66667px;}td.large-offset-6,td.large-offset-6.first,td.large-offset-6.last,th.large-offset-6,th.large-offset-6.first,th.large-offset-6.last{padding-left: 306px;}td.large-offset-7,td.large-offset-7.first,td.large-offset-7.last,th.large-offset-7,th.large-offset-7.first,th.large-offset-7.last{padding-left: 354.33333px;}td.large-offset-8,td.large-offset-8.first,td.large-offset-8.last,th.large-offset-8,th.large-offset-8.first,th.large-offset-8.last{padding-left: 402.66667px;}td.large-offset-9,td.large-offset-9.first,td.large-offset-9.last,th.large-offset-9,th.large-offset-9.first,th.large-offset-9.last{padding-left: 451px;}td.large-offset-10,td.large-offset-10.first,td.large-offset-10.last,th.large-offset-10,th.large-offset-10.first,th.large-offset-10.last{padding-left: 499.33333px;}td.large-offset-11,td.large-offset-11.first,td.large-offset-11.last,th.large-offset-11,th.large-offset-11.first,th.large-offset-11.last{padding-left: 547.66667px;}td.expander,th.expander{visibility: hidden; width: 0; padding: 0 !important;}table.container.radius{border-radius: 0; border-collapse: separate;}.block-grid{width: 100%; max-width: 580px;}.block-grid td{display: inline-block; padding: 8px;}.up-2 td{width: 274px !important;}.up-3 td{width: 177px !important;}.up-4 td{width: 129px !important;}.up-5 td{width: 100px !important;}.up-6 td{width: 80px !important;}.up-7 td{width: 66px !important;}.up-8 td{width: 56px !important;}table.text-center,th.text-center,td.text-center,h1.text-center,h2.text-center,h3.text-center,h4.text-center,h5.text-center,h6.text-center,p.text-center,span.text-center{text-align: center;}table.text-left,th.text-left,td.text-left,h1.text-left,h2.text-left,h3.text-left,h4.text-left,h5.text-left,h6.text-left,p.text-left,span.text-left{text-align: left;}table.text-right,th.text-right,td.text-right,h1.text-right,h2.text-right,h3.text-right,h4.text-right,h5.text-right,h6.text-right,p.text-right,span.text-right{text-align: right;}span.text-center{display: block; width: 100%; text-align: center;}@media only screen and (max-width: 596px){.small-float-center{margin: 0 auto !important; float: none !important; text-align: center !important;}.small-text-center{text-align: center !important;}.small-text-left{text-align: left !important;}.small-text-right{text-align: right !important;}}img.float-left{float: left; text-align: left;}img.float-right{float: right; text-align: right;}img.float-center,img.text-center{margin: 0 auto; Margin: 0 auto; float: none; text-align: center;}table.float-center,td.float-center,th.float-center{margin: 0 auto; Margin: 0 auto; float: none; text-align: center;}.hide-for-large{display: none !important; mso-hide: all; overflow: hidden; max-height: 0; font-size: 0; width: 0; line-height: 0;}@media only screen and (max-width: 596px){.hide-for-large{display: block !important; width: auto !important; overflow: visible !important; max-height: none !important; font-size: inherit !important; line-height: inherit !important;}}table.body table.container .hide-for-large *{mso-hide: all;}@media only screen and (max-width: 596px){table.body table.container .hide-for-large, table.body table.container .row.hide-for-large{display: table !important; width: 100% !important;}}@media only screen and (max-width: 596px){table.body table.container .callout-inner.hide-for-large{display: table-cell !important; width: 100% !important;}}@media only screen and (max-width: 596px){table.body table.container .show-for-large{display: none !important; width: 0; mso-hide: all; overflow: hidden;}}body,table.body,h1,h2,h3,h4,h5,h6,p,td,th,a{color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; padding: 0; margin: 0; Margin: 0; text-align: left; line-height: 1.3;}h1,h2,h3,h4,h5,h6{color: inherit; word-wrap: normal; font-family: Helvetica, Arial, sans-serif; font-weight: normal; margin-bottom: 10px; Margin-bottom: 10px;}h1{font-size: 34px;}h2{font-size: 30px;}h3{font-size: 28px;}h4{font-size: 24px;}h5{font-size: 20px;}h6{font-size: 18px;}body,table.body,p,td,th{font-size: 16px; line-height: 1.3;}p{margin-bottom: 10px; Margin-bottom: 10px;}p.lead{font-size: 20px; line-height: 1.6;}p.subheader{margin-top: 4px; margin-bottom: 8px; Margin-top: 4px; Margin-bottom: 8px; font-weight: normal; line-height: 1.4; color: #8a8a8a;}small{font-size: 80%; color: #cacaca;}a{color: #2199e8; text-decoration: none;}a:hover{color: #147dc2;}a:active{color: #147dc2;}a:visited{color: #2199e8;}h1 a,h1 a:visited,h2 a,h2 a:visited,h3 a,h3 a:visited,h4 a,h4 a:visited,h5 a,h5 a:visited,h6 a,h6 a:visited{color: #2199e8;}pre{background: #f3f3f3; margin: 30px 0; Margin: 30px 0;}pre code{color: #cacaca;}pre code span.callout{color: #8a8a8a; font-weight: bold;}pre code span.callout-strong{color: #ff6908; font-weight: bold;}table.hr{width: 100%;}table.hr th{height: 0; max-width: 580px; border-top: 0; border-right: 0; border-bottom: 1px solid #0a0a0a; border-left: 0; margin: 20px auto; Margin: 20px auto; clear: both;}.stat{font-size: 40px; line-height: 1;}p + .stat{margin-top: -16px; Margin-top: -16px;}span.preheader{display: none !important; visibility: hidden; mso-hide: all !important; font-size: 1px; color: #f3f3f3; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;}table.button{width: auto; margin: 0 0 16px 0; Margin: 0 0 16px 0;}table.button table td{text-align: left; color: #fefefe; background: #2199e8; border: 2px solid #2199e8;}table.button table td a{font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; color: #fefefe; text-decoration: none; display: inline-block; padding: 8px 16px 8px 16px; border: 0 solid #2199e8; border-radius: 3px;}table.button.radius table td{border-radius: 3px; border: none;}table.button.rounded table td{border-radius: 500px; border: none;}table.button:hover table tr td a,table.button:active table tr td a,table.button table tr td a:visited,table.button.tiny:hover table tr td a,table.button.tiny:active table tr td a,table.button.tiny table tr td a:visited,table.button.small:hover table tr td a,table.button.small:active table tr td a,table.button.small table tr td a:visited,table.button.large:hover table tr td a,table.button.large:active table tr td a,table.button.large table tr td a:visited{color: #fefefe;}table.button.tiny table td,table.button.tiny table a{padding: 4px 8px 4px 8px;}table.button.tiny table a{font-size: 10px; font-weight: normal;}table.button.small table td,table.button.small table a{padding: 5px 10px 5px 10px; font-size: 12px;}table.button.large table a{padding: 10px 20px 10px 20px; font-size: 20px;}table.button.expand,table.button.expanded{width: 100% !important;}table.button.expand table, table.button.expanded table{width: 100%;}table.button.expand table a, table.button.expanded table a{text-align: center; width: 100%; padding-left: 0; padding-right: 0;}table.button.expand center, table.button.expanded center{min-width: 0;}table.button:hover table td,table.button:visited table td,table.button:active table td{background: #147dc2; color: #fefefe;}table.button:hover table a,table.button:visited table a,table.button:active table a{border: 0 solid #147dc2;}table.button.secondary table td{background: #777777; color: #fefefe; border: 0px solid #777777;}table.button.secondary table a{color: #fefefe; border: 0 solid #777777;}table.button.secondary:hover table td{background: #919191; color: #fefefe;}table.button.secondary:hover table a{border: 0 solid #919191;}table.button.secondary:hover table td a{color: #fefefe;}table.button.secondary:active table td a{color: #fefefe;}table.button.secondary table td a:visited{color: #fefefe;}table.button.success table td{background: #3adb76; border: 0px solid #3adb76;}table.button.success table a{border: 0 solid #3adb76;}table.button.success:hover table td{background: #23bf5d;}table.button.success:hover table a{border: 0 solid #23bf5d;}table.button.alert table td{background: #ec5840; border: 0px solid #ec5840;}table.button.alert table a{border: 0 solid #ec5840;}table.button.alert:hover table td{background: #e23317;}table.button.alert:hover table a{border: 0 solid #e23317;}table.button.warning table td{background: #ffae00; border: 0px solid #ffae00;}table.button.warning table a{border: 0px solid #ffae00;}table.button.warning:hover table td{background: #cc8b00;}table.button.warning:hover table a{border: 0px solid #cc8b00;}table.callout{margin-bottom: 16px; Margin-bottom: 16px;}th.callout-inner{width: 100%; border: 1px solid #cbcbcb; padding: 10px; background: #fefefe;}th.callout-inner.primary{background: #def0fc; border: 1px solid #444444; color: #0a0a0a;}th.callout-inner.secondary{background: #ebebeb; border: 1px solid #444444; color: #0a0a0a;}th.callout-inner.success{background: #e1faea; border: 1px solid #1b9448; color: #fefefe;}th.callout-inner.warning{background: #fff3d9; border: 1px solid #996800; color: #fefefe;}th.callout-inner.alert{background: #fce6e2; border: 1px solid #b42912; color: #fefefe;}.thumbnail{border: solid 4px #fefefe; box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2); display: inline-block; line-height: 0; max-width: 100%; transition: box-shadow 200ms ease-out; border-radius: 3px; margin-bottom: 16px;}.thumbnail:hover, .thumbnail:focus{box-shadow: 0 0 6px 1px rgba(33, 153, 232, 0.5);}table.menu{width: 580px;}table.menu td.menu-item, table.menu th.menu-item{padding: 10px; padding-right: 10px;}table.menu td.menu-item a, table.menu th.menu-item a{color: #2199e8;}table.menu.vertical td.menu-item,table.menu.vertical th.menu-item{padding: 10px; padding-right: 0; display: block;}table.menu.vertical td.menu-item a, table.menu.vertical th.menu-item a{width: 100%;}table.menu.vertical td.menu-item table.menu.vertical td.menu-item,table.menu.vertical td.menu-item table.menu.vertical th.menu-item,table.menu.vertical th.menu-item table.menu.vertical td.menu-item,table.menu.vertical th.menu-item table.menu.vertical th.menu-item{padding-left: 10px;}table.menu.text-center a{text-align: center;}.menu[align="center"]{width: auto !important;}body.outlook p{display: inline !important;}@media only screen and (max-width: 596px){table.body img{width: auto; height: auto;}table.body center{min-width: 0 !important;}table.body .container{width: 95% !important;}table.body .columns, table.body .column{height: auto !important; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; padding-left: 16px !important; padding-right: 16px !important;}table.body .columns .column, table.body .columns .columns, table.body .column .column, table.body .column .columns{padding-left: 0 !important; padding-right: 0 !important;}table.body .collapse .columns, table.body .collapse .column{padding-left: 0 !important; padding-right: 0 !important;}td.small-1, th.small-1{display: inline-block !important; width: 8.33333% !important;}td.small-2, th.small-2{display: inline-block !important; width: 16.66667% !important;}td.small-3, th.small-3{display: inline-block !important; width: 25% !important;}td.small-4, th.small-4{display: inline-block !important; width: 33.33333% !important;}td.small-5, th.small-5{display: inline-block !important; width: 41.66667% !important;}td.small-6, th.small-6{display: inline-block !important; width: 50% !important;}td.small-7, th.small-7{display: inline-block !important; width: 58.33333% !important;}td.small-8, th.small-8{display: inline-block !important; width: 66.66667% !important;}td.small-9, th.small-9{display: inline-block !important; width: 75% !important;}td.small-10, th.small-10{display: inline-block !important; width: 83.33333% !important;}td.small-11, th.small-11{display: inline-block !important; width: 91.66667% !important;}td.small-12, th.small-12{display: inline-block !important; width: 100% !important;}.columns td.small-12, .column td.small-12, .columns th.small-12, .column th.small-12{display: block !important; width: 100% !important;}table.body td.small-offset-1, table.body th.small-offset-1{margin-left: 8.33333% !important; Margin-left: 8.33333% !important;}table.body td.small-offset-2, table.body th.small-offset-2{margin-left: 16.66667% !important; Margin-left: 16.66667% !important;}table.body td.small-offset-3, table.body th.small-offset-3{margin-left: 25% !important; Margin-left: 25% !important;}table.body td.small-offset-4, table.body th.small-offset-4{margin-left: 33.33333% !important; Margin-left: 33.33333% !important;}table.body td.small-offset-5, table.body th.small-offset-5{margin-left: 41.66667% !important; Margin-left: 41.66667% !important;}table.body td.small-offset-6, table.body th.small-offset-6{margin-left: 50% !important; Margin-left: 50% !important;}table.body td.small-offset-7, table.body th.small-offset-7{margin-left: 58.33333% !important; Margin-left: 58.33333% !important;}table.body td.small-offset-8, table.body th.small-offset-8{margin-left: 66.66667% !important; Margin-left: 66.66667% !important;}table.body td.small-offset-9, table.body th.small-offset-9{margin-left: 75% !important; Margin-left: 75% !important;}table.body td.small-offset-10, table.body th.small-offset-10{margin-left: 83.33333% !important; Margin-left: 83.33333% !important;}table.body td.small-offset-11, table.body th.small-offset-11{margin-left: 91.66667% !important; Margin-left: 91.66667% !important;}table.body table.columns td.expander, table.body table.columns th.expander{display: none !important;}table.body .right-text-pad, table.body .text-pad-right{padding-left: 10px !important;}table.body .left-text-pad, table.body .text-pad-left{padding-right: 10px !important;}table.menu{width: 100% !important;}table.menu td, table.menu th{width: auto !important; display: inline-block !important;}table.menu.vertical td, table.menu.vertical th, table.menu.small-vertical td, table.menu.small-vertical th{display: block !important;}table.menu[align="center"]{width: auto !important;}table.button.small-expand, table.button.small-expanded{width: 100% !important;}table.button.small-expand table, table.button.small-expanded table{width: 100%;}table.button.small-expand table a, table.button.small-expanded table a{text-align: center !important; width: 100% !important; padding-left: 0 !important; padding-right: 0 !important;}table.button.small-expand center, table.button.small-expanded center{min-width: 0;}}</style> <style>body, html, .body{background: #f3f3f3 !important;}.container.header{background: #f3f3f3;}.body-drip{border-top: 8px solid #663399;}</style> </head> <body> <table class="body" data-made-with-foundation=""> <tr> <td class="float-center" align="center" valign="top"> <center data-parsed=""> <table class="spacer float-center"> <tbody> <tr> <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody> </table> <table align="center" class="container body-drip float-center"> <tbody> <tr> <td> <table class="spacer"> <tbody> <tr> <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody> </table> <center data-parsed=""> <img src="https://1.bp.blogspot.com/-mysPFlbkIJc/V6gBE_2bsEI/AAAAAAAAABY/Qb8MJF4yptcSQZ4US0HO1AliCLf031BMQCLcB/s1600/NGUBER%2BLOGO.png" alt="" align="center" class="float-center" width="220px"> </center> <table class="spacer"> <tbody> <tr> <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody> </table> <table class="row"> <tbody> <tr> <th class="small-12 large-12 columns first last"> <table> <tr> <th> <h4 class="text-center">Password Reset</h4> <p class="text-center"></p></th> <th class="expander"></th> </tr></table> </th> </tr></tbody> </table> <hr> <table class="row"> <tbody> <tr> <th class="small-12 large-12 columns first last"> <table> <tr> <th> <p class="text-center">Permintaan anda untuk mereset password telah kami terima. Dengan persetujuan anda kami mereset password pada aplikasi anda. Silakan login ke aplikasi dengan menggunakan password baru anda dibawah ini:</p><center data-parsed=""> <table class="button success float-center"> <tr> <td> <table> <tr> <td style="background: #3adb76; padding: 10px;">'+newPassword+'</td></tr></table> </td></tr></table> </center> </th> <th class="expander"></th> </tr></table> </th> </tr></tbody> </table> <table class="row collapsed footer"> <tbody> <tr> <th class="small-12 large-12 columns first last"> <table> <tr> <th> <table class="spacer"> <tbody> <tr> <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td></tr></tbody> </table> <p class="text-center">@copywrite nguberjek team<br><a href="#">nguberjek.com</a> | <a href="#">Technical Support</a> | <a href="#">Sales</a></p></th> <th class="expander"></th> </tr></table> </th> </tr></tbody> </table> </td></tr></tbody> </table> </center> </td></tr></table> </body></html>' // html body
                        };
            transporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    let msg = resMessage(
                        0,
                        res.statusCode,
                        "Ooops, something went wrong!!!",
                        null,
                        err.message,
                        null,
                        null,
                        err.errors,
                        null,
                        null
                    );
                    res.send(msg);
                }
                let msg = resMessage(
                    1,
                    res.statusCode,
                    "Password baru anda akan dikirimkan melalui email",
                    null,
                    info.response,
                    null,
                    null,
                    null,
                    null,
                    null
                );
                res.send(msg);
            });
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                "Ooops, something went wrong!!!",
                null,
                err.message,
                null,
                null,
                "Email tidak ditemukan",
                null,
                null
            );
            res.send(msg);
        });
}

export const saldoDriver = (req, res) => {
    let query = req.query;
    let findDriver = driverModel.findOne({_id: query.riderId}).exec();
    findDriver
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                "Sisa Saldo Driver",
                null,
                null,
                null,
                (response.saldo) ? response.saldo : "0",
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                1,
                res.statusCode,
                "Sisa Saldo Driver",
                null,
                null,
                null,
                "0",
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const lastOrder = (req, res) => {
    let query = req.query;
    let findLastOrder = orderModel.findOne({
        $and: [{
            riderId: query.riderId
        }, {
            $or: [{
                status: 4
            }, {
                status: 5
            }]
        }]
    }).sort({"tanggal": -1}).exec();
    findLastOrder
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Order Terakhir',
                null,
                null,
                null,
                (response) ? response : '-',
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Order Terakhir',
                null,
                null,
                null,
                '-',
                null,
                null,
                null
            );
            res.send(msg);
        });
}

export const lastSelfieOrder = (req, res) => {
    let query = req.query;
    let findLastSelfieOrder = selfieorderModel.findOne({
        $and: [{
            riderId: query.riderId
        }, {
            $or: [{
                status: 2
            }, {
                status: 3
            }]
        }]
    }).sort({tanggal: -1}).exec();
    findLastSelfieOrder
        .then((response) => {
            let msg = resMessage(
                1,
                res.statusCode,
                'Last Selfie Order',
                null,
                null,
                null,
                (response) ? response : '-',
                null,
                null,
                null
            );
            res.send(msg);
        })
        .catch((err) => {
            let msg = resMessage(
                0,
                res.statusCode,
                'Last Selfie Order',
                null,
                null,
                null,
                '-',
                null,
                null,
                null
            );
            res.send(msg);
        });
}

function isNguberjek(val) {
    return val.role === "nguberjek";
}

function countNguberjek(val) {
    if(val.role === "nguberjek") {
        return val.count;
    }
    return 0;
}

function isNgubercar(val) {
    return val.role === "ngubercar";
}

function countNgubercar(val) {
    if(val.role === "ngubercar") {
        return val.count;
    }
    return 0;
}

export const driverByLocation = (req, res) => {
    let query = driverModel.aggregate([
        {
            $match: {
                senderId: {$ne: ""},
                koordinatTerakhir: {$ne: []}
            }
        },
        {
            $group: {
                _id: {
                    role : '$role',
                    lokasi: '$lokasi'
                },
                countRole: {$sum: 1}
            }
        },
        {
            $group: {
                _id: "$_id.lokasi",
                role: { 
                    "$push": { 
                        "role": "$_id.role",
                        "count": "$countRole"
                    },
                },
                "count": { "$sum": "$countRole" }
            }
        },{ 
            "$sort": { 
                "count": -1
            } 
        }
    ]).exec();

    query.then((response) => {
        let arr = [];
        let cnt = 0;
        for(let i=0; i<response.length; i++) {
            cnt++;
            let dt = {};
            let rl = [];
            dt.lokasi = response[i]._id;
            if(response[i].role.find(isNguberjek) !== undefined) {
                rl.push({
                    nama: "nguberjek",
                    total: response[i].role.find(countNguberjek).count
                })
            } else {
                rl.push({
                    nama: "nguberjek",
                    total: 0
                })
            }

            if(response[i].role.find(isNgubercar) !== undefined) {
                rl.push({
                    nama: "ngubercar",
                    total: response[i].role.find(countNgubercar).count
                })
            } else {
                rl.push({
                    nama: "ngubercar",
                    total: 0
                })
            }

            dt.role = rl;
            arr.push(dt);
            if(cnt === response.length) {
                let msg = resMessage(
                    1,
                    res.statusCode,
                    'Driver By Location',
                    null,
                    null,
                    null,
                    (arr) ? arr : '-',
                    null,
                    null,
                    null
                );
                res.send(msg)
                // res.send(arr);
            }
        }
    }).catch((error) => {
        let msg = resMessage(
            0,
            res.statusCode,
            'Driver By Location',
            null,
            null,
            null,
            '-',
            null,
            null,
            null
        );
        res.send(msg)
    })
}