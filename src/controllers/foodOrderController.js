/**
 * @author: Artha Prihardana
 * @Date: 2017-10-21 15:49:37
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-10-27 00:01:00
 */
import config from "../app.conf";
import request from "request";
import mongoose from "mongoose";
import moment from "moment";
require("moment/locale/id");
import foodOrderModel from "../models/foodOrderModel";
import driverModel from "../models/driverModel";
import boncengerModel from "../models/boncengerModel";
import objectHelper from "../helpers/objectHelper";
import generate from "../helpers/generateHelper";
import resMessage from "../helpers/resHelper";
import { sendNotification } from "../helpers/firebaseHelper";

const ObjectId = mongoose.Types.ObjectId;

class OrderController {
  constructor(data) {
    this.data = data;
  }

  getDetailHistori(cb) {
    let count = 0;
    let data = [];
    this.data.forEach((val, key, array) => {
      count++;
      let dt = {
        _id: val._id,
        orderId: val.orderId,
        kategori: val.kategori,
        tanggal: val.tanggal,
        lokasiAwal: val.lokasiAwal.lokasi,
        latitudeLA: val.lokasiAwal.koordinatLokasi[1],
        longitudeLA: val.lokasiAwal.koordinatLokasi[0],
        lokasiTujuan: val.lokasiAkhir.tujuan,
        latitudeLT: val.lokasiAkhir.koordinatTujuan[1],
        longitudeLT: val.lokasiAkhir.koordinatTujuan[0],
        jarak: val.jarak,
        waktu: val.waktu,
        harga: val.harga,
        status: val.status
      };
      if (val.riderId && val.riderId != undefined) {
        dt["driver"] = {
          _id: val.riderId._id,
          nama: val.riderId.nama,
          noTelp: val.riderId.noTelp,
          email: val.riderId.email,
          photo: val.riderId.photo,
          jenisKelamin: val.riderId.jenisKelamin,
          typeKendaraan: val.riderId.typeKendaraan,
          merkKendaraan: val.riderId.merkKendaraan,
          noPolisi: val.riderId.noPolisi,
          lokasi: val.riderId.lokasi,
          role: val.riderId.role,
          rating: 0
        };
      }
      if (val.boncengerId && val.boncengerId != undefined) {
        dt["boncenger"] = {
          _id: val.boncengerId._id,
          nama: val.boncengerId.nama,
          noTelp: val.boncengerId.noTelp,
          email: val.boncengerId.email,
          jenisKelamin: val.boncengerId.jenisKelamin,
          photo: val.boncengerId.photo
        };
      }
      if (val.riderId && val.riderId != undefined) {
        request(
          config.apiUrl +
            ":" +
            config.apiPort +
            "/api/v3/ns/ratingDriver?riderId=" +
            val.riderId._id,
          (err, res, body) => {
            let bd = JSON.parse(body);
            dt.driver.rating = bd.dt != undefined ? bd.dt : 0;
            console.log("rating==>", bd.dt);
            // console.log('type', dt.driver.rating);
            data.push(dt);

            if (array.length == count) {
              cb(data);
            }
          }
        );
      } else {
        data.push(dt);
        if (array.length == count) {
          cb(data);
        }
      }
      // data.push(dt);

      // if(array.length == count) {
      //     cb(data);
      // }
    });
  }

  getRadiusDriver(header, body, cb) {
    let options = {
      method: "GET",
      url:
        config.apiUrl +
        ":" +
        config.apiPort +
        config.apiVersion +
        "/ws/radius/driver",
      qs: {
        koordinat: body.koordinatResto,
        role: "nguberjek"
      },
      headers: {
        "cache-control": "no-cache",
        "x-access-token": header["x-access-token"]
      }
    };
    request(options, (error, response, body) => {
      try {
        if (error) {
          // throw new Error(error);
          console.error(error);
          cb({
            s: 0,
            message: "Ooop Something Went Wrong!!!"
          });
        }
        cb(JSON.parse(body));
      } catch (e) {
        // console.error('e==>', e);
        cb({
          s: 0,
          message: "Ooop Something Went Wrong!!!"
        });
      }
    });
  }

  getDetailOrder(cb) {
    // console.log("====================================");
    // console.log("this data ==>", this.data);
    // console.log("====================================");
    let dt = {
      _id: this.data._id,
      updated_at: moment(this.data.updated_at).format("LLL"),
      created_at: moment(this.data.created_at).format("LLL"),
      status: this.data.status,
      komentar: this.data.komentar || "",
      rating: this.data.rating || "",
      boncenger: {
        _id: this.data.boncengerId._id,
        senderId: this.data.boncengerId.senderId,
        noTelp: this.data.boncengerId.noTelp,
        email: this.data.boncengerId.email,
        nama: this.data.boncengerId.name,
        photo: this.data.boncengerId.photo
      },
      totalHarga: this.data.totalHarga,
      biayaAntar: this.data.biayaAntar,
      perkiraanHarga: this.data.perkiraanHarga,
      waktu: this.data.waktu,
      jarak: this.data.jarak,
      alamatPengantaran: this.data.alamatPengantaran,
      koordinatPengantaran: {
        latitude: this.data.koordinatPengantaran[1],
        longitude: this.data.koordinatPengantaran[0]
      },
      resto: {
        _id: this.data.namaResto._id,
        foto: this.data.namaResto.foto,
        jamBuka: this.data.namaResto.jamBuka,
        koordinat: {
          latitude: this.data.namaResto.koordinat[1],
          longitude: this.data.namaResto.koordinat[0]
        },
        alamat: this.data.namaResto.alamat,
        namaResto: this.data.namaResto.namaResto
      },
      tanggal: moment(this.data.tanggal).format("LLL"),
      orderId: this.data.orderId,
      rider: {
        _id: this.data.riderId._id,
        senderId: this.data.riderId.senderId,
        username: this.data.riderId.username,
        lokasi: this.data.riderId.lokasi,
        noPolisi: this.data.riderId.noPolisi,
        merkKendaraan: this.data.riderId.merkKendaraan,
        typeKendaraan: this.data.riderId.typeKendaraan,
        jenisKelamin: this.data.riderId.jenisKelamin,
        noTelp: this.data.riderId.noTelp,
        email: this.data.riderId.email,
        nama: this.data.riderId.nama,
        photo: this.data.riderId.photo,
        role: this.data.riderId.role
      }
    };
    console.log("==>", this.data.namaMenu.length);
    if (this.data.namaMenu && this.data.namaMenu.length > 0) {
      let menu = this.data.namaMenu;
      let arr = [];
      for (let i = 0; i < menu.length; i++) {
        console.log("menu[i]==>", menu[i]);
        arr.push({
          namaMenu: menu[i].menuId.namaMenu,
          deskripsi: menu[i].menuId.deskripsi,
          harga: menu[i].menuId.harga,
          jmlPorsi: menu[i].jmlPorsi,
          subTotal: menu[i].subTotal,
          catatan: menu[i].catatan,
          _id: menu[i]._id
        });
      }
      dt.menu = arr;
    }

    cb(dt);
  }

  getDetailHistoryOrder(cb) {
    let count = 0;
    let data = [];
    this.data.forEach((val, key, array) => {
      count++;
      let dt = {
        _id: val._id,
        orderId: val.orderId,
        kategori: val.kategori,
        tanggal: val.tanggal,
        // lokasiAwal: val.lokasiAwal.lokasi,
        // latitudeLA: val.lokasiAwal.koordinatLokasi[1],
        // longitudeLA: val.lokasiAwal.koordinatLokasi[0],
        // lokasiTujuan: val.lokasiAkhir.tujuan,
        // latitudeLT: val.lokasiAkhir.koordinatTujuan[1],
        // longitudeLT: val.lokasiAkhir.koordinatTujuan[0],
        jarak: val.jarak,
        waktu: val.waktu,
        harga: val.harga,
        status: val.status,
        komentar: val.komentar,
        rating: val.rating,
        totalHarga: val.totalHarga,
        biayaAntar: val.biayaAntar,
        perkiraanHarga: val.perkiraanHarga,
        waktu: val.waktu,
        jarak: val.jarak,
        alamatPengantaran: val.alamatPengantaran,
        koordinatPengantaran: val.koordinatPengantaran,
        namaResto: val.namaResto,
        tanggal: val.tanggal,
        show: val.show
      };
      if (val.riderId && val.riderId != undefined) {
        dt["riderId"] = {
          _id: val.riderId._id,
          nama: val.riderId.nama,
          noTelp: val.riderId.noTelp,
          email: val.riderId.email,
          photo: val.riderId.photo,
          jenisKelamin: val.riderId.jenisKelamin,
          typeKendaraan: val.riderId.typeKendaraan,
          merkKendaraan: val.riderId.merkKendaraan,
          noPolisi: val.riderId.noPolisi,
          lokasi: val.riderId.lokasi,
          role: val.riderId.role
        };
      }
      if (val.boncengerId && val.boncengerId != undefined) {
        dt["boncengerId"] = {
          _id: val.boncengerId._id,
          nama: val.boncengerId.nama,
          noTelp: val.boncengerId.noTelp,
          email: val.boncengerId.email,
          jenisKelamin: val.boncengerId.jenisKelamin,
          photo: val.boncengerId.photo
        };
      }
      data.push(dt);
      if (array.length == count) {
        cb(data);
      }
    });
  }
}

export const requestFoodOrder = (req, res, next) => {
  let header = req.headers;
  let body = req.body;
  let gen = new generate();
  gen.setOrderId();

  let order = new OrderController();
  order.getRadiusDriver(header, body, response => {
    // console.log('response radius ==>', response)
    let orderId = gen.getOrderId();
    // console.log('order id ==>', orderId)
    let saveOrder = new foodOrderModel();
    saveOrder.orderId = orderId;
    saveOrder.tanggal = new Date();
    saveOrder.namaResto = body.namaResto ? body.namaResto : "";
    saveOrder.koordinatPengantaran = body.koordinatPengantaran
      ? body.koordinatPengantaran.split(",").map(Number)
      : "";
    saveOrder.alamatPengantaran = body.alamatPengantaran
      ? body.alamatPengantaran
      : "";
    saveOrder.jarak = body.jarak ? body.jarak : "";
    saveOrder.waktu = body.waktu ? body.waktu : "";
    saveOrder.perkiraanHarga = body.perkiraanHarga ? body.perkiraanHarga : "";
    saveOrder.biayaAntar = body.biayaAntar ? body.biayaAntar : "";
    saveOrder.totalHarga = body.totalHarga ? body.totalHarga : "";
    saveOrder.boncengerId = body.boncengerId ? body.boncengerId : "";
    saveOrder.rating = body.rating ? body.rating : "";
    saveOrder.komentar = body.komentar ? body.komentar : "";
    saveOrder.status = 0;

    if (body.namaMenu && body.namaMenu.length > 0) {
      let arr = [];
      let menu = body.namaMenu;
      for (let i in menu) {
        arr.push({
          menuId: menu[i].menuId,
          harga: menu[i].harga,
          jmlPorsi: menu[i].jmlPorsi,
          subTotal: menu[i].subTotal,
          catatan: menu[i].catatan
        });
      }
      saveOrder.namaMenu = arr;
    }
    console.log("body pesanan menu ==>", body);
    let driverList = response.dt;
    const simpan = saveOrder.save();
    simpan
      .then(responseSimpan => {
        // console.log('response simpan ==> ', responseSimpan)
        res.send({
          dataOrder: responseSimpan,
          driverList: driverList
        });
      })
      .catch(err => {
        // console.log('error simpan ==>', err)
        res.send({
          s: 0,
          statusCode: res.statusCode,
          err: err,
          dataOrder: {},
          driverList: []
        });
      });
  });
};

export const acceptRequest = (req, res, next) => {
  let body = req.body;
  let accept = foodOrderModel
    .update(
      { orderId: body.orderId },
      { riderId: body.driverId, status: 1 },
      {}
    )
    .exec();
  accept
    .then(response => {
      let driverStatus = driverModel
        .update({ _id: body.driverId }, { ready: false }, {})
        .exec();
      let get = foodOrderModel
        .findOne({ orderId: body.orderId })
        .populate("riderId")
        .populate("boncengerId")
        .populate("namaResto")
        .populate("namaMenu.menuId")
        .exec();
      return [response, get];
    })
    .spread((response, get) => {
      let order = new OrderController(get);
      order.getDetailOrder(data => {
        let msg = resMessage(
          1,
          res.statusCode,
          "Detail Food Order",
          null,
          response,
          null,
          data,
          null,
          null,
          null
        );
        res.send(msg);
      });
      // res.send(get)
    })
    .catch(err => {
      let msg = resMessage(
        0,
        res.statusCode,
        err.message,
        null,
        null,
        null,
        null,
        err.errors,
        null,
        null
      );
      res.send(msg);
    });
};

export const updateOrder = (req, res) => {
  let body = req.body;
  if (body && body.orderId && body.status /*&& body.riderId*/) {
    let findOrder;
    if (body.riderId) {
      findOrder = foodOrderModel
        .findOne({ orderId: body.orderId })
        .populate("riderId")
        .populate("boncengerId")
        .populate("namaMenu.menuId")
        .exec();
    } else {
      findOrder = foodOrderModel
        .findOne({ orderId: body.orderId })
        .populate("boncengerId")
        .exec();
    }
    findOrder
      .then(response => {
        foodOrderModel
          .update(
            {
              orderId: body.orderId
            },
            {
              riderId: body.riderId,
              status: body.status,
              komentar:
                body.komentar != "" || body.komentar != undefined
                  ? body.komentar
                  : "",
              rating:
                body.rating != "" || body.rating != undefined ? body.rating : 0
            }
          )
          .exec();
        switch (body.status) {
          case "2":
            sendNotification(
              {
                title: "Order Status",
                status: 2,
                message: "Driver sedang menuju ke resto",
                orderId: "food_"+response.orderId
                // expired: 20000
              },
              response.boncengerId.senderId,
              gcmresponse => {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Driver sedang menuju ke resto",
                  null,
                  gcmresponse,
                  null,
                  [response],
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            );
            break;
          case "3":
            sendNotification(
              {
                title: "Order Status",
                status: 3,
                message: "Driver sedang mengantar pesanan anda",
                orderId: "food_"+response.orderId
                // expired: 20000
              },
              [response.boncengerId.senderId, response.riderId.senderId],
              gcmresponse => {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Driver sedang mengantar pesanan anda",
                  null,
                  gcmresponse,
                  null,
                  [response],
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            );
            break;
          case "4":
            sendNotification(
              {
                title: "Order Status",
                status: 4,
                message:
                  "Order Nguberfood telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
                orderId: "food_"+response.orderId
                // expired: 20000
              },
              [response.boncengerId.senderId, response.riderId.senderId],
              gcmresponse => {
                driverModel.findOne({ _id: body.riderId }).exec((err, data) => {
                  let saldo = data.saldo;
                  let ps = parseInt(saldo) - parseInt(response.biayaAntar) * 0.2;
                  driverModel
                    .update(
                      { _id: body.riderId },
                      { ready: true, saldo: ps },
                      {}
                    )
                    .exec();
                  let detailorder = new OrderController(response);
                  // detailorder.getDetailOrder(data => {
                  //   let msg = resMessage(
                  //     1,
                  //     res.statusCode,
                  //     "Order Nguberfood telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
                  //     null,
                  //     gcmresponse,
                  //     null,
                  //     data,
                  //     null,
                  //     null,
                  //     null
                  //   );
                  //   res.send(msg);
                  // });
                    let msg = resMessage(
                      1,
                      res.statusCode,
                      "Order Nguberfood telah selesai. Terima kasih telah menggunakan layanan NGUBERJEK. :)",
                      null,
                      gcmresponse,
                      null,
                      [response],
                      null,
                      null,
                      null
                    );
                    res.send(msg);
                });
              }
            );
            break;
          case "5":
            sendNotification(
              {
                title: "Order Status",
                status: 5,
                message: "Terjadi masalah dalam pengantaran makanan",
                orderId: "food_"+response.orderId
                // expired: 20000
              },
              [response.riderId.senderId],
              gcmresponse => {
                driverModel
                  .update({ _id: body.riderId }, { ready: true }, {})
                  .exec();
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Terjadi masalah dalam pengantaran makanan",
                  null,
                  null,
                  null,
                  [response],
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            );
            break;
          case "6":
            sendNotification(
              {
                title: "Order Status",
                status: 6,
                message: "Order telah dibatalkan oleh penumpang",
                orderId: "food_"+response.orderId
                // expired: 20000
              },
              [response.riderId.senderId],
              gcmresponse => {
                driverModel
                  .update({ _id: body.riderId }, { ready: true }, {})
                  .exec();
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Order telah dibatalkan oleh penumpang",
                  null,
                  null,
                  null,
                  [response],
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            );
            break;
          case "7":
            sendNotification(
              {
                title: "Order Status",
                status: 7,
                message: "Order telah dibatalkan oleh rider",
                orderId: response.orderId
                // expired: 20000
              },
              [response.boncengerId.senderId],
              gcmresponse => {
                driverModel
                  .update({ _id: body.riderId }, { ready: true }, {})
                  .exec();
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Order telah dibatalkan oleh rider",
                  null,
                  null,
                  null,
                  {},
                  null,
                  null,
                  null
                );
                res.send(msg);
              }
            );
          default:
            let msg = resMessage(
              1,
              res.statusCode,
              "Driver menerima pesanan Anda",
              null,
              null,
              null,
              [response],
              null,
              null,
              null
            );
            res.send(msg);
            break;
        }
      })
      .catch(err => {
        let msg = resMessage(
          0,
          res.statusCode,
          err.message,
          null,
          null,
          null,
          null,
          err.errors,
          null,
          null
        );
        res.send(msg);
      });
  } else {
    let msg = resMessage(
      0,
      res.statusCode,
      "parameter orderId, riderId dan status harus diisi",
      null,
      null,
      null,
      null,
      {},
      null,
      null
    );
    res.send(msg);
  }
};

export const getOrderFood = (req, res, next) => {
  let objQry = new objectHelper(req.query, ["limit", "page", "penumpang", "driver"]);
  let limitPerPage = parseInt(req.query.limit) || 25;
  let page = parseInt(req.query.page) || 1;
  let search = {
    $and: [
      { show: true },
      {
        $or: [
          // { namaResto: new RegExp(req.query.search, "i") },
          // { alamatPengantaran: new RegExp(req.query.search, "i") },
          { orderId: new RegExp(req.query.search, "i") }
        ]
      }
    ]
  };
  let query = objQry.objekFilter();
  let find = {};

  if (query.search) {
    find = search;
  } else if (query.all) {
    limitPerPage = 0;
  } else if (req.query.penumpang || req.query.driver) {
    limitPerPage = 0;
    find = {
      $and: [
        { show: true },
        // {
        //   $or: [{ status: 4 }, { status: 5 }, { status: 6 }, { status: 7 }]
        // },
        query || {}
      ]
    };
  } else {
    find = {
      show: true,
      $and: [
        // {
        //   $or: [{ status: 4 }, { status: 5 }, { status: 6 }, { status: 7 }]
        // },
        query || {}
      ]
    };
  }

  let getData = foodOrderModel
    .find(find)
    .populate({
      path: "riderId",
      match: {
        "nama": new RegExp(req.query.driver, "i")
      }
    })
    .populate({
      path: "boncengerId",
      match: {
        "nama": new RegExp(req.query.penumpang, "i")
      }
    })
    // .populate("boncengerId")
    .populate("namaResto")
    .populate("namaMenu.menuId")
    // .populate("riderId")
    .limit(limitPerPage)
    .skip(limitPerPage * (page - 1))
    .exec();
  getData
    .then(response => {
      let dataCount = foodOrderModel.count(find).exec();
      return [dataCount, response];
    })
    .spread((dataCount, response) => {
      if(req.query.penumpang && response.length > 0) {
        let arr = [];
        let cnt = 0;
        for(let i=0; i<response.length; i++) {
          cnt++;
          if(response[i].boncengerId != undefined && response[i].boncengerId != null) { 
            arr.push(response[i]);
          }
          if(cnt == response.length) {
            if(arr.length > 0) {
              let order = new OrderController(arr);
              order.getDetailHistoryOrder(data => {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Data histori order",
                  {
                    total: arr.length,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(arr.length / limitPerPage)
                  },
                  null,
                  null,
                  data,
                  null,
                  null,
                  null
                );
                res.send(msg);
              });
            } else {
              let msg = resMessage(
                1,
                res.statusCode,
                "Data histori order",
                {
                  total: arr.length,
                  limit: limitPerPage,
                  page: page,
                  pages: Math.ceil(arr.length / limitPerPage)
                },
                null,
                null,
                arr,
                null,
                null,
                null
              );
              res.send(msg);
            }
          }
        }
      } else if(req.query.driver && response.length > 0) {
        let arr = [];
        let cnt = 0;
        for(let i=0; i<response.length; i++) {
          cnt++;
          if(response[i].riderId != undefined && response[i].riderId != null) { 
            arr.push(response[i]);
          }
          if(cnt == response.length) {
            if(arr.length > 0) {
              let order = new OrderController(arr);
              order.getDetailHistoryOrder(data => {
                let msg = resMessage(
                  1,
                  res.statusCode,
                  "Data histori order",
                  {
                    total: arr.length,
                    limit: limitPerPage,
                    page: page,
                    pages: Math.ceil(arr.length / limitPerPage)
                  },
                  null,
                  null,
                  data,
                  null,
                  null,
                  null
                );
                res.send(msg);
              });
            } else {
              let msg = resMessage(
                1,
                res.statusCode,
                "Data histori order",
                {
                  total: arr.length,
                  limit: limitPerPage,
                  page: page,
                  pages: Math.ceil(arr.length / limitPerPage)
                },
                null,
                null,
                arr,
                null,
                null,
                null
              );
              res.send(msg);
            }
          }
        }
      } else {
        let msg = resMessage(
          1,
          res.statusCode,
          "Food Order Data",
          {
            total: dataCount,
            limit: limitPerPage,
            page: page,
            pages: Math.ceil(dataCount / limitPerPage)
          },
          null,
          null,
          response,
          null,
          null,
          null
        );
        res.send(msg);
      }
    })
    .catch(err => {
      let msg = resMessage(
        0,
        res.statusCode,
        err.message,
        null,
        null,
        null,
        null,
        err.errors,
        null,
        null
      );
      res.send(msg);
    });
};

export const pingOrder = (req, res) => {
  // let ObjectId = require('mongoose').Types.ObjectId;
  let prm = req.params || req.query;
  driverModel
    .find({
      $and: [
        {
          _id: prm.id
        },
        {
          ready: false
        }
      ]
    })
    .then(result => {
      if (result.length > 0) {
        let ord = foodOrderModel
          .find({ riderId: prm.id })
          .populate("boncengerId")
          .populate("riderId")
          .populate("namaResto")
          .populate("namaMenu.menuId")
          .sort({ tanggal: -1 })
          .exec();
        return [result, ord];
      }
    })
    .spread((result, ord) => {
      if (ord[0].status == 1 || ord[0].status == 2 || ord[0].status == 3) {
        // res.send(ord[0]);
        let order = new OrderController(ord[0]);
        order.getDetailOrder(x => {
          res.send({
            s: 1,
            statusCode: res.statusCode,
            message: "Order Id terakhir",
            dt: x
          });
        });
      } else {
        res.send({
          s: 1,
          statusCode: res.statusCode,
          message: "Tidak ditemukan Order terakhir"
        });
      }
    })
    .catch(err => {
      res.send({
        s: 0,
        statusCode: res.statusCode,
        dt: []
      });
    });
};