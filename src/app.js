/**
 * @author Artha Prihardana 
 * @Date 2017-01-28 11:29:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2017-09-28 08:50:55
 */
import express from 'express';
import path from 'path';
import http from 'http';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import compress from 'compression';
import methodOverride from 'method-override';
import helmet from 'helmet';
import cors from 'cors';
import mongoose from 'mongoose';
import mailer from 'nodemailer';
import firebase from 'node-gcm';
import moment from 'moment';
import socketio from 'socket.io';

import config from './app.conf';
import routes from './routes/index';
import tokenConfig from './middlewares/token';
import ioHelper from './helpers/ioHelper';
// import ioHelper from './helpers/ioHelper_clone';

// mongoose.connect(config.db);
mongoose.connect(config.db, config.dbAuth);
mongoose.Promise = require('bluebird');

const app = express();
const server = http.Server(app);
const io = new socketio(server, {
    pingInterval: 60000,
    pingTimeout: 5000,
    reconnection: true
});
ioHelper(io);

// middlewares
// app.all('/api/v3/ns/*', [tokenConfig]);
// app.all('/api/v3/ws/*', [tokenConfig]);

app.set('port', config.apiPort);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(compress());
app.use(methodOverride());
app.use(helmet());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static('public/images'));

app.use('/api/v3', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      s:0,
      statusCode: res.statusCode,
      message: err.messsage,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    s:0,
    statusCode: res.statusCode,
    message: err.message,
    error: {}
  });
});

// app.listen(app.get('port'), () => {
//   console.log(`Server running with port ${app.get('port')}`);
// });
server.listen(app.get('port'), () => {
  console.log(`Server running with port ${app.get('port')}`);
});

export default server;
// export default app;
